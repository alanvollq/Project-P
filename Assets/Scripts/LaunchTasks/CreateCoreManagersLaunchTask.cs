using Core.Context;
using Core.GameLaunch;
using Core.Systems.UI;
using UnityEngine;

namespace LaunchTasks
{
    [CreateAssetMenu(menuName = "Data/Launch Tasks/Create Core Managers", fileName = "Create Core Managers Launch Task")]
    public class CreateCoreManagersLaunchTask : LaunchTask
    {
        public override void Execute(IGameContext gameContext)
        {
            var uiManager = new UISystemManager();

            gameContext
                .AddSystem<IUISystemManager>(uiManager)
                ;
        }
    }
}