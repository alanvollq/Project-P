﻿using Core.Context;
using Core.GameLaunch;
using Core.Systems.SoundSystem.SoundTracks;
using Game.Data;
using Game.Managers.AdsManager;
using Game.Managers.EnemyManager;
using Game.Managers.GameSoundManager;
using Game.Managers.GameStateManager;
using Game.Managers.LevelCurrencyManager;
using Game.Managers.LevelHealthManager;
using Game.Managers.WaveManager;
using Game.Managers.WayPointManager;
using Game.Managers.WorldMarkManager;
using Game.Towers;
using UnityEngine;

namespace LaunchTasks
{
    [CreateAssetMenu(menuName = "Data/Launch Tasks/Create Game Managers",
        fileName = "Create Game Managers Launch Task")]
    public class CreateGameManagersLaunchTask : LaunchTask
    {
        [SerializeField] private WorldMarkPrefabsData _worldMarkPrefabsData;
        [SerializeField] private WayPointManagerSettings _pointManagerSettings;
        [SerializeField] private SoundTrack _mainSoundTrack;
        [SerializeField] private SoundTrack _battleSoundTrack;


        public override void Execute(IGameContext gameContext)
        {
            var towerManager = new TowerManager();
            var levelCurrencyManager = new LevelCurrencyManager();
            var levelHealthManager = new LevelHealthManager();
            var worldMarkManager = new WorldMarkManager(_worldMarkPrefabsData);
            var wayPointManager = new WayPointManager(_pointManagerSettings);
            var adsManager = new AdsManager();
            var gameSoundManager = new GameSoundManager(_mainSoundTrack, _battleSoundTrack);
            var gameStateManager = new GameStateManager();
            var enemyManager = new EnemyManager();
            var waveManager = new WaveManager();

            gameContext
                .AddManager<ITowerManager>(towerManager)
                .AddManager<ILevelCurrencyManager>(levelCurrencyManager)
                .AddManager<ILevelHealthManager>(levelHealthManager)
                .AddManager<IWorldMarkManager>(worldMarkManager)
                .AddManager<IWayPointManager>(wayPointManager)
                .AddManager<IAdsManager>(adsManager)
                .AddManager<IGameStateManager>(gameStateManager)
                .AddManager<IGameSoundManager>(gameSoundManager)
                .AddManager<IEnemyManager>(enemyManager)
                .AddManager<IWaveManager>(waveManager)
                ;
        }
    }
}