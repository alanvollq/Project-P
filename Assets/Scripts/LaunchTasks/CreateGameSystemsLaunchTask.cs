using Core.Context;
using Core.GameLaunch;
using Core.Systems.SoundSystem;
using Game.Data;
using Game.Setup;
using Game.Systems.BuildSystem;
using Game.Systems.CameraSystem;
using Game.Systems.CreatureSystem;
using Game.Systems.EnemySystem;
using Game.Systems.InputSystem;
using Game.Systems.LevelSystem;
using Game.Systems.PathSystem;
using Game.Systems.WaveSystem;
using UnityEngine;

namespace LaunchTasks
{
    [CreateAssetMenu(menuName = "Data/Launch Tasks/Create Game Systems", fileName = "Create Game Systems Launch Task")]
    public sealed class CreateGameSystemsLaunchTask : LaunchTask
    {
        [SerializeField] private SystemsData _systemsData;
        [SerializeField] private CreatureDataContainer _creatureDataContainer;


        public override void Execute(IGameContext gameContext)
        {
            var buildSystem = new BuildSystem(_creatureDataContainer);
            var inputSystem = new InputSystem();
            var cameraSystem = new CameraSystem(Camera.main);
            var newLevelSystem = new LevelSystem(_systemsData.LevelDataContainer);
            var waveSystem = new WaveSystem();
            var enemySystem = new EnemySystem();
            var creatureSystem = new CreatureSystem();
            var pathSystem = new PathSystem();

            gameContext
                .AddSystem<IBuildSystem>(buildSystem)
                .AddSystem<IInputSystem>(inputSystem)
                .AddSystem<ICameraSystem>(cameraSystem)
                .AddSystem<ILevelSystem>(newLevelSystem)
                .AddSystem<IWaveSystem>(waveSystem)
                .AddSystem<IEnemySystem>(enemySystem)
                .AddSystem<ICreatureSystem>(creatureSystem)
                .AddSystem<IPathSystem>(pathSystem)
                ;
        }
    }
}