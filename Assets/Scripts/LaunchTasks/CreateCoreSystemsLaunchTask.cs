﻿using Core.Context;
using Core.GameLaunch;
using Core.Systems.AppEventsSystem;
using Core.Systems.AudioSystem;
using Core.Systems.EventSystem;
using Core.Systems.SoundSystem;
using Core.Systems.SoundSystem.Sound;
using Core.Systems.TimeSystem;
using Core.Systems.UI.MessageboxSystem;
using Core.Systems.UI.PageSystem;
using Core.Systems.UI.PopupSystem;
using Core.Systems.UI.TooltipSystem;
using Core.Systems.UpdateSystem;
using Game.Setup;
using UnityEngine;

namespace LaunchTasks
{
    [CreateAssetMenu(menuName = "Data/Launch Tasks/Create Core Systems", fileName = "Create Core Systems Launch Task")]
    public class CreateCoreSystemsLaunchTask : LaunchTask
    {
        [SerializeField] private SoundSettingsData _soundSettingsData;
        [SerializeField] private SoundSource _soundSourcePrefab;
        [SerializeField] private SoundTrackEventChannel[] _soundTrackEventChannels;
        [SerializeField] private SystemsData _systemsData;

        private IGameContext _gameContext;


        public override void Execute(IGameContext gameContext)
        {
            _gameContext = gameContext;

            var timeSystem = new TimeSystem();
            var soundSystem = new SoundSystem(_soundTrackEventChannels, _soundSourcePrefab, _soundSettingsData);

            _gameContext.AddSystem<ITimeSystem>(timeSystem);
            _gameContext.AddSystem<ISoundSystem>(soundSystem);

            CreateGameObjectSystems();
            CreateUISystems();
        }

        private void CreateGameObjectSystems()
        {
            var dontDestroyableGameObject = new GameObject("[ Systems ]");

            var updatableSystem = dontDestroyableGameObject.AddComponent<UpdatableSystem>();
            var appEventsSystem = dontDestroyableGameObject.AddComponent<AppEventsSystem>();
       
            DontDestroyOnLoad(dontDestroyableGameObject);

            _gameContext
                .AddSystem<IUpdateSystem>(updatableSystem)
                .AddSystem<IAppEventsSystem>(appEventsSystem)
                ;
        }

        private void CreateUISystems()
        {
            var uiContainer = _systemsData.UIContainer;
            var pageSystem = new PageSystem(uiContainer.Pages);
            var popupSystem = new PopupSystem(uiContainer.Popups);
            var messageboxSystem = new MessageboxSystem(uiContainer.MessageBoxes);
            var tooltipSystem = new TooltipSystem(uiContainer.Tooltips);
            var eventSystem = new EventSystem();

            _gameContext
                .AddSystem<IPageSystem>(pageSystem)
                .AddSystem<IPopupSystem>(popupSystem)
                .AddSystem<IMessageboxSystem>(messageboxSystem)
                .AddSystem<ITooltipSystem>(tooltipSystem)
                .AddSystem<IEventSystem>(eventSystem)
                ;
        }
    }
}