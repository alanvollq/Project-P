﻿using Cheats;
using Core.Context;
using Core.Extensions;
using Core.GameLaunch;
using UnityEngine;

namespace LaunchTasks
{
    [CreateAssetMenu(menuName = "Data/Launch Tasks/Create Cheats", fileName = "Create Cheats Launch Task")]
    public class CreateCheatsLaunchTask : LaunchTask
    {
        [SerializeField] private GameObject _cheatsPrefab;

        public override void Execute(IGameContext gameContext)
        {
            var cheats = _cheatsPrefab.Instantiate();
            cheats.GetComponentInChildren<CheatsController>().Initialization(gameContext);
            DontDestroyOnLoad(cheats);
        }
    }
}