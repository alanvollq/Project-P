using Core.Context;
using Core.GameLaunch;
using UnityEngine;

namespace LaunchTasks
{
    [CreateAssetMenu(menuName = "Data/Launch Tasks/Initialize Items", fileName = "Initialize Items Launch Task")]
    public class InitializeItemsLaunchTask : LaunchTask
    {
        public override void Execute(IGameContext gameContext)
        {
            gameContext.InitializeItems();
        }
    }
}