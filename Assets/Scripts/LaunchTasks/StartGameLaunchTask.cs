using Core.Context;
using Core.GameLaunch;
using Game.Managers.GameStateManager;
using UnityEngine;

namespace LaunchTasks
{
    [CreateAssetMenu(menuName = "Data/Launch Tasks/Start Game", fileName = "Start Game Launch Task")]
    public class StartGameLaunchTask : LaunchTask
    {
        public override void Execute(IGameContext gameContext)
        {
            var gameStateManager = gameContext.GetManager<IGameStateManager>();
            gameStateManager.StartGame();
        }
    }
}