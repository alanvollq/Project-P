﻿using Core.Context;
using Core.GameLaunch;
using Core.SaveSystem;
using UnityEngine;

namespace LaunchTasks
{
    [CreateAssetMenu(menuName = "Data/Launch Tasks/Create Save System", fileName = "Create Save System Launch Task")]
    public class CreateSaveSystemLaunchTask : LaunchTask
    {
        public override void Execute(IGameContext gameContext)
        {
            SaveSystemProvider.SetSaveSystem(new PlayerPrefsSaveSystem());
        }
    }
}