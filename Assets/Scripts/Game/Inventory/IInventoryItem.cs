﻿namespace Game.Inventory
{
    public interface IInventoryItem
    {
        public int Count { get; }
    }
}