﻿using System;
using UnityEngine;

namespace Game.Creatures
{
    public class CreatureAnimationHandler
    {
        private readonly Animator _animator;

        private static readonly int _attack = Animator.StringToHash("Attack");

        public CreatureAnimationHandler(Animator animator)
        {
            _animator = animator;
        }


        public void PlayAttackAnimation()
        {
            _animator.SetTrigger(_attack);
        }

        public void PauseAnimation()
        {
            _animator.enabled = false;
        }

        public void PlayAnimation()
        {
            _animator.enabled = true;
        }
    }
}