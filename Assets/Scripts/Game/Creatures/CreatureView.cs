﻿using System;
using Game.Misc;
using UnityEngine;

namespace Game.Creatures
{
    public class CreatureView : MonoBehaviour
    {
        public event Action Selected;

        [SerializeField] private ClickHandler _clickHandler;
        [SerializeField] private SpriteRenderer _radiusSpriteRenderer;

        private CreatureAnimationHandler _animationHandler;


        private void Awake()
        {
            _clickHandler.Down += OnDown;

            var animator = GetComponentInChildren<Animator>(true);
            _animationHandler = new CreatureAnimationHandler(animator);
            
        }

        private void OnDown()
        {
            Selected?.Invoke();
        }

        public void PlayAttackAnimation()
        {
            _animationHandler.PlayAttackAnimation();
        }

        public void OnPause()
        {
            _animationHandler.PauseAnimation();
        }

        public void OnPlay()
        {
            _animationHandler.PlayAnimation();
        }
        
        public void SetRadiusViewEnable(bool value)
        {
            _radiusSpriteRenderer.enabled = value;
        }
    }
}