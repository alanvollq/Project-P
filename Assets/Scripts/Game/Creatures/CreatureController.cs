﻿using System;
using Core.Pool;
using Game.Abilities;
using Game.Systems.DamageSystem;
using Game.Units.Creatures;
using UnityEngine;

namespace Game.Creatures
{
    public class CreatureController : MonoBehaviour, IPoolable, IDamageDealer
    {
        [SerializeField] private CreatureView _view;
        [SerializeField] private TargetHandler _targetHandler;

        private static CreatureController _lastSelected;
        private Weapon _weapon;
        private bool _isPause;


        private void Awake()
        {
            _view.Selected += OnSelected;

            _weapon = new Weapon(_targetHandler, this, transform);
            _weapon.Attack += _view.PlayAttackAnimation;
        }

        private void OnSelected()
        {
            _view.SetRadiusViewEnable(true);
        }

        public void Init(Creature creature)
        {
            var weaponAbilityData = creature.AbilitySlots.WeaponAbilitySlot.ActiveAbility.WeaponAbilityData;
            _targetHandler.SetDistance(weaponAbilityData.Stats.Range);

            _isPause = false;

            _weapon.SetAbilityData(weaponAbilityData);
        }

        private void Update()
        {
            if (_isPause)
                return;

            _weapon?.Update();
        }

        public void Played(float pauseTime)
        {
            _view.OnPlay();
            _isPause = false;
        }

        public void Pause()
        {
            _view.OnPause();
            _isPause = true;
        }

        public void Recycle()
        {
        }

        public void Release()
        {
        }

        public event Action<DamageResult> OnDealDamage;

        public void AddDamageResult(DamageResult damageResult)
        {
            //     //TODO прокинуть урон через систему урона, а не через диллера.
            //     _allDamage += damageResult.Damage;
            OnDealDamage?.Invoke(damageResult);
        }
    }
}