using System.Collections.Generic;
using Game.Enemies;
using Game.Enemies.States;
using UnityEngine;

namespace Game.Creatures
{
    public class TargetHandler : MonoBehaviour
    {
        [SerializeField] private Transform _radiusTransform;
        
        private readonly List<Enemy> _enemies = new ();
        private float _distance;
        private float _additionalDistance;


        public bool HasTarget => _enemies.Count > 0;

        
        private void Awake()
        {
            var localScale = transform.localScale;
            _additionalDistance = Mathf.Min(localScale.x, localScale.y) / 2;
        }

        public void SetDistance(float value)
        {
            _distance = value + _additionalDistance;
            var scale = _distance * 2;
            _radiusTransform.localScale = new Vector3(scale, scale, 1);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            var enemy = other.GetComponent<Enemy>();
            if (enemy == null)
                return;

            if(_enemies.Contains(enemy))
                return;
            
            //TODO: костыль. Почему-то враги добавляются несколько раз.
            if(enemy.Stats.CurrentHealth < 1)
                return;
            
            enemy.StateChanged += EnemyOnStateChanged;
            _enemies.Add(enemy);
            Debug.Log($"Add Enemy {enemy.name} {_enemies.Count}");
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            var enemy = other.GetComponent<Enemy>();
            if (enemy == null)
                return;

            RemoveEnemy(enemy);
        }
        
        private void EnemyOnStateChanged(Enemy enemy, EnemyStateId enemyStateId)
        {
            if (enemyStateId is not (EnemyStateId.ReachingTarget or EnemyStateId.Dying)) 
                return;
            
            RemoveEnemy(enemy);
        }

        private void RemoveEnemy(Enemy enemy)
        {
            if(_enemies.Contains(enemy) == false)
                return;
            
            enemy.StateChanged -= EnemyOnStateChanged;
            _enemies.Remove(enemy);
            Debug.Log($"Remove Enemy  {enemy.name} {_enemies.Count}");
        }
        
        public Enemy GetTarget()
        {
            return _enemies[0];
        }
    }
}