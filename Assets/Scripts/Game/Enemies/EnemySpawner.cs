using System;
using System.Collections.Generic;
using Core.Systems.UpdateSystem;
using System.Linq;
using Core.Systems.TimeSystem;
using Game.Enemies.States;
using Game.Levels;
using Game.Levels.Waves;
using Game.Systems.EnemySystem;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Game.Enemies
{
    public class EnemySpawner : IUpdatable
    {
        public event Action SpawnCompleted;

        private readonly List<TestSpawner> _testSpawners = new();
        private readonly IEnemySystem _enemySystem;
        private readonly ITimeSystem _timeSystem;


        public EnemySpawner(IEnemySystem enemySystem, ITimeSystem timeSystem)
        {
            _enemySystem = enemySystem;
            _timeSystem = timeSystem;
        }


        public void SetWave(WaveData waveData, LevelMap levelMap)
        {
            Reset();

            var spawnerOnWaves = waveData.SpawnersOnWaves.ToList();
            for (var i = spawnerOnWaves.Count - 1; i >= 0; i--)
            {
                var waveParts = spawnerOnWaves[i].WaveParts;
                var spawnPoint = levelMap.SpawnPoints[i];

                foreach (var wavePart in waveParts)
                {
                    var testSpawner = new TestSpawner(_enemySystem, _timeSystem);
                    testSpawner.SetWavePart(wavePart, spawnPoint, levelMap.EndPoint);
                    _testSpawners.Add(testSpawner);
                }
            }
        }

        public void Reset()
        {
            _testSpawners.Clear();
        }

        public void Update()
        {
            if (_timeSystem.IsPause)
                return;

            if (_testSpawners.Count < 1)
                return;

            _testSpawners.ForEach(item => item.Update());
            _testSpawners.RemoveAll(spawner => spawner.IsSpawnCompleted);

            if (_testSpawners.Count < 1)
                SpawnCompleted?.Invoke();
        }
    }


    public class TestSpawner
    {
        private readonly IEnemySystem _enemySystem;
        private readonly ITimeSystem _timeSystem;
        private WavePart _wavePart;
        private Transform _spawnPoint;
        private Transform _targetPoint;
        private float _delayLeftTime;
        private float _spawnLeftTime;
        private int _needCount;


        public TestSpawner(IEnemySystem enemySystem, ITimeSystem timeSystem)
        {
            _enemySystem = enemySystem;
            _timeSystem = timeSystem;
        }


        public bool IsSpawnCompleted => _needCount == 0;


        public void SetWavePart(WavePart wavePart, Transform spawnPoint, Transform targetPoint)
        {
            _wavePart = wavePart;
            _delayLeftTime = _wavePart.StartDelay;
            _needCount = wavePart.Count;

            _spawnPoint = spawnPoint;
            _targetPoint = targetPoint;
        }

        public void Update()
        {
            if (_timeSystem.IsPause)
                return;

            _delayLeftTime -= Time.deltaTime;
            if (_delayLeftTime > 0)
                return;

            if (IsSpawnCompleted)
                return;

            _spawnLeftTime -= Time.deltaTime;
            if (_spawnLeftTime > 0)
                return;

            var enemyData = _wavePart.EnemyDataByQuality;
            _spawnLeftTime = _wavePart.Interval;
            _needCount--;

            var enemy = _enemySystem.GetEnemy(enemyData);
            var startPosition = _spawnPoint.position +
                                new Vector3(Random.Range(-0.25f, 0.25f), Random.Range(-0.25f, 0.25f));
            enemy.Init(startPosition, _targetPoint.position, enemyData.Stats);
            enemy.StateChanged += EnemyOnStateChanged;
        }

        private void EnemyOnStateChanged(Enemy enemy, EnemyStateId enemyStateId)
        {
            if (enemyStateId is EnemyStateId.ReachingTarget or EnemyStateId.Dying)
            {
                enemy.StateChanged -= EnemyOnStateChanged;
                _enemySystem.ReturnEnemy(enemy);
            }
        }
    }
}