﻿namespace Game.Enemies.States
{
    public enum EnemyStateId
    {
        Searching,
        Moving,
        ReachingTarget,
        Dying
    }
}