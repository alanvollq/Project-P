using Game.UI;
using Game.UI.UIElements;
using UnityEngine;

namespace Game.Enemies
{
    public class EnemyView : MonoBehaviour
    {
        [SerializeField] private TransformFillBar _fillBar;
        
        private EnemyStats _stats;
        private Animator _animator;
        private static readonly int TakeDamage = Animator.StringToHash("TakeDamage");


        private void Awake()
        {
            _animator = GetComponentInChildren<Animator>(true);
        }

        private void OnHealthChanged()
        {
            _fillBar.SetValue(_stats.PercentCurrentValue);
            _animator.SetTrigger(TakeDamage);
        }

        public void Init(EnemyStats stats)
        {
            _stats = stats;
            OnHealthChanged();
            _stats.HealthChanged += OnHealthChanged;
        }

        public void PauseAnimation()
        {
            _animator.enabled = false;
        }

        public void PlayAnimation()
        {
            _animator.enabled = true;
        }
    }
}