using System;
using Core.Extensions;
using Core.Pool;
using DG.Tweening;
using Game.Enemies.States;
using Game.Systems.DamageSystem;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

namespace Game.Enemies
{
    public class Enemy : MonoBehaviour, IDamageable, IPoolable
    {
        public event Action<Enemy, EnemyStateId> StateChanged;
        public static event Action<Enemy> MoveCompleted;
        public static event Action<Enemy> Death;

        [SerializeField] private EnemyView _view;

        private EnemyStats _stats;
        private Transform _transform;


        public EnemyStats Stats => _stats;


        private void Awake()
        {
            _transform = transform;
        }

        public void Init(Vector3 startPoint, Vector3 targetPoint, EnemyStats stats)
        {
            _stats = new EnemyStats(stats);
            _transform.position = startPoint;
            _view.Init(_stats);
            Move(targetPoint);
        }

        public void Pause()
        {
            _transform.DOPause();
            _view.PauseAnimation();
        }

        public void Played(float pauseTime)
        {
            _transform.DOPlay();
            _view.PlayAnimation();
        }

        private void Move(Vector3 position)
        {
            var path = new NavMeshPath();
            NavMesh.CalculatePath(_transform.position, position, NavMesh.AllAreas, path);
            var pathPoints = path.corners;
            for (var i = pathPoints.Length - 1; i >= 0; i--)
            {
                pathPoints[i].x += Random.Range(-0.2f, 0.2f);
                pathPoints[i].y += Random.Range(-0.2f, 0.2f);
            }

            var distance = path.corners.Distance();
            var duration = distance / _stats.Speed;
            _transform
                .DOPath(pathPoints, duration)
                .SetEase(Ease.Linear)
                .OnComplete(() =>
                {
                    StateChanged?.Invoke(this, EnemyStateId.ReachingTarget);
                    MoveCompleted?.Invoke(this);
                })
                ;
        }

        public DamageResult TakeDamage(Damage damage)
        {
            var prevHealth = _stats.CurrentHealth;
            _stats.SetValue(_stats.CurrentHealth - damage.Value);
            var damageResult = new DamageResult
            {
                Damage = prevHealth - _stats.CurrentHealth
            };

            if (_stats.CurrentHealth <= 0)
            {
                StateChanged?.Invoke(this, EnemyStateId.Dying);
                Death?.Invoke(this);
            }

            damage.Owner.AddDamageResult(damageResult);
            return damageResult;
        }

        public void Recycle()
        {
        }

        public void Release()
        {
            _transform.DOKill();
        }
    }
}