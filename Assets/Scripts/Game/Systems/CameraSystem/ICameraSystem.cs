﻿using System;
using Core.Systems;
using UnityEngine;

namespace Game.Systems.CameraSystem
{
    public interface ICameraSystem : ISystem
    {
        public event Action<float> ZoomChanged;

        public Camera MainCamera { get; }
        public Vector3 CameraPosition { get; }
        public float MoveSpeed { get; }
        public float Zoom { get; }
        public float ScaleFactor { get; }
        public float OrthographicSize { get; }

        public void SetCameraPosition(Vector3 position);
        public void SetOrthographicSize(float value);
        public void Move(Vector2 normalized);
        public void SetMoveSpeed(float value);
    }
}