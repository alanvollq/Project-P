﻿using Core.Systems;
using UnityEngine;

namespace Game.Systems.PathSystem
{
    public interface IPathSystem : ISystem
    {
        public Vector3[] GetPath(Vector3 from, Vector3 to);
    }
}