﻿using UnityEngine;
using UnityEngine.AI;

namespace Game.Systems.PathSystem
{
    public class PathSystem : IPathSystem
    {
        public Vector3[] GetPath(Vector3 from, Vector3 to)
        {
            var path = new NavMeshPath();
            NavMesh.CalculatePath(from, to, NavMesh.AllAreas, path);
            return path.corners;
        }
    }
}