﻿using System;
using Core.Context;
using Core.Systems.UpdateSystem;
using Game.Systems.CameraSystem;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Game.Systems.InputSystem
{
    public class InputSystem : IInputSystem, IGameContextInitializable, IFixedUpdatable
    {
        public event Action<float> OnZoomStarted;
        public event Action PrimaryTouch;

        private readonly PlayerInputActions _playerInputActions = new();

        private bool _hasPrimaryTouch;
        private bool _hasSecondTouch;
        private Vector2 _deltaMove;
        private Vector2 _primaryWorldPosition;
        private Vector2 _secondWorldPosition;
        private Vector2 _screenPosition;
        private Vector2 _worldTouchPosition;
        private Vector2 _primaryTouchPosition;
        private Vector2 _secondaryTouchPosition;
        private float _lastTouchDistance;
        private float _touchZoom;

        private ICameraSystem _cameraSystem;
        private IUpdateSystem _updateSystem;

        public bool HasPrimaryTouch => _hasPrimaryTouch;
        public bool HasSecondTouch => _hasSecondTouch;
        public Vector2 DeltaMove => _deltaMove;
        public Vector2 WorldPosition => _primaryWorldPosition;
        public Vector2 ScreenPosition => _screenPosition;
        public Vector2 WorldTouchPosition => _worldTouchPosition;


        public void Initialization(IGameContext gameContext)
        {
            _cameraSystem = gameContext.GetSystem<ICameraSystem>();
            _updateSystem = gameContext.GetSystem<IUpdateSystem>();
            _updateSystem.AddFixedUpdatable(this);

            InitOnAndroid();
            InitOnEditor();

            Enable();
        }

        public void Enable()
        {
            _playerInputActions.Enable();
        }

        public void Disable()
        {
            _playerInputActions.Disable();
        }

        private void InitOnAndroid()
        {
            _playerInputActions.Touch.Movement.performed += i => { _deltaMove = i.ReadValue<Vector2>(); };
            _playerInputActions.Touch.PrimaryPosition.performed += context =>
            {
                _primaryTouchPosition = context.ReadValue<Vector2>();
                _primaryWorldPosition = _cameraSystem.MainCamera.ScreenToWorldPoint(_primaryTouchPosition);
            };

            _playerInputActions.Touch.SecondaryPosition.performed += delegate(InputAction.CallbackContext context)
            {
                _secondaryTouchPosition = context.ReadValue<Vector2>();
                _secondWorldPosition = _cameraSystem.MainCamera.ScreenToWorldPoint(_secondaryTouchPosition);
            };

            _playerInputActions.Touch.Touch0Contact.started += PrimaryTouchStarted;
            _playerInputActions.Touch.Touch0Contact.canceled += i => _hasPrimaryTouch = false;

            _playerInputActions.Touch.Touch1Contact.started += i =>
            {
                _lastTouchDistance = Vector2.Distance(_primaryTouchPosition, _secondaryTouchPosition);
                _hasSecondTouch = true;
            };
            _playerInputActions.Touch.Touch1Contact.canceled += i => _hasSecondTouch = false;
        }

        private void TouchZoomUpdate()
        {
            if (_hasPrimaryTouch == false || _hasSecondTouch == false)
                return;

            var touchDistance = Vector2.Distance(_primaryTouchPosition, _secondaryTouchPosition);
            var difference = Mathf.Abs(touchDistance - _lastTouchDistance);

            if (touchDistance < _lastTouchDistance)
                _touchZoom = 1;
            else if (touchDistance > _lastTouchDistance)
                _touchZoom = -1;
            else
                _touchZoom = 0;

            //TODO: move zoom speed to settings.
            _touchZoom *= difference * Time.fixedDeltaTime * 10;

            _lastTouchDistance = touchDistance;
            OnZoomStarted?.Invoke(_touchZoom);
        }

        private void InitOnEditor()
        {
            _playerInputActions.Mouse.Movement.performed += i => _deltaMove = i.ReadValue<Vector2>();
            _playerInputActions.Mouse.Position.performed += PrimaryPositionChanged;
            _playerInputActions.Mouse.Click.started += PrimaryTouchStarted; 

            _playerInputActions.Mouse.Zoom.started += i => OnZoomStarted?.Invoke(-i.ReadValue<float>());
        }

        private void PrimaryTouchStarted(InputAction.CallbackContext context)
        {
            _hasPrimaryTouch = true;
            PrimaryTouch?.Invoke();
        }

        private void PrimaryPositionChanged(InputAction.CallbackContext context)
        {
            var position = context.ReadValue<Vector2>();
            _screenPosition = position;
            _primaryWorldPosition = _cameraSystem.MainCamera.ScreenToWorldPoint(position);
        }

        public void FixedUpdate()
        {
            TouchZoomUpdate();
        }
    }
}