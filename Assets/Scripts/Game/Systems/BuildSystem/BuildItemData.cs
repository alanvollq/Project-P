using UnityEngine;

namespace Game.Systems.BuildSystem
{
    [CreateAssetMenu(menuName = "Data/Game/Builds/Build Item Data", fileName = "Build Item Data")]
    public class BuildItemData : ScriptableObject
    {
        [SerializeField] private PlacingItem _placingItem;
        [SerializeField] private Build _buildPrefab;

        
        public PlacingItem PlacingItem => _placingItem;
        public Build BuildPrefab => _buildPrefab;
    }
}