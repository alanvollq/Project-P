﻿using UnityEngine;

namespace Game.Systems.BuildSystem
{
    public class BuildPlacingView : MonoBehaviour
    {
        [SerializeField] private Transform _radius;


        private void Awake()
        {
            SetEnableRadiusView(false);
        }

        public void SetEnableRadiusView(bool value)
        {
            _radius.gameObject.SetActive(value);
        }

        public void SetRadiusValue(float value)
        {
            _radius.localScale = new Vector3(value, value, 1);
        }
    }
}