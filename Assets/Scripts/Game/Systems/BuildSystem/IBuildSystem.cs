using System;
using System.Collections.Generic;
using Core.Systems;
using Game.Units.Creatures;

namespace Game.Systems.BuildSystem
{
    public interface IBuildSystem : ISystem
    {
        public event Action BuildModeActivated;
        public event Action BuildModeDeactivated;
        public event Action Built;
        
        
        public void AcceptPlacing(Creature lastSelected);
    }
}