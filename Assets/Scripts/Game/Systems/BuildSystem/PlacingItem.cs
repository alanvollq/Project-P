using System;
using System.Collections.Generic;
using Game.Systems.CameraSystem;
using Game.Systems.InputSystem;
using UnityEngine;

namespace Game.Systems.BuildSystem
{
    public class PlacingItem : MonoBehaviour
    {
        [SerializeField] private List<PlacingItemPart> _placingItemParts;
        [SerializeField] private PlacingItemMoveHandler _moveHandler;
        
        private int _currentRotateAngle;
        private bool _isRotating;
        private float _touchDistance;
        private Vector2 _touchOffset;
        

        public void Init(IInputSystem inputSystem, ICameraSystem cameraSystem)
        {
            _moveHandler.Init(inputSystem, cameraSystem, transform);
        }

        public void Rotate()
        {
        }
    }
}