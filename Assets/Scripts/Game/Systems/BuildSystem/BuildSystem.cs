using System;
using System.Collections.Generic;
using Core.Context;
using Core.Extensions;
using Core.Loggers;
using Core.Systems.UI.PopupSystem;
using Game.Data;
using Game.Levels;
using Game.Systems.CreatureSystem;
using Game.Systems.LevelSystem;
using Game.Units.Creatures;
using UnityEngine;
using static Game.Data.Loggers.GameLogData;

namespace Game.Systems.BuildSystem
{
    public class BuildSystem : IBuildSystem, IGameContextInitializable
    {
        public event Action BuildModeActivated;
        public event Action BuildModeDeactivated;
        public event Action Built;

        private IGameContext _gameContext;
        private ILevelSystem _levelSystem;
        private IPopupSystem _popupSystem;

        private LevelMap _levelMap;
        private readonly CreatureDataContainer _creatureDataContainer;

        private Vector3 _selectPosition;
        private Transform _buildsParent;
        private bool _isBuildModeActive;
        private ICreatureSystem _creatureSystem;


        public BuildSystem(CreatureDataContainer creatureDataContainer)
        {
            _creatureDataContainer = creatureDataContainer;
        }


        public void Initialization(IGameContext gameContext)
        {
            _gameContext = gameContext;

            _levelSystem = _gameContext.GetSystem<ILevelSystem>();
            _popupSystem = _gameContext.GetSystem<IPopupSystem>();
           _creatureSystem = _gameContext.GetSystem<ICreatureSystem>();

            _levelSystem.LevelLoaded += OnLevelLoaded;
            _levelSystem.LevelUnloaded += OnLevelUnloaded;

            _buildsParent = new GameObject("[ Builds ]").transform;
            _buildsParent.SetParent(_gameContext.RootOwner.GameRoot);
        }

        private void OnLevelLoaded()
        {
            _levelMap = _levelSystem.LevelMap;
            _levelMap.SelectPosition += OnLevelMapSelectPosition;
            _levelMap.SelectedEmpty += MapOnSelectedEmpty;
        }

        private void OnLevelUnloaded()
        {
            _levelMap.SelectPosition -= OnLevelMapSelectPosition;
            _levelMap.SelectedEmpty -= MapOnSelectedEmpty;
            _levelMap = null;
        }

        private void OnLevelMapSelectPosition(Vector3 selectPosition)
        {
            ActivateBuildMode();
            _selectPosition = selectPosition;
        }

        private void MapOnSelectedEmpty()
        {
            DeactivateBuildMode();
        }

        private void ActivateBuildMode()
        {
            if (_isBuildModeActive == true)
                return;

            BuildLogData.Log("ActivateBuildMode");
            _isBuildModeActive = true;
            BuildModeActivated?.Invoke();
        }

        private void DeactivateBuildMode()
        {
            if (_isBuildModeActive == false)
                return;

            BuildLogData.Log("DeactivateBuildMode");
            _isBuildModeActive = false;
            BuildModeDeactivated?.Invoke();
        }

        public void AcceptPlacing(Creature creature)
        {
            var creatureController = _creatureSystem.GetCreature(creature);
            creatureController.transform.position = _selectPosition;

            DeactivateBuildMode();
            Built?.Invoke();
        }
    }
}