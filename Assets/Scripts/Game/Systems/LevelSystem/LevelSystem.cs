﻿using System;
using System.Collections.Generic;
using Core.Context;
using Core.Systems.UpdateSystem;
using Game.Data;
using Game.Levels;
using Game.Levels.MainCampaign;
using Game.Levels.StateMachine;
using Game.Units.Creatures;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Game.Systems.LevelSystem
{
    public class LevelSystem : ILevelSystem, IGameContextInitializable
    {
        public event Action LevelSelected;
        public event Action LevelLoaded;
        public event Action LevelUnloaded;

        private IGameContext _gameContext;

        private Transform _levelParent;
        private List<Creature> _creaturesOnLevel;
        private LevelStateMachine _stateMachine;
        private IUpdateSystem _updateSystem;


        public LevelSystem(LevelDataContainer levelDataContainer)
        {
            levelDataContainer.Load();
        }


        public LevelData LevelData { get; private set; }

        public LevelMap LevelMap { get; private set; }
        
        public StageData SelectedStage { get; private set; }
        
        public IEnumerable<Creature> SelectedCreatures => _creaturesOnLevel;



        public void Initialization(IGameContext gameContext)
        {
            _gameContext = gameContext;
            _updateSystem = _gameContext.GetSystem<IUpdateSystem>();

            _levelParent = new GameObject("[ Levels ]").transform;
            _levelParent.SetParent(_gameContext.RootOwner.GameRoot);
            
            _stateMachine = new LevelStateMachine(gameContext);
            _stateMachine.Initialization();
            _updateSystem.AddUpdatable(_stateMachine);
        }


        public void SelectLevel(LevelData levelData)
        {
            LevelData = levelData;
            LevelSelected?.Invoke();
        }
        
        public void SelectState(StageData stageData)
        {
            SelectedStage = stageData;
        }

        public void RestartLevel()
        {
            // _level.Restart();
        }

        private LevelMap CreateMap()
        {
            var map = Object.Instantiate(LevelData.LevelMapPrefab, _levelParent);
            map.transform.position += map.Offset;
            map.Init(_gameContext);
            return map;
        }
        
        public void LoadLevel()
        {
            LevelMap = CreateMap();
            // _level = new Level(_selectedLevel, _creaturesOnLevel, _levelMap);
            
            LevelLoaded?.Invoke();
            
            _stateMachine.Enable();
        }

        public void UnloadLevel()
        {
            Object.Destroy(LevelMap.gameObject);
            
            _stateMachine.Disable();
        }

        public void SetCreatures(List<Creature> creaturesOnLevel)
        {
            _creaturesOnLevel = creaturesOnLevel;
        }

        public void CompleteLevel()
        {
            // _level.Complete();
        }
    }
}