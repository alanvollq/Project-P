﻿using System;
using System.Collections.Generic;
using Core.Systems;
using Game.Levels;
using Game.Levels.MainCampaign;
using Game.Units.Creatures;

namespace Game.Systems.LevelSystem
{
    public interface ILevelSystem : ISystem
    {
        public event Action LevelSelected;
        public event Action LevelLoaded;
        public event Action LevelUnloaded;


        public LevelData LevelData { get; }
        public LevelMap LevelMap { get; }
        public IEnumerable<Creature> SelectedCreatures { get; }
        public StageData SelectedStage { get; }


        public void SelectLevel(LevelData levelData);
        public void LoadLevel();
        public void UnloadLevel();
        public void SetCreatures(List<Creature> creaturesOnLevel);
        public void CompleteLevel();
        public void SelectState(StageData stageData);
       public void RestartLevel();
    }
}