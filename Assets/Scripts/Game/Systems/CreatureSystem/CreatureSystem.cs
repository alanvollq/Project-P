﻿using System.Collections.Generic;
using System.Linq;
using Core.Context;
using Core.Extensions;
using Core.Pool;
using Core.Systems.TimeSystem;
using Game.Creatures;
using Game.Units.Creatures;
using UnityEngine;

namespace Game.Systems.CreatureSystem
{
    public class CreatureSystem : ICreatureSystem, IGameContextInitializable
    {
        private IGameContext _gameContext;
        private ITimeSystem _timeSystem;
        private readonly Dictionary<Creature, ObjectsPool<CreatureController>> _pools = new();
        private Transform _enableCreaturesParent;
        private Transform _disableCreaturesParent;


        public void Initialization(IGameContext gameContext)
        {
            _gameContext = gameContext;
            _timeSystem = gameContext.GetSystem<ITimeSystem>();
            _timeSystem.Played += TimeSystemOnPlayed;
            _timeSystem.Paused += TimeSystemOnPaused;

            var creatureParent = new GameObject("[ Creatures ]").transform;
            _enableCreaturesParent = new GameObject("[ Enable Creatures ]").transform;
            _disableCreaturesParent = new GameObject("[ Disable Creatures ]").transform;
            creatureParent.SetParent(_gameContext.RootOwner.GameRoot);
            _enableCreaturesParent.SetParent(creatureParent);
            _disableCreaturesParent.SetParent(creatureParent);
        }

        private void TimeSystemOnPlayed(float pauseTime)
        {
            _pools.Values
                .SelectMany(pool => pool.ActiveObjects)
                .ApplyToAll(controller => controller.Played(pauseTime));
        }

        private void TimeSystemOnPaused()
        {
            _pools.Values
                .SelectMany(pool => pool.ActiveObjects)
                .ApplyToAll(controller => controller.Pause());
        }

        public CreatureController GetCreature(Creature creature)
        {
            if (_pools.TryGetValue(creature, out var pool) == false)
            {
                pool = new ObjectsPool<CreatureController>(
                    _enableCreaturesParent, _disableCreaturesParent, creature.CreatureControllerPrefab
                    );
                _pools.Add(creature, pool);
            }

            var creatureController = pool.Get();
            creatureController.Init(creature);
            return creatureController;
        }

        public void ReturnCreature(CreatureController creatureController)
        {
            _pools.Values.ApplyToAll(pool => pool.TryReturn(creatureController));
        }

        public void ReturnAll()
        {
            _pools.Values.ApplyToAll(pool => pool.ReturnAll());
        }
    }
}