﻿using Core.Systems;
using Game.Creatures;
using Game.Units.Creatures;

namespace Game.Systems.CreatureSystem
{
    public interface ICreatureSystem : ISystem
    {
        public CreatureController GetCreature(Creature creature);

        public void ReturnCreature(CreatureController creatureController);
        public void ReturnAll();
    }
}