﻿using System;
using Core.Systems;
using Game.Levels;
using Game.Levels.Waves;
using Game.Misc;

namespace Game.Systems.WaveSystem
{
    public interface IWaveSystem : ISystem
    {
        public event Action WaveStarted;
        public event Action WaveCompleted;
        public event Action WavesChanged;


        public WaveData CurrentWaveData { get; }
        public int CurrentWaveNumber { get; }
        public int WaveCount { get; }
        public Counter EnemyCounter { get; }
        public Counter WaveCounter { get; }
        public bool WaveIsActive { get; }


        public void StartWave();
        public void ClearWave();
        public void WaveComplete();
        public void SetWaves(LevelWaves levelWaves, LevelMap levelMap);
    }
}