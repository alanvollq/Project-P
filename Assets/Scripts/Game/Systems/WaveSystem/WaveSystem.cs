﻿using System;
using System.Linq;
using Core.Context;
using Core.Loggers;
using Core.Systems.TimeSystem;
using Core.Systems.UpdateSystem;
using Game.Enemies;
using Game.Levels;
using Game.Levels.Waves;
using Game.Managers.EnemyManager;
using Game.Misc;
using Game.Systems.EnemySystem;
using static Game.Data.Loggers.GameLogData;

namespace Game.Systems.WaveSystem
{
    public class WaveSystem : IWaveSystem, IGameContextInitializable
    {
        public event Action WaveStarted;
        public event Action WaveCompleted;
        public event Action WavesChanged;

        private LevelWaves _levelWaves;
        private LevelMap _levelMap;
        private int _currentWaveNumber;
        private EnemySpawner _enemySpawner;

        private IUpdateSystem _updateSystem;
        private IEnemySystem _enemySystem;
        private ITimeSystem _timeSystem;
        private IEnemyManager _enemyManager;
        private WaveData _currentWaveData;
        private readonly Counter _enemyCounter = new Counter();
        private readonly Counter _waveCounter = new Counter();
        private bool _waveIsActive;


        public WaveData CurrentWaveData => _currentWaveData;
        public int CurrentWaveNumber => _currentWaveNumber;
        public int WaveCount => _levelWaves.Waves.Count();
        public Counter EnemyCounter => _enemyCounter;
        public Counter WaveCounter => _waveCounter;
        public bool WaveIsActive => _waveIsActive;


        public void Initialization(IGameContext gameContext)
        {
            _updateSystem = gameContext.GetSystem<IUpdateSystem>();
            _enemySystem = gameContext.GetSystem<IEnemySystem>();
            _timeSystem = gameContext.GetSystem<ITimeSystem>();
            _enemyManager = gameContext.GetManager<IEnemyManager>();

            _enemySpawner = new EnemySpawner(_enemySystem, _timeSystem);
            _updateSystem.AddUpdatable(_enemySpawner);
            _enemyManager.EnemyMoveCompleted += OnEnemyRemoved;
            _enemyManager.EnemyDead += OnEnemyRemoved;
            _enemyCounter.ValueDepleted += EnemyCounterOnValueDepleted;
        }

        private void EnemyCounterOnValueDepleted()
        {
            WaveComplete();
        }

        private void OnEnemyRemoved(Enemy enemy)
        {
            _enemyCounter.DecreaseCount();
        }

        public void SetWaves(LevelWaves levelWaves, LevelMap levelMap)
        {
            _waveIsActive = false;
            _levelWaves = levelWaves;
            _levelMap = levelMap;
            _currentWaveNumber = 0;
            
            _waveCounter.SetMax(WaveCount);
            _waveCounter.SetCurrentToZero();

            WavesChanged?.Invoke();
        }

        public void StartWave()
        {
            _enemyCounter.SetMax(_levelWaves.Waves.ElementAt(_currentWaveNumber).EnemyCount);
            _enemyCounter.SetCurrentToMax();
            
            
            
            _waveCounter.IncreaseValue();
            _currentWaveNumber++;
            _waveIsActive = true;
            WaveSystemLogData.Log($"Start Wave: {_currentWaveNumber}");

            if (_currentWaveNumber > WaveCount)
                return;

            _currentWaveData = _levelWaves.Waves.ElementAt(_currentWaveNumber - 1);
            _enemySpawner.SetWave(_currentWaveData, _levelMap);

            WaveStarted?.Invoke();
        }

        public void ClearWave()
        {
            _enemySpawner.Reset();
        }

        public void WaveComplete()
        {
            if(_waveIsActive == false)
                return;
            
            _waveIsActive = false;
            
            WaveSystemLogData.Log($"Wave Completed {_currentWaveNumber}");
            WaveCompleted?.Invoke();
        }
    }
}