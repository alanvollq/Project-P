﻿using System.Collections.Generic;
using System.Linq;
using Core.Context;
using Core.Extensions;
using Core.Pool;
using Core.Systems.TimeSystem;
using Game.Data.Units.Enemies;
using Game.Enemies;
using UnityEngine;

namespace Game.Systems.EnemySystem
{
    public class EnemySystem : IEnemySystem, IGameContextInitializable
    {
        private IGameContext _gameContext;
        private readonly Dictionary<EnemyDataByQuality, ObjectsPool<Enemy>> _pools = new();
        private ITimeSystem _timeSystem;
        private Transform _enableEnemiesParent;
        private Transform _disableEnemiesParent;


        public void Initialization(IGameContext gameContext)
        {
            _gameContext = gameContext;
            _timeSystem = _gameContext.GetSystem<ITimeSystem>();
            _timeSystem.Played += TimeSystemOnPlayed;
            _timeSystem.Paused += TimeSystemOnPaused;

            var enemyParent = new GameObject("[ Enemies ]").transform;
            _enableEnemiesParent = new GameObject("[ Enable Enemies ]").transform;
            _disableEnemiesParent = new GameObject("[ Disable Enemies ]").transform;
            enemyParent.SetParent(_gameContext.RootOwner.GameRoot);
            _enableEnemiesParent.SetParent(enemyParent);
            _disableEnemiesParent.SetParent(enemyParent);
        }

        private void TimeSystemOnPlayed(float pauseTime)
        {
            _pools.Values
                .SelectMany(pool => pool.ActiveObjects)
                .ApplyToAll(enemy => enemy.Played(pauseTime));
        }

        private void TimeSystemOnPaused()
        {
            _pools.Values
                .SelectMany(pool => pool.ActiveObjects)
                .ApplyToAll(enemy => enemy.Pause());
        }

        public Enemy GetEnemy(EnemyDataByQuality enemyDataByQuality)
        {
            if (_pools.TryGetValue(enemyDataByQuality, out var pool) == false)
            {
                pool = new ObjectsPool<Enemy>(_enableEnemiesParent, _disableEnemiesParent, enemyDataByQuality.Prefab);
                _pools.Add(enemyDataByQuality, pool);
            }

            var enemy = pool.Get();
            return enemy;
        }

        public void ReturnEnemy(Enemy enemy)
        {
            _pools.Values.ApplyToAll(pool => pool.TryReturn(enemy));
        }

        public void ReturnAll()
        {
            _pools.Values.ApplyToAll(pool => pool.ReturnAll());
        }
    }
}