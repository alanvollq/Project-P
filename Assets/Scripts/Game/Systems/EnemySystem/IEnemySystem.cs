﻿using Core.Systems;
using Game.Data.Units.Enemies;
using Game.Enemies;

namespace Game.Systems.EnemySystem
{
    public interface IEnemySystem : ISystem
    {
        public Enemy GetEnemy(EnemyDataByQuality enemyDataByQuality);
        public void ReturnEnemy(Enemy enemy);
        public void ReturnAll();
    }
}