namespace Game.Systems.DamageSystem
{
    public class Damage
    {
        public IDamageDealer Owner;
        public float Value;
    }
}