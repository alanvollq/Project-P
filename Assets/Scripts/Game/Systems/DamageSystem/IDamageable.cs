namespace Game.Systems.DamageSystem
{
    public interface IDamageable
    {
        public DamageResult TakeDamage(Damage damage);
    }
}