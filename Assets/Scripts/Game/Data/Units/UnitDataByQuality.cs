﻿using Game.QualityTypes;
using UnityEngine;

namespace Game.Data.Units
{
    public abstract class UnitDataByQuality<TUnitData> : ScriptableObject where TUnitData : UnitData
    {
        [SerializeField] private TUnitData _unitData;
        [SerializeField] private QualityType _qualityType;

        
        public TUnitData UnitData => _unitData;
        public QualityType QualityType => _qualityType;
    }
}