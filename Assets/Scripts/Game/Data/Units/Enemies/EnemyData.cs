using UnityEngine;

namespace Game.Data.Units.Enemies
{
    [CreateAssetMenu(fileName = "Enemy Data", menuName = "Data/Game/Units/Enemies/Enemy Data")]
    public class EnemyData : UnitData
    {
    }
}