using Game.Enemies;
using UnityEngine;

namespace Game.Data.Units.Enemies
{
    [CreateAssetMenu(fileName = "Enemy Data By Quality", menuName = "Data/Game/Units/Enemies/Enemy Data By Quality")]
    public class EnemyDataByQuality : UnitDataByQuality<EnemyData>
    {
        [SerializeField] private Sprite _icon;
        [SerializeField] private EnemyStats _stats;
        [SerializeField] private Enemy _prefab;
        
        
        public Sprite Icon => _icon;
        public EnemyStats Stats => _stats;
        public Enemy Prefab => _prefab;
    }
}