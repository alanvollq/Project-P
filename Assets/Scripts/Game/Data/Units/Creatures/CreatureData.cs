﻿using Game.Data.Units.Creatures.Abilities;
using UnityEngine;

namespace Game.Data.Units.Creatures
{
    [CreateAssetMenu(fileName = "Creature Data", menuName = "Data/Game/Units/Creatures/Creature Data")]
    public class CreatureData : UnitData
    {
        [SerializeField] private AbilitySlotsData _abilitySlotsData;
        
        
        public AbilitySlotsData AbilitySlotsData => _abilitySlotsData;
    }
}