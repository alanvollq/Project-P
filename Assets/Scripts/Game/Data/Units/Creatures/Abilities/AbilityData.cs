using Core.Extensions;
using Game.Units;
using UnityEngine;

namespace Game.Data.Units.Creatures.Abilities
{
    public abstract class AbilityData : ScriptableObject
    {
        [SerializeField] private string _name;
        [SerializeField] private Sprite _icon;
        [SerializeField] private UnitType _unitType;


        public string Name => _name;
        public Sprite Icon => _icon;
        public UnitType UnitType => _unitType;

        public string GetDescription()
        {
            return
                $"Отравляет противника, каждый раунд нанося {"77".Color(Color.yellow)} ед." +
                $" Урон, который получает цель, увеличивается на {"25&".Color(Color.yellow)}" +
                $" на {"3".Color(Color.cyan)} раунда.";
        }
    }
}