﻿using System;
using UnityEngine;

namespace Game.Data.Units.Creatures.Abilities
{
    [Serializable]
    public class AbilitySlotData <T> where T : AbilityData
    {
        [SerializeField] private T _firstAbility;
        [SerializeField] private T _secondAbility;
        
        
        public T FirstAbility => _firstAbility;
        public T SecondAbility => _secondAbility;
    }
}