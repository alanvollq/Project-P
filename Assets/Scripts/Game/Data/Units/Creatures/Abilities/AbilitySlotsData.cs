﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace Game.Data.Units.Creatures.Abilities
{
    [Serializable]
    public class AbilitySlotsData
    {
        [SerializeField] private AbilitySlotData<WeaponAbilityData> _weaponAbilitySlotData;
        [SerializeField] private AbilitySlotData<NotWeaponAbilityData> _firstAbilitySlotData;
        [SerializeField] private AbilitySlotData<NotWeaponAbilityData> _secondAbilitySlotData;


        public AbilitySlotData<WeaponAbilityData> WeaponAbilitySlotData => _weaponAbilitySlotData;
        public AbilitySlotData<NotWeaponAbilityData> FirstAbilitySlotData => _firstAbilitySlotData;
        public AbilitySlotData<NotWeaponAbilityData> SecondAbilitySlotData => _secondAbilitySlotData;

        public IEnumerable<AbilityData> AllAbilities => GetAllAbilities();

        private IEnumerable<AbilityData> GetAllAbilities()
        {
            yield return _weaponAbilitySlotData.FirstAbility;
            yield return _weaponAbilitySlotData.SecondAbility;
            yield return _firstAbilitySlotData.FirstAbility;
            yield return _firstAbilitySlotData.SecondAbility;
            yield return _secondAbilitySlotData.FirstAbility;
            yield return _secondAbilitySlotData.SecondAbility;
        }
    }
}