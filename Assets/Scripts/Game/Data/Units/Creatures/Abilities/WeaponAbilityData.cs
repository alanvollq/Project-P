using Game.Charges;
using Game.Towers;
using UnityEngine;

namespace Game.Data.Units.Creatures.Abilities
{
    [CreateAssetMenu(fileName = "Weapon Ability Data", menuName = "Data/Game/Units/Creatures/Abilities/Weapon/Weapon Ability Data")]
    public class WeaponAbilityData : AbilityData
    {
        [SerializeField] private WeaponStats _stats;
        [SerializeField] private Charge _chargePrefab;

        
        public WeaponStats Stats => _stats;
        public Charge ChargePrefab => _chargePrefab;
    }
}