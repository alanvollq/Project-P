﻿using UnityEngine;

namespace Game.Data.Units.Creatures.Abilities
{
    [CreateAssetMenu(fileName = "Not Weapon Ability Data", menuName = "Data/Game/Units/Creatures/Abilities/Not Weapon/Not Weapon Data")]
    public class NotWeaponAbilityData : AbilityData
    {
    }
}