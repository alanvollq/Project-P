using UnityEngine;

namespace Game.Data.Units.Creatures.Abilities
{
    [CreateAssetMenu(fileName = "Active Ability Data", menuName = "Data/Game/Units/Creatures/Abilities/Active Ability Data")]
    public class ActiveAbilityData : NotWeaponAbilityData
    {
        
    }
}