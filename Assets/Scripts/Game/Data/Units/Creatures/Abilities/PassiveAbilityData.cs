using UnityEngine;

namespace Game.Data.Units.Creatures.Abilities
{
    [CreateAssetMenu(fileName = "Passive Ability Data", menuName = "Data/Game/Units/Creatures/Abilities/Passive Ability Data")]
    public class PassiveAbilityData : NotWeaponAbilityData
    {
        
    }
}