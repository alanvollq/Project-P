using Game.Creatures;
using Game.Systems.BuildSystem;
using UnityEngine;

namespace Game.Data.Units.Creatures
{
    [CreateAssetMenu(fileName = "Creature Data By Quality", menuName = "Data/Game/Creatures/Creature Data By Quality")]
    public class CreatureDataByQuality : UnitDataByQuality<CreatureData>
    {
        [SerializeField] private Sprite _icon;
        [SerializeField] private CreatureController _creatureControllerPrefab;
        [SerializeField] private GameObject _preview;
        
        
        public Sprite Icon => _icon;
        public GameObject Preview => _preview;
        public CreatureController CreatureControllerPrefab => _creatureControllerPrefab;
    }
}