﻿using Game.Units;
using UnityEngine;

namespace Game.Data.Units
{
    public abstract class UnitData : ScriptableObject
    {
        [SerializeField] private string _name;
        [SerializeField] private string _description;
        [SerializeField] private UnitType _unitType;


        public string Name => _name;
        public string Description => _description;
        public UnitType UnitType => _unitType;
    }
}