﻿using Core;
using Core.Data;
using Game.UI;
using Game.UI.UIElements;
using Game.UI.WorldMarkManager;
using UnityEngine;

namespace Game.Data
{
    [CreateAssetMenu(menuName = "Data/Game/Managers/World Mark Prefabs Data", fileName = "World Mark Prefabs Data")]
    public class WorldMarkPrefabsData : DataContainer<SerializablePair<WorldMarkType, WorldMarkView>>
    {
        
    }
}