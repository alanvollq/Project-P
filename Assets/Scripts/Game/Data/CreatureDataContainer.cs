﻿using Core.Data;
using Game.Creatures;
using Game.Data.Units.Creatures;
using UnityEngine;

namespace Game.Data
{
    [CreateAssetMenu(fileName = "Creature Data Container", menuName = "Data/Game/Creatures/Creature Data Container")]
    public class CreatureDataContainer : DataContainer<CreatureDataByQuality>
    {
        
    }
}