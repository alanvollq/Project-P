﻿using Core.Data;
using Game.Units;
using UnityEngine;

namespace Game.Data
{
    [CreateAssetMenu(fileName = "Unit Type Data Container", menuName = "Data/Game/Unit Type Data Container")]
    public class UnitTypeDataContainer : DataContainer<UnitType>
    {
        
    }
}