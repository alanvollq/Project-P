﻿using Core.Data;
using Game.Data.Units.Enemies;
using Game.Enemies;
using UnityEngine;

namespace Game.Data
{
    [CreateAssetMenu(fileName = "Enemies Data Container",
        menuName = "Data/Game/Data Containers/Enemies Data Container")]
    public class EnemiesDataContainer : DataContainer<EnemyDataByQuality>
    {
    }
}