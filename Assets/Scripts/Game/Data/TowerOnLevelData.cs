﻿using System.Collections.Generic;
using Game.Creatures;
using Game.Data.Units.Creatures;
using Game.Inventory;
using Game.UI.UIElements.ScrollViews;
using UnityEngine;

namespace Game.Data
{
    [CreateAssetMenu(fileName = "Tower On Level Data", menuName = "Data/Game/Tower On Level Data")]
    public class TowerOnLevelData : ScriptableObject
    {
        [SerializeField] private List<CreatureDataByQuality> _towerInventoryItems;
        

        public List<CreatureDataByQuality> TowerInventoryItems => _towerInventoryItems;


        public void Clear()
        {
            // _towerInventoryItems.ForEach(item => item.SetOnLevel(false));
            _towerInventoryItems.Clear();
        }

        public void SetItems(List<TowerSlot> towerSlots)
        {
            towerSlots.ForEach(item => _towerInventoryItems.Add(item.CreatureDataByQuality));
        }
    }
}