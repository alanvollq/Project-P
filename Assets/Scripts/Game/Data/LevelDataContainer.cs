﻿using System.Linq;
using Core.Data;
using Game.Levels;
using UnityEngine;

namespace Game.Data
{
    [CreateAssetMenu(fileName = "Level Data Container", menuName = "Data/Game/Levels/Level Data Container")]
    public class LevelDataContainer : DataContainer<LevelData>
    {
        public void Load()
        {
            foreach (var levelData in Data)
                levelData.Load();
        }
    }
}