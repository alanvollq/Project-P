using System;
using Core.Data;
using UnityEngine;

namespace Game.Data.Loggers
{
    [CreateAssetMenu(fileName = "Game Log Data", menuName = "Data/Loggers/Game Log Data")]
    public class GameLogData : ScriptableObject
    {
        [SerializeField] private SOLogData _defaultLogData;
        [SerializeField] private UILogData _uiLogData;
        [SerializeField] private SOLogData _clickHandlerLogData;
        [SerializeField] private SOLogData _dragHandlerLogData;
        [SerializeField] private SOLogData _towerLogData;
        [SerializeField] private SOLogData _chargeLogData;
        [SerializeField] private SOLogData _enemyLogData;
        [SerializeField] private SOLogData _enemySpawnerLogData;
        [SerializeField] private SOLogData _buildLogData;
        [SerializeField] private SOLogData _targetHandlerLogData;
        [SerializeField] private SOLogData _waveSystemLogData;
        [SerializeField] private SOLogData _levelCurrencyManagerLogData;
        [SerializeField] private SOLogData _levelStateMachineLogData;
        [SerializeField] private SOLogData _playStateMachineLogData;

        private static GameLogData _instance;


        public static SOLogData DefaultLogData => _instance._defaultLogData;
        public static UILogData UILogData => _instance._uiLogData;
        public static SOLogData ClickHandlerLogData => _instance._clickHandlerLogData;
        public static SOLogData DragHandlerLogData => _instance._dragHandlerLogData;
        public static SOLogData TowerLogData => _instance._towerLogData;
        public static SOLogData ChargeLogData => _instance._chargeLogData;
        public static SOLogData EnemyLogData => _instance._enemyLogData;
        public static SOLogData EnemySpawnerLogData => _instance._enemySpawnerLogData;
        public static SOLogData BuildLogData => _instance._buildLogData;
        public static SOLogData TargetHandlerLogData => _instance._targetHandlerLogData;
        public static SOLogData WaveSystemLogData => _instance._waveSystemLogData;
        public static SOLogData LevelCurrencyManagerLogData => _instance._levelCurrencyManagerLogData;
        public static SOLogData LevelStateMachineLogData => _instance._levelStateMachineLogData;
        public static SOLogData PlayStateMachineLogData => _instance._playStateMachineLogData;


        public void Init()
        {
            _instance = this;
        }
    }

    [Serializable]
    public class UILogData
    {
        [SerializeField] private SOLogData _logData;
        [SerializeField] private SOLogData _pageLogData;
        [SerializeField] private SOLogData _popupLogData;
        [SerializeField] private SOLogData _messageboxLogData;


        public SOLogData Default => _logData;
        public SOLogData Page => _pageLogData;
        public SOLogData Popup => _popupLogData;
        public SOLogData Messagebox => _messageboxLogData;
    }
}