﻿using Core.Systems.EventSystem;

namespace Game.Events.Waves
{
    public record TryStartWaveEvent() : IEvent;
}