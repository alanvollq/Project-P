﻿namespace Game.UI.WorldMarkManager
{
    public enum WorldMarkType
    {
        Spawner,
        EndPoint
    }
}