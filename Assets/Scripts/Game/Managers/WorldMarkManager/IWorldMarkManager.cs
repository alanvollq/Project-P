﻿using Core.Managers;

namespace Game.Managers.WorldMarkManager
{
    public interface IWorldMarkManager : IManager
    {
        public void AddMark(WorldMark worldMark);
        public void RemoveMark(WorldMark worldMark);
    }
}