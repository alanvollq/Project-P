﻿using System.Collections.Generic;
using System.Linq;
using Core.Context;
using Core.Extensions;
using Core.Systems.UI.PopupSystem;
using Core.UI;
using Game.Data;
using Game.UI;
using Game.UI.UIElements;
using UnityEngine;

namespace Game.Managers.WorldMarkManager
{
    public class WorldMarkManager : IWorldMarkManager, IGameContextInitializable
    {
        private readonly WorldMarkPrefabsData _worldMarkPrefabsData;
        private RectTransform _markParent;
        private IGameContext _gameContext;
        private IPopupSystem _popupSystem;

        private readonly Dictionary<WorldMark, WorldMarkView> _worldMarks = new();


        public WorldMarkManager(WorldMarkPrefabsData worldMarkPrefabsData)
        {
            _worldMarkPrefabsData = worldMarkPrefabsData;
        }

        public void Initialization(IGameContext gameContext)
        {
            _gameContext = gameContext;
            _popupSystem = gameContext.GetSystem<IPopupSystem>();
            _popupSystem.PopupOpened += OnPopupOpened;
            _popupSystem.PopupClosed += OnPopupClosed;

            var parentObject = new GameObject("[ World Marks ]");
            
            _markParent = parentObject.AddComponent<RectTransform>();
            _markParent.SetParent(gameContext.RootOwner.UIRoot.transform);
            _markParent.SetDefaultMaxStretch();
            parentObject.AddComponent<SafeArea>();
        }

        private void OnPopupOpened(BasePopup _) => SetMarkCanShow(false);

        private void OnPopupClosed(BasePopup _) => SetMarkCanShow(_popupSystem.HasOpenedPopup == false);

        private void SetMarkCanShow(bool value)
        {
            foreach (var worldMarkView in _worldMarks)
                worldMarkView.Value.SetShowAvailable(value);
        }

        public void AddMark(WorldMark worldMark)
        {
            if (worldMark == null)
                return;

            var prefab = _worldMarkPrefabsData.Data.First(item => item.Key == worldMark.MarkType).Value;
            var worldMarkView = Object.Instantiate(prefab, _markParent);
            worldMarkView.Init(_gameContext);
            worldMarkView.SetItem(worldMark.transform);
            worldMarkView.SetShowAvailable(_popupSystem.HasOpenedPopup == false);
            _worldMarks.Add(worldMark, worldMarkView);
        }

        public void RemoveMark(WorldMark worldMark)
        {
            if (worldMark == null)
                return;

            if (_worldMarks.ContainsKey(worldMark) == false)
                return;

            _worldMarks.Remove(worldMark);
        }
    }
}