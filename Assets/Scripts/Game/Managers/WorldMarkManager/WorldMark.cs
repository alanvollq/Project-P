﻿using Game.UI.WorldMarkManager;
using UnityEngine;

namespace Game.Managers.WorldMarkManager
{
    public class WorldMark : MonoBehaviour
    {
        [SerializeField] private WorldMarkType _markType;

        
        public WorldMarkType MarkType => _markType;
    }
}