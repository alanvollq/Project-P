using System;
using Core.Managers;

namespace Game.Managers.LevelCurrencyManager
{
    public interface ILevelCurrencyManager : IManager
    {
        public event Action<int> CurrencyValueChanged;
        public event Action<int> CurrencyAdded; 
        public event Action<int> CurrencyRemoved; 
            
        
        public int CurrentCurrencyValue { get; }
        
        
        public bool TrySetCurrencyValue(int value);
        public bool TryAddCurrencyValue(int value);
        public bool TryGetCurrencyValue(int value);
    }
}