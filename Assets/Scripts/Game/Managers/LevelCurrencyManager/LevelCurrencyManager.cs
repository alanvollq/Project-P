﻿using System;
using Core.Loggers;
using static Game.Data.Loggers.GameLogData;

namespace Game.Managers.LevelCurrencyManager
{
    public class LevelCurrencyManager : ILevelCurrencyManager
    {
        public event Action<int> CurrencyValueChanged;
        public event Action<int> CurrencyAdded;
        public event Action<int> CurrencyRemoved;

        private int _currentCurrencyValue;


        public int CurrentCurrencyValue => _currentCurrencyValue;


        public bool TrySetCurrencyValue(int value)
        {
            LevelCurrencyManagerLogData.Log($"Try set currency: {value}. Current value: {_currentCurrencyValue}");
            if (value < 0)
                return false;

            var difference = value - _currentCurrencyValue;
            var changeEvent = difference switch
            {
                > 0 => CurrencyAdded,
                < 0 => CurrencyRemoved,
                _ => null
            };
            
            _currentCurrencyValue = value;
            
            changeEvent?.Invoke(difference);
            CurrencyValueChanged?.Invoke(_currentCurrencyValue);
            
            return true;
        }

        public bool TryAddCurrencyValue(int value)
        {
            return
                value > 0 &&
                TrySetCurrencyValue(_currentCurrencyValue + value);
        }

        public bool TryGetCurrencyValue(int value)
        {
            return 
                value > 0 &&
                TrySetCurrencyValue(_currentCurrencyValue - value);
        }
    }
}