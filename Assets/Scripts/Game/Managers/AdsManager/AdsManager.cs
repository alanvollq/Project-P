﻿using System;

namespace Game.Managers.AdsManager
{
    public class AdsManager : IAdsManager
    {
        private Action _successAction;
        private Action _failureAction;
        

        public void ShowAds(Action successAction, Action failureAction)
        {
            _successAction = successAction;
            _failureAction = failureAction;
        }

        //TODO: Remove after Tests.
        public void ShowAds(Action successAction, Action failureAction, bool result)
        {
            (result ? successAction : failureAction)?.Invoke();
        }
    }
}