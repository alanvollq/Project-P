﻿using System;
using Core.Managers;

namespace Game.Managers.AdsManager
{
    public interface IAdsManager : IManager
    {
        public void ShowAds(Action successAction, Action failureAction);
        
        
        //TODO: For Tests
        public void ShowAds(Action successAction, Action failureAction, bool result);
    }
}