﻿using System;
using Core.Managers;
using Game.Enemies;

namespace Game.Managers.EnemyManager
{
    public interface IEnemyManager : IManager
    {
        public event Action<Enemy> EnemyMoveCompleted;
        public event Action<Enemy> EnemyDead;
    }
}