﻿using System;
using Game.Enemies;

namespace Game.Managers.EnemyManager
{
    public class EnemyManager : IEnemyManager
    {
        public event Action<Enemy> EnemyMoveCompleted;
        public event Action<Enemy> EnemyDead;


        public EnemyManager()
        {
            Enemy.MoveCompleted += EnemyOnMoveCompleted;
            Enemy.Death += EnemyOnDeath;
        }

        private void EnemyOnDeath(Enemy enemy)
        {
            EnemyDead?.Invoke(enemy);
        }

        private void EnemyOnMoveCompleted(Enemy enemy)
        {
            EnemyMoveCompleted?.Invoke(enemy);
        }
    }
}