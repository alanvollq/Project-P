﻿using Core.Context;
using Core.Loggers;
using Core.StateMachine;
using Core.Systems.UpdateSystem;
using Game.Levels.MainCampaign;
using Game.Managers.GameSoundManager;
using Game.Managers.GameStateManager.States;
using Game.Systems.LevelSystem;
using static Game.Data.Loggers.GameLogData;

namespace Game.Managers.GameStateManager
{
    public partial class GameStateManager : IGameStateManager, IUpdatable, IGameContextInitializable
    {
        private IGameContext _gameContext;
        private readonly FiniteStateMachine<GameStateId, GameStateTriggerId> _finiteStateMachine = new();
        private IUpdateSystem _updateSystem;
        private IGameSoundManager _gameSoundManager;
        private ILevelSystem _levelSystem;


        public void Initialization(IGameContext gameContext)
        {
            _gameContext = gameContext;
            _updateSystem = gameContext.GetSystem<IUpdateSystem>();
            _updateSystem.AddUpdatable(this);

            _gameSoundManager = gameContext.GetManager<IGameSoundManager>();

            CreateStateMachine();

            _finiteStateMachine.StateChanged += OnFiniteStateChanged;
        }

        private void OnFiniteStateChanged()
        {
            var fromMessage = _finiteStateMachine.PreviousState is not null
                ? $"{_finiteStateMachine.PreviousState.Id}"
                : "_Enable";

            PlayStateMachineLogData
                .Log($"State changed - from: {fromMessage} to: {_finiteStateMachine.CurrentState.Id}");

            var previousState = _finiteStateMachine.PreviousState;
            var currentState = _finiteStateMachine.CurrentState;

            if (currentState.Id == GameStateId.LevelPlay)
                _gameSoundManager.PlayBattleTheme();
            else if (previousState == null || previousState.Id == GameStateId.LevelPlay)
                _gameSoundManager.PlayMainTheme();
        }

        public void StartGame()
        {
            _finiteStateMachine.Enable();
        }

        public void StartNewLevel()
        {
            _finiteStateMachine.InvokeTrigger(GameStateTriggerId.NewLevel);
        }

        public void CompleteLevel()
        {
            _finiteStateMachine.InvokeTrigger(GameStateTriggerId.EndLevel);
        }

        public void OpenCreatureLibrary()
        {
            _finiteStateMachine.InvokeTrigger(GameStateTriggerId.OpenCreaturesLibrary);
        }

        public void OpenEnemyLibrary()
        {
            _finiteStateMachine.InvokeTrigger(GameStateTriggerId.OpenEnemiesLibrary);
        }

        public void OpenStore()
        {
            _finiteStateMachine.InvokeTrigger(GameStateTriggerId.OpenStore);
        }

        public void OpenResearching()
        {
            _finiteStateMachine.InvokeTrigger(GameStateTriggerId.OpenResearching);
        }

        public void OpenMenu()
        {
            _finiteStateMachine.InvokeTrigger(GameStateTriggerId.OpenMenu);
        }


        public void OpenStage(StageData stageData)
        {
            _levelSystem.SelectState(stageData);
            _finiteStateMachine.InvokeTrigger(GameStateTriggerId.SelectedStage);
        }
    }
}