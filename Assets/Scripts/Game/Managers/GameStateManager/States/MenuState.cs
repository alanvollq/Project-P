﻿
using Core.StateMachine.States;
using Core.Systems.UI.PageSystem;
using Game.UI.Pages;

namespace Game.Managers.GameStateManager.States
{
    public class MenuState : State<GameStateId>
    {
        private readonly IPageSystem _pageSystem;
        
        
        public MenuState(IPageSystem pageSystem) : base(GameStateId.Menu)
        {
            _pageSystem = pageSystem;
        }
        
        
        protected override void OnEnter()
        {
            _pageSystem.Open<MainPage>();
        }
    }
}