﻿using Core.StateMachine.States;
using Core.Systems.UI.PageSystem;
using Game.UI.Pages;

namespace Game.Managers.GameStateManager.States
{
    public class ViewingCreatureLibraryState : State<GameStateId>
    {
        private readonly IPageSystem _pageSystem;


        public ViewingCreatureLibraryState(IPageSystem pageSystem) : base(GameStateId.ViewingCreatureLibrary)
        {
            _pageSystem = pageSystem;
        }


        protected override void OnEnter()
        {
            _pageSystem.Open<CreaturesLibraryPage>();
        }
    }
}