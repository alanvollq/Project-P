﻿using Core.StateMachine.States;
using Core.Systems.UI.PageSystem;
using Game.UI.Pages;

namespace Game.Managers.GameStateManager.States
{
    public class ViewingEnemyLibraryState : State<GameStateId>
    {
        private readonly IPageSystem _pageSystem;


        public ViewingEnemyLibraryState(IPageSystem pageSystem) : base(GameStateId.ViewingEnemyLibrary)
        {
            _pageSystem = pageSystem;
        }


        protected override void OnEnter()
        {
            _pageSystem.Open<EnemiesLibraryPage>();
        }
    }
}