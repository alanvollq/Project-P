﻿using Core.StateMachine.States;
using Core.Systems.UI.PageSystem;
using Game.UI.Pages;

namespace Game.Managers.GameStateManager.States
{
    public class ResearchingState : State<GameStateId>
    {
        private readonly IPageSystem _pageSystem;


        public ResearchingState(IPageSystem pageSystem) : base(GameStateId.Researching)
        {
            _pageSystem = pageSystem;
        }


        protected override void OnEnter()
        {
            _pageSystem.Open<ResearchPage>();
        }
    }
}