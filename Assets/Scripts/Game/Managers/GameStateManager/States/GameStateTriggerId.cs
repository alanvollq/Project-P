﻿namespace Game.Managers.GameStateManager.States
{
    public enum GameStateTriggerId
    {
        NewLevel,
        OpenCreaturesLibrary,
        OpenEnemiesLibrary,
        OpenResearching,
        EndLevel,
        SelectedStage,
        OpenStore,
        OpenMenu
    }
}