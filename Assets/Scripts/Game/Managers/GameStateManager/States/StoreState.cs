﻿using Core.StateMachine.States;
using Core.Systems.UI.PageSystem;
using Game.UI.Pages;

namespace Game.Managers.GameStateManager.States
{
    public class StoreState : State<GameStateId>
    {
        private readonly IPageSystem _pageSystem;
        
        
        public StoreState(IPageSystem pageSystem) : base(GameStateId.Store)
        {
            _pageSystem = pageSystem;
        }
        
        
        protected override void OnEnter()
        {
            _pageSystem.Open<StorePage>();
        }
    }
}