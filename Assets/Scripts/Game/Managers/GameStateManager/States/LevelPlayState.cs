﻿using Core.StateMachine.States;
using Core.Systems.UI.PageSystem;
using Game.Systems.CreatureSystem;
using Game.Systems.EnemySystem;
using Game.Systems.LevelSystem;
using Game.Systems.WaveSystem;
using Game.UI.Pages;
using Game.UI.Pages.GamePage;

namespace Game.Managers.GameStateManager.States
{
    public class LevelPlayState: State<GameStateId>
    {
        private readonly IPageSystem _pageSystem;
        private readonly IEnemySystem _enemySystem;
        private readonly ICreatureSystem _creatureSystem;
        private readonly ILevelSystem _levelSystem;
        private readonly IWaveSystem _waveSystem;

        public LevelPlayState(
            IPageSystem pageSystem,
            IEnemySystem enemySystem,
            ILevelSystem levelSystem,
            IWaveSystem waveSystem, 
            ICreatureSystem creatureSystem
            ) : base(GameStateId.LevelPlay)
        {
            _pageSystem = pageSystem;
            _enemySystem = enemySystem;
            _levelSystem = levelSystem;
            _waveSystem = waveSystem;
            _creatureSystem = creatureSystem;
        }
        
        protected override void OnEnter()
        {
            _pageSystem.Open<GamePage>();
            _levelSystem.LoadLevel();
            _waveSystem.SetWaves(_levelSystem.LevelData.LevelWaves, _levelSystem.LevelMap);
        }

        protected override void OnExit()
        {
            _enemySystem.ReturnAll();
            _creatureSystem.ReturnAll();
            _pageSystem.Open<MainPage>();
            _levelSystem.UnloadLevel();
        }

        protected override void OnExecute()
        {
             
        }
    }
}