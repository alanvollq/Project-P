﻿namespace Game.Managers.GameStateManager.States
{
    public enum GameStateId
    {
        Menu,
        ViewingCreatureLibrary,
        ViewingEnemyLibrary,
        Researching,
        LevelSelection,
        LevelPlay,
        Store
    }
}