﻿using Core.StateMachine.States;
using Core.Systems.UI.PageSystem;
using Core.Systems.UI.PopupSystem;
using Game.Levels;
using Game.Systems.LevelSystem;
using Game.UI.Pages;
using Game.UI.Popups.LevelInfo;
using UnityEngine;

namespace Game.Managers.GameStateManager.States
{
    public class LevelSelectionState : State<GameStateId>
    {
        private readonly IPageSystem _pageSystem;
        private readonly IPopupSystem _popupSystem;
        private readonly ILevelSystem _levelSystem;


        public LevelSelectionState(IPageSystem pageSystem, IPopupSystem popupSystem, ILevelSystem levelSystem) : base(GameStateId.LevelSelection)
        {
            _pageSystem = pageSystem;
            _popupSystem = popupSystem;
            _levelSystem = levelSystem;
        }

        protected override void OnEnter()
        {
            _levelSystem.LevelSelected += LevelSystemOnLevelSelected;
            
            _pageSystem.Open<StagePage>(new CampaignStagePageOpenParam(_levelSystem.SelectedStage));
        }

        protected override void OnExit()
        {
            _levelSystem.LevelSelected -= LevelSystemOnLevelSelected;
        }
        
        private void LevelSystemOnLevelSelected()
        {
            var openParam = new LevelInfoPopupOpenParam(_levelSystem.LevelData);
            _popupSystem.Open<LevelInfoPopup>(openParam);
        }
    }
}