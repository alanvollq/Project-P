﻿using Core.Systems.UI.PageSystem;
using Core.Systems.UI.PopupSystem;
using Game.Managers.GameStateManager.States;
using Game.Systems.CreatureSystem;
using Game.Systems.EnemySystem;
using Game.Systems.LevelSystem;
using Game.Systems.WaveSystem;
using Game.UI.Pages;

namespace Game.Managers.GameStateManager
{
    public partial class GameStateManager
    {
        private void CreateStateMachine()
        {
            CreateStates();
            CreateTransitions();

            _finiteStateMachine.SetStartState(GameStateId.Menu);
        }

        private void CreateStates()
        {
            var pageSystem = _gameContext.GetSystem<IPageSystem>();
            var popupSystem = _gameContext.GetSystem<IPopupSystem>();
            var enemySystem = _gameContext.GetSystem<IEnemySystem>();
            var waveSystem = _gameContext.GetSystem<IWaveSystem>();
            _levelSystem = _gameContext.GetSystem<ILevelSystem>();
            var creatureSystem = _gameContext.GetSystem<ICreatureSystem>();

            var menuState = new MenuState(pageSystem);
            var levelPlayState = new LevelPlayState(pageSystem, enemySystem, _levelSystem, waveSystem, creatureSystem);
            var levelSelectionState = new LevelSelectionState(pageSystem, popupSystem, _levelSystem);
            var creatureLibraryState = new ViewingCreatureLibraryState(pageSystem);
            var enemyLibraryState = new ViewingEnemyLibraryState(pageSystem);
            var researchingState = new ResearchingState(pageSystem);
            var storeState = new StoreState(pageSystem);

            _finiteStateMachine
                .AddState(menuState)
                .AddState(levelPlayState)
                .AddState(levelSelectionState)
                .AddState(creatureLibraryState)
                .AddState(enemyLibraryState)
                .AddState(researchingState)
                .AddState(storeState)
                ;
        }

        private void CreateTransitions()
        {
            _finiteStateMachine
                .AddTransition(GameStateId.LevelPlay, GameStateTriggerId.NewLevel)
                .AddTransition(GameStateId.Menu, GameStateTriggerId.OpenMenu)
                .AddTransition(GameStateId.ViewingCreatureLibrary, GameStateTriggerId.OpenCreaturesLibrary)
                .AddTransition(GameStateId.ViewingEnemyLibrary, GameStateTriggerId.OpenEnemiesLibrary)
                .AddTransition(GameStateId.Store, GameStateTriggerId.OpenStore)
                .AddTransition(GameStateId.Researching, GameStateTriggerId.OpenResearching)
                .AddTransition(GameStateId.Menu, GameStateTriggerId.EndLevel)
                .AddTransition(GameStateId.LevelSelection, GameStateTriggerId.SelectedStage)
                ;
        }

        public void Update()
        {
            _finiteStateMachine.Update();
        }
    }
}