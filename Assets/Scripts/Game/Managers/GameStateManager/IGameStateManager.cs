﻿using Core.Managers;
using Game.Levels.MainCampaign;

namespace Game.Managers.GameStateManager
{
    public interface IGameStateManager : IManager
    {
        void StartGame();
        void StartNewLevel();
        void CompleteLevel();

        public void OpenCreatureLibrary();
        public void OpenEnemyLibrary();
        public void OpenStore();
        public void OpenResearching();
        public void OpenMenu();

        public void OpenStage(StageData stageData);
    }
}