﻿using System.Collections.Generic;
using Game.Levels.Waves;
using Game.Misc;

namespace Game.Managers.WaveManager
{
    public class WaveManager : IWaveManager
    {
        public IEnumerable<WaveData> Waves { get; }
        public WaveData CurrentWaveData { get; }
        public Counter WavesCounter { get; }
        public Counter EnemyOnCurrentWaveCounter { get; }
    }
}