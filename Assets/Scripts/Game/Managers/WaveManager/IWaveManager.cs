﻿using System.Collections.Generic;
using Core.Managers;
using Game.Levels.Waves;
using Game.Misc;

namespace Game.Managers.WaveManager
{
    public interface IWaveManager : IManager
    {
        public IEnumerable<WaveData> Waves { get; }
        public WaveData CurrentWaveData { get; }
        public Counter WavesCounter { get; }
        public Counter EnemyOnCurrentWaveCounter { get; }
    }
}