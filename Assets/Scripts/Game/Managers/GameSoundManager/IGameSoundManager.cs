﻿using Core.Managers;

namespace Game.Managers.GameSoundManager
{
    public interface IGameSoundManager : IManager
    {
       public void PlayMainTheme();
       public void PlayBattleTheme();
    }
}