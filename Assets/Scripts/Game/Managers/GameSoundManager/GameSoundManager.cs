﻿using Core.Systems.SoundSystem.SoundTracks;

namespace Game.Managers.GameSoundManager
{
    public class GameSoundManager : IGameSoundManager
    {
        private readonly ISoundTrack _mainSoundTrack;
        private readonly ISoundTrack _battleSoundTrack;

        
        public GameSoundManager(ISoundTrack mainSoundTrack, ISoundTrack battleSoundTrack)
        {
            _mainSoundTrack = mainSoundTrack;
            _battleSoundTrack = battleSoundTrack;
        }


        public void PlayMainTheme()
        {
            _mainSoundTrack.Play();
        }

        public void PlayBattleTheme()
        {
            _battleSoundTrack.Play();
        }
    }
}