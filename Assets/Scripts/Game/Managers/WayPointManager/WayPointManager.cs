﻿using System.Collections.Generic;
using Core.Context;
using Core.Systems.UpdateSystem;
using Game.Levels;
using Game.Systems.BuildSystem;
using Game.Systems.LevelSystem;
using Game.Systems.PathSystem;
using UnityEngine;

namespace Game.Managers.WayPointManager
{
    public class WayPointManager : IWayPointManager, IGameContextInitializable
    {
        private readonly WayPointManagerSettings _settings;

        private ILevelSystem _levelSystem;
        private IPathSystem _pathSystem;
        private IUpdateSystem _updateSystem;
        private Transform _dotParent;
        private List<WayPointDotSpawner> _wayPointDotSpawners = new();
        private IBuildSystem _buildSystem;


        public WayPointManager(WayPointManagerSettings settings)
        {
            _settings = settings;
        }

        public void Initialization(IGameContext gameContext)
        {
            _dotParent = new GameObject("[ Way Point Dots]").transform;
            _dotParent.SetParent(gameContext.RootOwner.GameRoot);

            _pathSystem = gameContext.GetSystem<IPathSystem>();
            _levelSystem = gameContext.GetSystem<ILevelSystem>();
            _updateSystem = gameContext.GetSystem<IUpdateSystem>();
            _buildSystem = gameContext.GetSystem<IBuildSystem>();
            
            _levelSystem.LevelLoaded += LevelSystemOnLevelLoaded;
            _levelSystem.LevelUnloaded += LevelSystemOnLevelUnloaded;
            
            _buildSystem.Built += BuildSystemOnBuilt;
        }

        private void BuildSystemOnBuilt()
        {
            foreach (var wayPointDotSpawner in _wayPointDotSpawners)
            {
                wayPointDotSpawner.RecalculatePath();
            }
        }

        private void LevelSystemOnLevelLoaded()
        {
            foreach (var spawnPoint in _levelSystem.LevelMap.SpawnPoints)
            {
                var wayPointDotSpawner = new WayPointDotSpawner(
                    _dotParent, _settings.WayPointDotPrefab, _settings.Interval,
                    spawnPoint, _levelSystem.LevelMap.EndPoint, _pathSystem);

                _updateSystem.AddUpdatable(wayPointDotSpawner);
                _wayPointDotSpawners.Add(wayPointDotSpawner);
            }
        }

        private void LevelSystemOnLevelUnloaded()
        {
            foreach (var wayPointDotSpawner in _wayPointDotSpawners)
                _updateSystem.RemoveUpdatable(wayPointDotSpawner);

            _wayPointDotSpawners.Clear();
        }
    }
}