﻿using UnityEngine;

namespace Game.Managers.WayPointManager
{
    [CreateAssetMenu(menuName = "Data/Game/Managers/Way Point Manager Settings", fileName = "WayPointManagerSettings")]
    public class WayPointManagerSettings : ScriptableObject
    {
        [SerializeField] private WayPointDot _wayPointDotPrefab;
        [SerializeField] private float _interval;


        public WayPointDot WayPointDotPrefab => _wayPointDotPrefab;
        public float Interval => _interval;
    }
}