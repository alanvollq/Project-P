﻿using System;

namespace Game.Managers.LevelHealthManager
{
    public class LevelHealthManager : ILevelHealthManager
    {
        public event Action<int> HealthValueChanged;
        public event Action HealthDepleted;

        private int _currentHealthValue;


        public int CurrentHealthValue => _currentHealthValue;
        public bool HasHealth => _currentHealthValue > 0;


        public void SetHealthValue(int value)
        {
            if (value < 0)
                return;

            _currentHealthValue = value;
            if (_currentHealthValue < 0)
                _currentHealthValue = 0;

            HealthValueChanged?.Invoke(_currentHealthValue);

            if (_currentHealthValue < 1)
                HealthDepleted?.Invoke();
        }

        public void AddHealthValue(int value)
        {
            SetHealthValue(_currentHealthValue + value);
        }

        public void RemoveHealthValue(int value)
        {
                SetHealthValue(_currentHealthValue - value);
        }
    }
}