using System;
using Core.Managers;

namespace Game.Managers.LevelHealthManager
{
    public interface ILevelHealthManager : IManager
    {
        public event Action<int> HealthValueChanged;
        public event Action HealthDepleted;
            
        
        public int CurrentHealthValue { get; }
        public bool HasHealth { get; }
        
        
        public void SetHealthValue(int value);
        public void AddHealthValue(int value);
        public void RemoveHealthValue(int value);
    }
}