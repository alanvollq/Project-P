﻿using System;
using System.Collections.Generic;
using Core.Pool;
using UnityEngine;

namespace Game
{
    public class ClickVisualEffectHandler : MonoBehaviour
    {
        [SerializeField] private ClickEffect _clickEffectPrefab;

        private ObjectsPool<ClickEffect> _pool;
        private Transform _transform;
        private List<ClickEffect> _activeEffect;


        private void Awake()
        {
            _transform = transform;
            _pool = new ObjectsPool<ClickEffect>(_transform, _transform, _clickEffectPrefab);
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(0) == false)
                return;

            var position = Input.mousePosition;
            var effect = _pool.Get();
            effect.transform.position = position;
            effect.Completed += EffectOnCompleted;
        }

        private void EffectOnCompleted(ClickEffect clickEffect)
        {
            clickEffect.Completed -= EffectOnCompleted;
            _pool.Return(clickEffect);
        }
    }
}