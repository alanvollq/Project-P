﻿using System.Collections.Generic;
using Core.Extensions;
using Core.Pool;
using Core.Systems.UpdateSystem;
using Game.Systems.PathSystem;
using UnityEngine;

namespace Game
{
    public class WayPointDotSpawner : IUpdatable
    {
        private readonly ObjectsPool<WayPointDot> _dotPool;
        private readonly float _interval;
        private readonly Vector3 _spawnPoint;
        private readonly Vector3 _endPoint;
        private readonly IPathSystem _pathSystem;
        private Vector3[] _path;
        private float _distance;
        private float _leftTimeToSpawn;
        private readonly float _dotSpeed;
        private readonly List<WayPointDot> _dots = new ();


        public WayPointDotSpawner(
            Transform dotParent, WayPointDot dotPrefab, float interval,
            Transform spawnPoint, Transform endPoint, IPathSystem pathSystem)
        {
            _spawnPoint = spawnPoint.position;
            _endPoint = endPoint.position;
            _pathSystem = pathSystem;
            _interval = interval;
            _dotPool = new ObjectsPool<WayPointDot>(dotParent, dotParent, dotPrefab);
            _dotSpeed = dotPrefab.Speed;

            CreateStartDots();
        }

        private Vector3 GetPositionAtDistance(float distance)
        {
            var remainingDistance = distance;
            for (var i = 0; i < _path.Length - 1; i++)
            {
                var segmentDistance = Vector3.Distance(_path[i], _path[i + 1]);
                if (segmentDistance > remainingDistance)
                {
                    return Vector3.Lerp(_path[i], _path[i + 1], remainingDistance / segmentDistance);
                }

                remainingDistance -= segmentDistance;
            }

            return _path[^1];
        }

        public void RecalculatePath()
        {
            foreach (var wayPointDot in _dots)
            {
                wayPointDot.WayCompleted -= WayPointDotOnWayCompleted;
                _dotPool.Return(wayPointDot);
            }
            _dots.Clear();

            CreateStartDots();
        }

        private void CreateStartDots()
        {
            _path = _pathSystem.GetPath(_spawnPoint, _endPoint);
            _distance = _path.Distance();

            var dotsCount = (int)(_distance / (_interval * _dotSpeed));
            for (var i = 0; i < dotsCount; i++)
            {
                var distance = i * _interval * _dotSpeed;
                var position = GetPositionAtDistance(distance);
                var dot = _dotPool.Get(dot => { dot.Transform.position = position; });
                dot.WayCompleted += WayPointDotOnWayCompleted;
                var path = _pathSystem.GetPath(position, _endPoint);
                dot.Move(path);
                _dots.Add(dot);
            }

            _leftTimeToSpawn = _interval;
        }

        public void Update()
        {
            _path = _pathSystem.GetPath(_spawnPoint, _endPoint);
            _leftTimeToSpawn -= Time.deltaTime;

            if (_leftTimeToSpawn > 0)
                return;

            _leftTimeToSpawn = _interval;
            SpawnDot();
            
        }

        private void SpawnDot()
        {
            var dot = _dotPool.Get(dot => { dot.Transform.position = _spawnPoint; });
            dot.WayCompleted += WayPointDotOnWayCompleted;
            dot.Move(_path);
            _dots.Add(dot);
        }

        private void WayPointDotOnWayCompleted(WayPointDot dot)
        {
            dot.WayCompleted -= WayPointDotOnWayCompleted;
            _dots.Remove(dot);
            _dotPool.Return(dot);
        }
    }
}