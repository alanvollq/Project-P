using System;
using System.Collections.Generic;
using Core.Data;
using Core.Data.UIContainers;
using Core.Systems.UI.MessageboxSystem;
using Core.Systems.UI.PageSystem;
using Core.Systems.UI.PopupSystem;
using Core.Systems.UI.TooltipSystem;
using Game.Data;
using Game.Data.Loggers;
using UnityEngine;

namespace Game.Setup
{
    [CreateAssetMenu(fileName = "Systems Data", menuName = "Data/Systems Data")]
    public class SystemsData : ScriptableObject
    {
        [SerializeField] private UIContainer _uiContainer;
        [SerializeField] private LevelDataContainer _levelDataContainer;

        
        public UIContainer UIContainer => _uiContainer;
        public LevelDataContainer LevelDataContainer => _levelDataContainer;
    }


    [Serializable]
    public class UIContainer
    {
        [SerializeField] private PagesContainer _pagesContainer;
        [SerializeField] private PopupsContainer _popupsContainer;
        [SerializeField] private MessageboxesContainer _messageboxesContainer;
        [SerializeField] private TooltipContainer _tooltipContainer;


        public IEnumerable<BasePage> Pages => _pagesContainer.Data;
        public IEnumerable<BasePopup> Popups => _popupsContainer.Data;
        public IEnumerable<BaseMessagebox> MessageBoxes => _messageboxesContainer.Data;
        public IEnumerable<BaseTooltip> Tooltips => _tooltipContainer.Data;
    }
}