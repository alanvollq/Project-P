﻿using System;
using Core.Extensions;
using Core.Pool;
using DG.Tweening;
using UnityEngine;

namespace Game
{
    public class WayPointDot : MonoBehaviour, IPoolable
    {
        public event Action<WayPointDot> WayCompleted;

        [SerializeField] private Transform _transform;
        [SerializeField] private SpriteRenderer _spriteRenderer;
        [SerializeField] private float _speed;


        public Transform Transform => _transform;
        public float Speed => _speed;


        public void SetColor(Color color)
        {
            _spriteRenderer.color = color;
        }

        public void Move(Vector3[] path)
        {
            var distance = path.Distance();
            var duration = distance / _speed;
            _transform
                .DOPath(path, duration)
                .OnComplete(() => WayCompleted?.Invoke(this))
                .SetAutoKill();
        }

        public void Recycle()
        {
            _transform.DOKill();
        }

        public void Release()
        {
            _transform.DOKill();
        }
    }
}