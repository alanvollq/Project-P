using Core.SaveSystem;
using Game.Constants;
using UnityEngine;

namespace Game.Levels
{
    [CreateAssetMenu(fileName = "Level Data", menuName = "Data/Game/Levels/Level Data")]
    public class LevelData : ScriptableObject
    {
        [SerializeField] private LevelInfo _levelInfo;
        [SerializeField] private LevelWaves _levelWaves;
        [SerializeField] private LevelMap _levelMapPrefab;
        [SerializeField] private BaseLevelStatsData _baseLevelStats;
        [SerializeField] private LevelData _previousLevel;
        
        private bool _isComplete;


        public LevelInfo LevelInfo => _levelInfo;
        public LevelWaves LevelWaves => _levelWaves;
        public LevelMap LevelMapPrefab => _levelMapPrefab;
        public BaseLevelStatsData BaseLevelStats => _baseLevelStats;

        public bool Available => _previousLevel == null || _previousLevel.IsComplete;
        public bool IsComplete => _isComplete;


        [ContextMenu("Complete")]
        public void Complete()
        {
            _isComplete = true;
            SaveSystemProvider.SetBool(SaveKeys.LevelCompletedSaveKey(_levelInfo.Name), true);
            SaveSystemProvider.Save();
        }

        public void Load()
        {
            _isComplete = SaveSystemProvider.GetBool(SaveKeys.LevelCompletedSaveKey(_levelInfo.Name));
        }
    }
}