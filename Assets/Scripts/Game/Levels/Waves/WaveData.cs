using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Levels.Waves
{
    [Serializable]
    public class WaveData
    {
        [SerializeField] private List<SpawnerOnWave> _spawnersOnWave;
        [SerializeField] private List<List<WavePart>> _waveSettings;


        public IEnumerable<SpawnerOnWave> SpawnersOnWaves => _spawnersOnWave;

        public int EnemyCount =>
            _spawnersOnWave.Sum(spawnerOnWave => spawnerOnWave.WaveParts.Sum(wavePart => wavePart.Count));
    }
}