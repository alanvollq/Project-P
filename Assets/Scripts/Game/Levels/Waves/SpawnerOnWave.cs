using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Levels.Waves
{
    [Serializable]
    public class SpawnerOnWave
    {
        [SerializeField] private List<WavePart> _waveParts;

        
        public IEnumerable<WavePart> WaveParts => _waveParts;
    }
}