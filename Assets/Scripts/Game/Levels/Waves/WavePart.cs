using System;
using Game.Data.Units.Enemies;
using Game.Enemies;
using UnityEngine;
using UnityEngine.Serialization;

namespace Game.Levels.Waves
{
    [Serializable]
    public class WavePart
    {
        [FormerlySerializedAs("_enemyData")] [SerializeField] private EnemyDataByQuality _enemyDataByQuality;
        [SerializeField] private int _count;
        [SerializeField] private float _interval;
        [SerializeField] private float _startDelay;


        public EnemyDataByQuality EnemyDataByQuality => _enemyDataByQuality;
        public int Count => _count;
        public float Interval => _interval;
        public float StartDelay => _startDelay;
    }
}