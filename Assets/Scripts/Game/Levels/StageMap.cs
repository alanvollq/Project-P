﻿using System.Collections.Generic;
using Core.Context;
using Game.Misc;
using Game.Systems.CameraSystem;
using Game.Systems.InputSystem;
using Game.UI.Pages.UIElements.StagePage;
using UnityEngine;

namespace Game.Levels
{
    public class StageMap : MonoBehaviour
    {
        [SerializeField] private CameraController _cameraController;
        [SerializeField] private List<LevelBannerView> _levelBannerViews;
        [SerializeField] private Transform _bottomLeftBound;
        [SerializeField] private Transform _topRightBound;
        
        private IInputSystem _inputSystem;
        private ICameraSystem _cameraSystem;


        public List<LevelBannerView> LevelBannerViews => _levelBannerViews;
        public void Init(IGameContext gameContext)
        {
            _inputSystem = gameContext.GetSystem<IInputSystem>();
            _cameraSystem = gameContext.GetSystem<ICameraSystem>();
            _cameraController.Init(_inputSystem, _cameraSystem);
            _cameraController.OnLevelLoaded(_bottomLeftBound.position, _topRightBound.position);
            
            foreach (var levelBannerView in _levelBannerViews)
            {
            }
        }
    }
}