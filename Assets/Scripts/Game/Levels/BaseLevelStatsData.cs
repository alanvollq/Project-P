using UnityEngine;

namespace Game.Levels
{
    [CreateAssetMenu(fileName = "Base Level Stats Data", menuName = "Data/Game/Levels/Base Level Stats Data")]
    public class BaseLevelStatsData : ScriptableObject
    {
        [SerializeField] private int _healthPoint;
        [SerializeField] private int _currencyAmount;
        [SerializeField] private int _creatureCountAvailable;


        public int HealthPoint => _healthPoint;
        public int CurrencyAmount => _currencyAmount;
        public int CreatureCountAvailable => _creatureCountAvailable;
    }
}