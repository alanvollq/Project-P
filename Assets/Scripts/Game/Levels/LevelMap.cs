﻿using System;
using System.Collections.Generic;
using Core.Context;
using Game.Managers.WorldMarkManager;
using Game.Misc;
using Game.Systems.CameraSystem;
using Game.Systems.InputSystem;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Game.Levels
{
    public class LevelMap : MonoBehaviour
    {
        public event Action SelectedEmpty;
        public event Action<Vector3> SelectPosition; 

        [SerializeField] private List<Transform> _spawnPoints;
        [SerializeField] private Transform _endPoint;
        [SerializeField] private float _step = 1;
        [SerializeField] private Vector3 _offset = new(-0.5f, -0.5f);
        [SerializeField] private ClickHandler _clickHandler;
        [SerializeField] private Tilemap _buildMap;
        [SerializeField] private float _dragSensitivity = 0.2f;
        [SerializeField] private CameraController _cameraController;
        [SerializeField] private Transform _bottomLeftBound;
        [SerializeField] private Transform _topRightBound;
        [SerializeField] private List<WorldMark> _worldMarks;

        private IInputSystem _inputSystem;
        private ICameraSystem _cameraSystem;
        private IWorldMarkManager _worldMarkManager;

        private Vector3 _cameraPositionOnDown;
        private Vector3 _correctPosition;
        private bool _hasTile;


        public List<Transform> SpawnPoints => _spawnPoints;
        public Transform EndPoint => _endPoint;
        public Vector3 Offset => _offset;


        public void Init(IGameContext gameContext)
        {
            _inputSystem = gameContext.GetSystem<IInputSystem>();
            _cameraSystem = gameContext.GetSystem<ICameraSystem>();
            _worldMarkManager = gameContext.GetManager<IWorldMarkManager>();

            _worldMarks.ForEach(item => _worldMarkManager.AddMark(item));
            
            _clickHandler.Up += ClickHandlerOnUp;
            _clickHandler.Down += ClickHandlerOnDown;

            _cameraController.Init(_inputSystem, _cameraSystem);
            _cameraController.OnLevelLoaded(_bottomLeftBound.position, _topRightBound.position);
        }

        private void OnDestroy()
        {
            _worldMarks.ForEach(item => _worldMarkManager.RemoveMark(item));
        }

        private void ClickHandlerOnDown()
        {
            _cameraPositionOnDown = _cameraSystem.CameraPosition;
            SetCorrectPosition(_inputSystem.WorldPosition);
            _hasTile = _buildMap.HasTile(GetTilePosition(_correctPosition));

            if (_hasTile == false)
                SelectedEmpty?.Invoke();
        }

        private void ClickHandlerOnUp()
        {
            if (_hasTile == false)
                return;

            var dragDistance = Vector3.Distance(_cameraPositionOnDown, _cameraSystem.CameraPosition);
            if (dragDistance > _dragSensitivity)
                return;

            SelectPosition?.Invoke(_correctPosition);
        }

        private void SetCorrectPosition(Vector2 touchPosition)
        {
            _correctPosition = new Vector2
            {
                x = Mathf.Round(touchPosition.x / _step) * _step,
                y = Mathf.Round(touchPosition.y / _step) * _step
            };
        }

        private Vector3Int GetTilePosition(Vector3 position)
        {
            var x = Mathf.RoundToInt(position.x);
            var y = Mathf.RoundToInt(position.y);
            return new Vector3Int(x, y, 0);
        }

        public void RemoveShadow(Vector3 position)
        {
            _buildMap.SetTile(GetTilePosition(position), null);
        }
    }
}