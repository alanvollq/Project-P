﻿using System.Collections.Generic;
using UnityEngine;

namespace Game.Levels.MainCampaign
{
    [CreateAssetMenu(fileName = "Campaign Data", menuName = "Data/Game/Levels/Campaign Data")]
    public class CampaignData : ScriptableObject
    {
        [SerializeField] private List<StageData> _stages;


        public IEnumerable<StageData> Stages => _stages;
    }
}