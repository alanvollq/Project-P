﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Levels.MainCampaign
{
    [CreateAssetMenu(fileName = "Stage Data", menuName = "Data/Game/Levels/Stage Data")]
    public class StageData : ScriptableObject
    {
        [SerializeField] private string _name;
        [SerializeField] private Sprite _previewSprite;
        [SerializeField] private List<LevelData> _levels;
        [SerializeField] private StageData _previousStage;
        [SerializeField] private StageMap _stageMap;


        public string Name => _name;
        public Sprite PreviewSprite => _previewSprite;
        public bool IsAvailable => _previousStage == null || _previousStage.IsComplete;
        public bool IsComplete => _levels.All(level => level.IsComplete);
        public StageMap Map => _stageMap;
        public List<LevelData> Levels => _levels;
    }
}