using System;
using System.Collections.Generic;
using System.Linq;
using Game.Data.Units.Enemies;
using Game.Levels.Waves;
using UnityEngine;

namespace Game.Levels
{
    [Serializable]
    public class LevelWaves 
    {
        [SerializeField] private List<WaveData> _waves;


        public IEnumerable<WaveData> Waves => _waves;


        public IEnumerable<EnemyDataByQuality> GetEnemies()
        {
            return _waves
                .SelectMany(wave => wave.SpawnersOnWaves)
                .SelectMany(spawnerData => spawnerData.WaveParts)
                .Select(wavePartData => wavePartData.EnemyDataByQuality)
                .Distinct()
                .OrderBy(enemyData => enemyData.QualityType.Level)
                .ThenBy(enemyData => enemyData.UnitData.name);
        }
    }
}