﻿using Core.Context;
using Core.Loggers;
using Core.StateMachine;
using Core.Systems.EventSystem;
using Core.Systems.UI.PopupSystem;
using Core.Systems.UpdateSystem;
using Game.Events.Waves;
using Game.Levels.StateMachine.States;
using Game.Managers.EnemyManager;
using Game.Managers.LevelCurrencyManager;
using Game.Managers.LevelHealthManager;
using Game.Systems.LevelSystem;
using Game.Systems.WaveSystem;
using UnityEngine;
using static Game.Data.Loggers.GameLogData;

namespace Game.Levels.StateMachine
{
    public sealed class LevelStateMachine : StateMachine<LevelStateId, LevelTriggerId>, IUpdatable
    {
        private readonly ILevelHealthManager _levelHealthManager;
        private readonly ILevelCurrencyManager _levelCurrencyManager;
        private readonly IPopupSystem _popupSystem;
        private readonly IWaveSystem _waveSystem;
        private readonly IEventSystem _eventSystem;
        private readonly ILevelSystem _levelSystem;
        private readonly IEnemyManager _enemyManager;


        public LevelStateMachine(IGameContext gameContext)
        {
            _popupSystem = gameContext.GetSystem<IPopupSystem>();
            _waveSystem = gameContext.GetSystem<IWaveSystem>();
            _levelSystem = gameContext.GetSystem<ILevelSystem>();
            _eventSystem = gameContext.GetSystem<IEventSystem>();
            _enemyManager = gameContext.GetManager<IEnemyManager>();

            _levelCurrencyManager = gameContext.GetManager<ILevelCurrencyManager>();
            _levelHealthManager = gameContext.GetManager<ILevelHealthManager>();
        }

        protected override void CreateStates()
        {
            var startState = new StartState(_levelCurrencyManager, _levelHealthManager, _levelSystem);
            var waveState = new WaveState(_waveSystem, _enemyManager, _levelCurrencyManager, _levelHealthManager);
            var prepareState = new PrepareState();
            var completeState = new CompleteState(_popupSystem, _levelSystem);
            var defeatedState = new DefeatedState(_popupSystem);

            FiniteStateMachine
                .AddState(startState)
                .AddState(prepareState)
                .AddState(waveState)
                .AddState(completeState)
                .AddState(defeatedState)
                .SetStartState(LevelStateId.Start)
                ;

            FiniteStateMachine.StateChanged += FiniteStateMachineOnStateChanged;
        }

        private void FiniteStateMachineOnStateChanged()
        {
            var fromMessage = FiniteStateMachine.PreviousState is not null
                ? $"{FiniteStateMachine.PreviousState.Id}"
                : "_Enable";

            LevelStateMachineLogData
                .Log($"State changed - from: {fromMessage} to: {FiniteStateMachine.CurrentState.Id}");
        }

        protected override void CreateTransitions()
        {
            FiniteStateMachine
                .AddTransition(LevelStateId.Start, LevelStateId.Prepare)
                .AddTransition(LevelStateId.Prepare, LevelStateId.Wave, LevelTriggerId.PrepareToWave)
                .AddTransition(LevelStateId.Wave, LevelStateId.Prepare, LevelTriggerId.WaveToPrepare)
                // .AddTransition(LevelStateId.Wave, LevelStateId.Completed, LevelTriggerId.WaveToCompleted)
                // .AddTransition(LevelStateId.Wave, LevelStateId.Defeated, LevelTriggerId.WaveToDefeated)
                .AddTransition(LevelStateId.Wave, LevelStateId.Completed, WaveToCompletedCondition)
                .AddTransition(LevelStateId.Wave, LevelStateId.Defeated, WaveToDefeatedCondition)
                ;
        }

        private bool WaveToCompletedCondition()
        {
            return _waveSystem.WaveIsActive == false && _levelHealthManager.HasHealth;
        }

        private bool WaveToDefeatedCondition()
        {
            return _waveSystem.WaveIsActive == false && _levelHealthManager.HasHealth == false;
        }

        protected override void OnEnable()
        {
            _eventSystem.Subscribe<TryStartWaveEvent>(OnTryStartWave);

            _waveSystem.WaveCompleted += OnWaveCompleted;
            _levelHealthManager.HealthDepleted += OnHealthDepleted;
        }

        protected override void OnDisable()
        {
            _eventSystem.Unsubscribe<TryStartWaveEvent>(OnTryStartWave);

            _waveSystem.WaveCompleted -= OnWaveCompleted;
            _levelHealthManager.HealthDepleted -= OnHealthDepleted;
        }

        private void OnTryStartWave(TryStartWaveEvent @event)
        {
            if (_waveSystem.WaveIsActive)
            {
                //TODO: Show message about wave is active.
                return;
            }

            FiniteStateMachine.InvokeTrigger(LevelTriggerId.PrepareToWave);
        }

        private void OnWaveCompleted()
        {
            if (_waveSystem.CurrentWaveNumber == _waveSystem.WaveCount)
            {
                // FiniteStateMachine.InvokeTrigger(LevelTriggerId.WaveToCompleted);
            }
            else
                FiniteStateMachine.InvokeTrigger(LevelTriggerId.WaveToPrepare);
        }

        private void OnHealthDepleted()
        {
            FiniteStateMachine.InvokeTrigger(LevelTriggerId.WaveToDefeated);
        }
    }
}