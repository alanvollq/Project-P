﻿namespace Game.Levels.StateMachine.States
{
    public enum LevelStateId
    {
        Start,
        Wave,
        Prepare,
        Completed,
        Defeated
    }
}