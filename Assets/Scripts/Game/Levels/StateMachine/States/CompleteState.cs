﻿using Core.StateMachine.States;
using Core.Systems.UI.PopupSystem;
using Game.Systems.LevelSystem;
using Game.UI.Popups;

namespace Game.Levels.StateMachine.States
{
    public class CompleteState : State<LevelStateId>
    {
        private readonly IPopupSystem _popupSystem;
        private readonly ILevelSystem _levelSystem;


        public CompleteState(IPopupSystem popupSystem, ILevelSystem levelSystem) : base(LevelStateId.Completed)
        {
            _popupSystem = popupSystem;
            _levelSystem = levelSystem;
        }


        protected override void OnEnter()
        {
            _levelSystem.LevelData.Complete();
            var openParam = new UIResultLevelPopupOpenParam(true);
            _popupSystem.Open<LevelResultPopup>(openParam);
        }

        protected override void OnExit()
        {
            _popupSystem.CloseLast();
        }
    }
}