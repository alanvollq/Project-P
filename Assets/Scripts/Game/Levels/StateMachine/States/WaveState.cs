﻿using Core.StateMachine.States;
using Game.Enemies;
using Game.Managers.EnemyManager;
using Game.Managers.LevelCurrencyManager;
using Game.Managers.LevelHealthManager;
using Game.Systems.WaveSystem;

namespace Game.Levels.StateMachine.States
{
    public class WaveState : State<LevelStateId>
    {
        private readonly IWaveSystem _waveSystem;
        private readonly ILevelCurrencyManager _levelCurrencyManager;
        private readonly ILevelHealthManager _levelHealthManager;
        private readonly IEnemyManager _enemyManager;


        public WaveState(
            IWaveSystem waveSystem,
            IEnemyManager enemyManager,
            ILevelCurrencyManager levelCurrencyManager,
            ILevelHealthManager levelHealthManager
            ) : base(LevelStateId.Wave)
        {
            _waveSystem = waveSystem;
            _enemyManager = enemyManager;
            _levelCurrencyManager = levelCurrencyManager;
            _levelHealthManager = levelHealthManager;
        }


        protected override void OnEnter()
        {
            _enemyManager.EnemyMoveCompleted += OnEnemyMoveCompleted;
            _enemyManager.EnemyDead += OnEnemyDead;
            _levelHealthManager.HealthDepleted += LevelHealthManagerOnHealthDepleted;
            
            _waveSystem.StartWave();
        }

        private void LevelHealthManagerOnHealthDepleted()
        {
           _waveSystem.WaveComplete();
        }

        protected override void OnExit()
        {
            _enemyManager.EnemyMoveCompleted -= OnEnemyMoveCompleted;
            _enemyManager.EnemyDead -= OnEnemyDead;
            _levelHealthManager.HealthDepleted -= LevelHealthManagerOnHealthDepleted;
        }

        private void OnEnemyDead(Enemy enemy)
        {
            _levelCurrencyManager.TryAddCurrencyValue(enemy.Stats.Reward);
        }

        private void OnEnemyMoveCompleted(Enemy enemy)
        {
            _levelHealthManager.RemoveHealthValue(enemy.Stats.Damage);
        }
    }
}