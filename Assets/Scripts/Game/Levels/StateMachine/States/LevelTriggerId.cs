﻿namespace Game.Levels.StateMachine.States
{
    public enum LevelTriggerId
    {
        PrepareToWave,
        WaveToCompleted,
        WaveToPrepare,
        WaveToDefeated
    }
}