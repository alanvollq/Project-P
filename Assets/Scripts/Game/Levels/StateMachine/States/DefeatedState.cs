﻿using Core.StateMachine.States;
using Core.Systems.UI.PopupSystem;
using Game.UI.Popups;

namespace Game.Levels.StateMachine.States
{
    public class DefeatedState : State<LevelStateId>
    {
        private readonly IPopupSystem _popupSystem;

        public DefeatedState(IPopupSystem popupSystem) : base(LevelStateId.Defeated)
        {
            _popupSystem = popupSystem;
        }

        protected override void OnEnter()
        {
            var openParam = new UIResultLevelPopupOpenParam(false);
            _popupSystem.Open<LevelResultPopup>(openParam);
        }

        protected override void OnExit()
        {
            _popupSystem.CloseLast();
        }
    }
}