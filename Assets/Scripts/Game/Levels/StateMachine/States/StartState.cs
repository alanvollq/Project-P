﻿using Core.StateMachine.States;
using Game.Managers.LevelCurrencyManager;
using Game.Managers.LevelHealthManager;
using Game.Systems.LevelSystem;

namespace Game.Levels.StateMachine.States
{
    
    
    public class StartState : State<LevelStateId>
    {
        private readonly ILevelCurrencyManager _levelCurrencyManager;
        private readonly ILevelHealthManager _levelHealthManager;
        private readonly ILevelSystem _levelSystem;
        

        public StartState(
            ILevelCurrencyManager levelCurrencyManager,
            ILevelHealthManager levelHealthManager,
            ILevelSystem levelSystem
            ) : base(LevelStateId.Start)
        {
            _levelCurrencyManager = levelCurrencyManager;
            _levelHealthManager = levelHealthManager;
            _levelSystem = levelSystem;
        }

        protected override void OnEnter()
        {
           var levelStats = _levelSystem.LevelData.BaseLevelStats;
           
           _levelCurrencyManager.TrySetCurrencyValue(levelStats.CurrencyAmount);
           _levelHealthManager.SetHealthValue(levelStats.HealthPoint);
           
            IsExitAvailable = true;
        }
    }
}