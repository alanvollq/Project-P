﻿using Core.StateMachine.States;

namespace Game.Levels.StateMachine.States
{
    public class PrepareState : State<LevelStateId>
    {
        public PrepareState() : base(LevelStateId.Prepare)
        {
        }
    }
}