﻿using System.Collections.Generic;
using System.Linq;
using Game.Levels.Waves;
using Game.Misc;

namespace Game.Levels
{
    public class Level
    {
        private readonly List<Wave> _waves = new();
        private readonly Counter _waveCounter = new();

        public Level(IReadOnlyCollection<WaveData> wavesData)
        {
            _waves.AddRange(wavesData.Select(waveData => new Wave(waveData)));
            
            _waveCounter.SetMax(wavesData.Count);
            _waveCounter.SetCurrentToZero();
        }

        public void StartNextWave()
        {
            if(_waves.All(wave => wave.IsCompleted))
                return;
            
            
        }
    }
}