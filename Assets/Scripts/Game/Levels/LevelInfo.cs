﻿using System;
using UnityEngine;

namespace Game.Levels
{
    [Serializable]
    public class LevelInfo
    {
        [SerializeField] private string _name;
        [SerializeField, TextArea] private string _description;


        public string Name => _name;
        public string Description => _description;
    }
}