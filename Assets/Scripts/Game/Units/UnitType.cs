﻿using UnityEngine;

namespace Game.Units
{
    [CreateAssetMenu(fileName = "Unit Type", menuName = "Data/Game/Units/Unit Type")]
    public class UnitType : ScriptableObject
    {
        [SerializeField] private string _name;
        [SerializeField] private Sprite _icon;
        [SerializeField] private Color _color;
        [SerializeField] private UnitType _advantage;
        [SerializeField] private UnitType _disadvantage;
        
        
        public string Name => _name;
        public Sprite Icon => _icon;
        public UnitType Advantage => _advantage;
        public UnitType Disadvantage => _disadvantage;
        public Color Color => _color;
    }
}