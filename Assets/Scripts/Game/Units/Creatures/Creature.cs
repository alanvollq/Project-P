﻿using Game.Creatures;
using Game.Data.Units.Creatures;
using Game.QualityTypes;
using Game.Units.Creatures.Abilities.Slots;
using UnityEngine;

namespace Game.Units.Creatures
{
    public class Creature
    {
        private readonly CreatureDataByQuality _creatureDataByQuality;

        public Creature(CreatureDataByQuality creatureDataByQuality)
        {
            _creatureDataByQuality = creatureDataByQuality;
            
            var abilitySlotsData = _creatureDataByQuality.UnitData.AbilitySlotsData;
            Icon = _creatureDataByQuality.Icon;
            Name = _creatureDataByQuality.UnitData.Name;
            UnitType = _creatureDataByQuality.UnitData.UnitType;
            QualityType = _creatureDataByQuality.QualityType;
            AbilitySlots = new AbilitySlots(abilitySlotsData, QualityType);
        }


        public QualityType QualityType { get; }
        public UnitType UnitType { get; }
        public AbilitySlots AbilitySlots { get; }
        public Sprite Icon { get; }
        public string Name { get; }
        
        public CreatureController CreatureControllerPrefab => _creatureDataByQuality.CreatureControllerPrefab;

        
        public bool EqualsData(CreatureDataByQuality creatureDataByQuality)
        {
            return _creatureDataByQuality == creatureDataByQuality;
        }
    }
}