﻿using System;

namespace Game.Units.Creatures
{
    public class CreatureSlot
    {
        public event Action<Creature> Changed;
        public event Action<CreatureSlot> Selected;


        public Creature Creature { get; private set; }


        public void SetCreature(Creature creature)
        {
            Creature = creature;
            Changed?.Invoke(Creature);
        }

        public void Select()
        {
            Selected?.Invoke(this);
        }
    }
}