using Game.QualityTypes;
using UnityEngine;
using System;
using Game.Units.Creatures.Abilities.Slots;

namespace Game.Units.Creatures.Abilities
{
    public interface IAbility
    {
        public event Action<bool> AvailableChanged;
        public event Action<bool> ActiveChanged;


        public IAbilitySlot<Ability> AbilitySlot { get; }
        public UnitType UnitType { get; }
        public QualityType QualityType { get; }
        public string Name { get; }
        public Sprite Icon { get; }
        public bool IsAvailable { get; }
        public bool IsActive { get; }


        public void SetAvailable(bool value);
        public void SetActive(bool value);
        public void SetSlot<T>(AbilitySlot<T> abilitySlot) where T : Ability;
    }
}