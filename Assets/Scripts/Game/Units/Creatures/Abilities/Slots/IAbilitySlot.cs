﻿using System;

namespace Game.Units.Creatures.Abilities.Slots
{
    public interface IAbilitySlot<out TAbility> where TAbility : Ability
    {
        public event Action ActiveAbilityChanged;

        public TAbility FirstAbility { get; }
        public TAbility SecondAbility { get; }
        public TAbility ActiveAbility { get; }


        public void SwitchAbility();
    }
}