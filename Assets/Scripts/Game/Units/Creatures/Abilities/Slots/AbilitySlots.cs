using System;
using Game.Data.Units.Creatures.Abilities;
using Game.QualityTypes;

namespace Game.Units.Creatures.Abilities.Slots
{
    public class AbilitySlots
    {
        public event Action ActiveAbilityChanged;
        
        
        public AbilitySlots(AbilitySlotsData abilitySlotsData, QualityType qualityType)
        {
            var weaponAbilitySlotData = abilitySlotsData.WeaponAbilitySlotData;
            var firstWeaponAbility = new WeaponAbility(weaponAbilitySlotData.FirstAbility, qualityType);
            var secondWeaponAbility = new WeaponAbility(weaponAbilitySlotData.SecondAbility, qualityType);
            WeaponAbilitySlot = new AbilitySlot<WeaponAbility>(firstWeaponAbility, secondWeaponAbility);

            var notWeaponAbilitySlotData = abilitySlotsData.FirstAbilitySlotData;
            var firstNotWeaponAbility = new NotWeaponAbility(notWeaponAbilitySlotData.FirstAbility, qualityType);
            var secondNotWeaponAbility = new NotWeaponAbility(notWeaponAbilitySlotData.SecondAbility, qualityType);
            FirstAbilitySlot = new AbilitySlot<NotWeaponAbility>(firstNotWeaponAbility, secondNotWeaponAbility);

            notWeaponAbilitySlotData = abilitySlotsData.SecondAbilitySlotData;
            firstNotWeaponAbility = new NotWeaponAbility(notWeaponAbilitySlotData.FirstAbility, qualityType);
            secondNotWeaponAbility = new NotWeaponAbility(notWeaponAbilitySlotData.SecondAbility, qualityType);
            SecondAbilitySlot = new AbilitySlot<NotWeaponAbility>(firstNotWeaponAbility, secondNotWeaponAbility);
            
            WeaponAbilitySlot.ActiveAbilityChanged += AbilitySlotOnActiveAbilityChanged;
            FirstAbilitySlot.ActiveAbilityChanged += AbilitySlotOnActiveAbilityChanged;
            SecondAbilitySlot.ActiveAbilityChanged += AbilitySlotOnActiveAbilityChanged;
        }

        private void AbilitySlotOnActiveAbilityChanged()
        {
            ActiveAbilityChanged?.Invoke();
        }


        public IAbilitySlot<WeaponAbility> WeaponAbilitySlot { get; }
        public IAbilitySlot<NotWeaponAbility> FirstAbilitySlot { get; }
        public IAbilitySlot<NotWeaponAbility> SecondAbilitySlot { get; }
    }
}