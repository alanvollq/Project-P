using System;

namespace Game.Units.Creatures.Abilities.Slots
{
    public class AbilitySlot<T> : IAbilitySlot<T> where T : Ability
    {
        public event Action ActiveAbilityChanged;


        public AbilitySlot(T firstAbility, T secondAbility)
        {
            FirstAbility = firstAbility;
            SecondAbility = secondAbility;

            FirstAbility.SetSlot(this);
            SecondAbility.SetSlot(this);
            SwitchAbility();
        }


        public T FirstAbility { get; }

        public T SecondAbility { get; }

        public T ActiveAbility { get; private set; }


        public void SwitchAbility()
        {
            if (ActiveAbility == null)
                SetActiveAbility(FirstAbility);
            else if (ActiveAbility == FirstAbility)
                SetActiveAbility(SecondAbility);
            else
                SetActiveAbility(FirstAbility);
        }

        private void SetActiveAbility(T ability)
        {
            // if(ability.IsAvailable == false)
            //     return;
            
            ActiveAbility?.SetActive(false);

            ActiveAbility = ability;
            ActiveAbility.SetActive(true);
            ActiveAbilityChanged?.Invoke();
        }
    }
}