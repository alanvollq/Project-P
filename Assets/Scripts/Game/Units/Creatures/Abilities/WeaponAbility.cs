﻿using Game.Data.Units.Creatures.Abilities;
using Game.QualityTypes;

namespace Game.Units.Creatures.Abilities
{
    public class WeaponAbility : Ability
    {
        private readonly WeaponAbilityData _weaponAbilityData;
        
        
        public WeaponAbility(AbilityData abilityData, QualityType qualityType) : base(abilityData, qualityType)
        {
            _weaponAbilityData = abilityData as WeaponAbilityData;
        }

        
        public WeaponAbilityData WeaponAbilityData => _weaponAbilityData;
    }
}