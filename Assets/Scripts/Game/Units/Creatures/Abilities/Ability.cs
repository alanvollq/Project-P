﻿using System;
using Game.Data.Units.Creatures.Abilities;
using Game.QualityTypes;
using Game.Units.Creatures.Abilities.Slots;
using UnityEngine;

namespace Game.Units.Creatures.Abilities
{
    public abstract class Ability : IAbility
    {
        public event Action<bool> AvailableChanged;
        public event Action<bool> ActiveChanged;

        private readonly AbilityData _abilityData;
        private bool _isAvailable;
        private bool _isActive;

//TODO: Убравть когда появятся наследники как WeaponAbility
        public AbilityData AbilityData => _abilityData;


        protected Ability(AbilityData abilityData, QualityType qualityType)
        {
            _abilityData = abilityData;
            QualityType = qualityType;
        }


        public IAbilitySlot<Ability> AbilitySlot { get; private set; }
        public UnitType UnitType => _abilityData.UnitType;
        public string Name => _abilityData.Name;
        public Sprite Icon => _abilityData.Icon;
        public bool IsAvailable => _isAvailable;
        public bool IsActive => _isActive;

        public QualityType QualityType { get; }


        public void SetAvailable(bool value)
        {
            _isAvailable = value;
            AvailableChanged?.Invoke(_isAvailable);
        }

        public void SetActive(bool value)
        {
            _isActive = value;
            if (value)
            {
                if (AbilitySlot.ActiveAbility != this)
                    AbilitySlot.SwitchAbility();
            }

            ActiveChanged?.Invoke(_isActive);
        }

        public void SetSlot<T>(AbilitySlot<T> abilitySlot) where T : Ability
        {
            AbilitySlot = abilitySlot;
        }
    }
}