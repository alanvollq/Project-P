﻿using Game.Data.Units.Creatures.Abilities;
using Game.QualityTypes;

namespace Game.Units.Creatures.Abilities
{
    public class NotWeaponAbility : Ability
    {
        public NotWeaponAbility(AbilityData abilityData, QualityType qualityType) : base(abilityData, qualityType)
        {
        }
    }
}