using UnityEngine;

namespace Game.QualityTypes
{
    [CreateAssetMenu(fileName = "Types Of Quality", menuName = "Data/Game/Types Of Quality")]
    public class QualityType : ScriptableObject
    {
        [SerializeField] private string _name;
        [SerializeField] private Color _primaryColor;
        [SerializeField] private Color _secondaryColor;
        [SerializeField] private int _level;


        public string Name => _name;
        public Color PrimaryColor => _primaryColor;
        public Color SecondaryColor => _secondaryColor;
        public int Level => _level;
    }
}