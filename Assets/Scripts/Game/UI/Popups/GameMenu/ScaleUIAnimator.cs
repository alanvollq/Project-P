﻿using Core.Systems.UI;
using DG.Tweening;
using UnityEngine;

namespace Game.UI.Popups.GameMenu
{
    public class ScaleUIAnimator : UIAnimator
    {
        [SerializeField] private RectTransform _transform;
        [SerializeField] private Vector3 _startScale;
        [SerializeField] private Vector3 _finalScale;
        [SerializeField] private float _duration;


        public override void SetDefaultState()
        {
            _transform.localScale = _startScale;
        }

        public override void PlayAnimation(Sequence sequence)
        {
            _transform.DOScale(_finalScale, _duration);
        }
    }
}