﻿using Core.Systems.UI;
using Core.Systems.UI.PopupSystem;
using Game.Systems.BuildSystem;
using Game.UI.Pages.GamePage.BuildList;
using Game.Units.Creatures;
using UnityEngine;
using Core.Extensions;
using Game.Levels;
using Game.Systems.LevelSystem;

namespace Game.UI.Popups
{
    public class CreatureBuildPopup : BasePopup
    {
        [SerializeField] private BuildListView _listView;
        [SerializeField] private BuildPlacingView _buildPlacingViewPrefab;

        private IBuildSystem _buildSystem;
        private ILevelSystem _levelSystem;
        private Vector3 _placePoint;
        private Creature _lastSelected;
        private Transform _buildPlacingParent;
        private BuildPlacingView _buildPlacingView;
        private LevelMap _levelMap;


        protected override void OnInitialization()
        {
            _buildPlacingParent = new GameObject("[ Placing Item ]").transform;
            _buildPlacingParent.SetParent(GameContext.RootOwner.GameRoot);
            _buildPlacingView = _buildPlacingViewPrefab.Instantiate(_buildPlacingParent);
            _buildPlacingView.gameObject.SetActive(false);

            _buildSystem = GameContext.GetSystem<IBuildSystem>();
            _levelSystem = GameContext.GetSystem<ILevelSystem>();
            
            _levelSystem.LevelLoaded += LevelSystemOnLevelLoaded;
            _levelSystem.LevelUnloaded += LevelSystemOnLevelUnloaded;

            _listView.Initialization();
            _listView.SetEnable(false);
            _listView.Selected += OnCreatureSelected;
        }

        private void LevelSystemOnLevelLoaded()
        {
            _levelMap = _levelSystem.LevelMap;
            _levelMap.SelectPosition += LevelMapOnSelectPosition;
        }
        
        private void LevelSystemOnLevelUnloaded()
        {
            _levelMap.SelectPosition -= LevelMapOnSelectPosition;
            _levelMap = null;
        }

        protected override void OnBeforeOpen(IUIElementOpenParam openParam)
        {
            _listView.SetAvailableCreatures( _levelSystem.SelectedCreatures);
            _listView.SetEnable(true);
            
        }

        private void LevelMapOnSelectPosition(Vector3 position)
        {
            _buildPlacingView.transform.position = position;
            _buildPlacingView.gameObject.SetActive(true);
        }

        protected override void OnAfterClose()
        {
            _lastSelected = null;
            _listView.SetEnable(false);
            _listView.RemoveAll();

            _buildPlacingView.SetEnableRadiusView(false);
            _buildPlacingView.gameObject.SetActive(false);
        }

        private void OnCreatureSelected(Creature creature)
        {
            if (_lastSelected == creature)
            {
                _buildSystem.AcceptPlacing(_lastSelected);
                PopupSystem.CloseLast();
            }
            else
            {
                _lastSelected = creature;

                var range = creature.AbilitySlots.WeaponAbilitySlot.ActiveAbility.WeaponAbilityData.Stats.Range;
                _buildPlacingView.SetRadiusValue(range);
                _buildPlacingView.SetEnableRadiusView(true);
            }
        }
    }
}