using Core.Systems.TimeSystem;
using Core.Systems.UI;
using Core.Systems.UI.PopupSystem;
using Game.Systems.LevelSystem;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Popups
{
    public sealed class GameMenuPopup : BasePopup
    {
        [SerializeField] private Button _continueButton;
        [SerializeField] private Button _restartButton;
        [SerializeField] private Button _completeButton;

        private ITimeSystem _timeSystem;
        private ILevelSystem _levelSystem;


        protected override void OnInitialization()
        {
            _timeSystem = GameContext.GetSystem<ITimeSystem>();
            _levelSystem = GameContext.GetSystem<ILevelSystem>();

            _continueButton.onClick.AddListener(OnContinueButtonDown);
            _restartButton.onClick.AddListener(OnRestartButtonDown);
            _completeButton.onClick.AddListener(OnCompleteButtonDown);
        }

        protected override void OnBeforeOpen(IUIElementOpenParam openParam)
        {
            _timeSystem.Pause();
        }

        protected override void OnAfterClose()
        {
            _timeSystem.Play();
        }

        private void OnContinueButtonDown()
        {
            PopupSystem.CloseLast();
        }

        private void OnRestartButtonDown()
        {
            _levelSystem.RestartLevel();
        }

        private void OnCompleteButtonDown()
        {
            _levelSystem.CompleteLevel();
        }
    }
}