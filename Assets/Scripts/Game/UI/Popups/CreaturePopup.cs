﻿using Core.Systems.UI;
using Core.Systems.UI.PopupSystem;
using Game.Towers;

namespace Game.UI.Popups
{
    public class CreaturePopup : BasePopup
    {
    }

    public record CreaturePopupOpenParam(TowerController Controller) : IUIElementOpenParam
    {
        public TowerController Controller { get; } = Controller;
    }
}