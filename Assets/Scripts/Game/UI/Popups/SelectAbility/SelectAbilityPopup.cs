﻿using System;
using Core.Systems.UI;
using Core.Systems.UI.PopupSystem;
using Core.Systems.UI.TooltipSystem;
using Game.Abilities;
using Game.Data.Units.Creatures;
using Game.Data.Units.Creatures.Abilities;
using Game.UI.Popups.SelectCreature;
using Game.UI.Tooltips;
using Game.UI.Views;
using Game.UI.Views.Creatures;
using Game.Units.Creatures;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Popups.SelectAbility
{
    public class SelectAbilityPopup : BasePopup
    {
        [SerializeField] private Image _creatureIcon;
        [SerializeField] private UnitTypeView _unitTypeView;
        [SerializeField] private TMP_Text _creatureTypeName;
        [SerializeField] private TMP_Text _qualityTypeName;
        [SerializeField] private TMP_Text _unitTypeName;
        [SerializeField] private SelectableAbilitySlotsView _selectableAbilitySlotsView;

        [SerializeField] private Button _selectButton;
        [SerializeField] private Button _changeButton;
        [SerializeField] private Button _removeButton;

        private SelectAbilityPopupOpenParam _openParam;
        private CreatureSlot _creatureSlot;
        private Creature _creature;
        private ITooltipSystem _tooltipSystem;
        private WeaponAbilityData _weaponAbilityData;
        private PassiveAbilityData _passiveAbilityData;
        private ActiveAbilityData _activeAbilityData;


        protected override void OnInitialization()
        {
            _tooltipSystem = GameContext.GetSystem<ITooltipSystem>();
            _selectButton.onClick.AddListener(OnSelectButtonDown);
            _changeButton.onClick.AddListener(OnChangeButtonDown);
            _removeButton.onClick.AddListener(OnRemoveButtonDown);
            _selectableAbilitySlotsView.AbilitySlotDown += OnAbilitySlotViewDown;
        }

        private void OnSelectButtonDown()
        {
            _creatureSlot.SetCreature(_creature);
            PopupSystem.CloseLast();
        }

        private void OnChangeButtonDown()
        {
            PopupSystem.CloseLast();
        }

        private void OnRemoveButtonDown()
        {
            _creatureSlot.SetCreature(null);
            PopupSystem.CloseLast();
        }

        protected override void OnBeforeOpen(IUIElementOpenParam openParam)
        {
            _openParam = openParam as SelectAbilityPopupOpenParam;
            if (_openParam is null)
                throw new Exception("open param is empty");

            _creatureSlot = _openParam.CreatureSlot;
            _creature = _creatureSlot.Creature ?? new Creature(_openParam.CreatureDataByQuality);
            _creatureTypeName.text = _creature.Name;

            var unitType = _creature.UnitType;
            _creatureIcon.sprite = _creature.Icon;

            _unitTypeView.SetType(unitType);
            _unitTypeName.text = unitType.Name;
            _unitTypeName.color = unitType.Color;

            var qualityType = _creature.QualityType;
            _qualityTypeName.text = qualityType.Name;
            _qualityTypeName.color = qualityType.PrimaryColor;
            _creatureTypeName.color = qualityType.PrimaryColor;


            _selectButton.gameObject.SetActive(_creatureSlot.Creature is null);
            _removeButton.gameObject.SetActive(_creatureSlot.Creature is not null);


            _selectableAbilitySlotsView.SetAbilities(_creature);
        }

        private void OnAbilitySlotViewDown(AbilityView abilityView)
        {
            var rectTransform = abilityView.GetComponent<RectTransform>();
            var openParam = new AbilityTooltipOpenParam(abilityView.Ability, rectTransform
            );
            _tooltipSystem.Open<AbilityTooltip>(openParam);
        }
    }

    public record SelectAbilityPopupOpenParam(
        CreatureSlot CreatureSlot,
        CreatureDataByQuality CreatureDataByQuality = null) : IUIElementOpenParam
    {
        public CreatureSlot CreatureSlot { get; } = CreatureSlot;
        public CreatureDataByQuality CreatureDataByQuality { get; } = CreatureDataByQuality;
    }
}