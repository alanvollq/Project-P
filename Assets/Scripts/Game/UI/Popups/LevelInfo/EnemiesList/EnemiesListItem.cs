﻿using Game.Data.Units.Enemies;

namespace Game.UI.Popups.LevelInfo.EnemiesList
{
    public class EnemiesListItem
    {
        public EnemiesListItem(EnemyDataByQuality enemyDataByQuality)
        {
            EnemyDataByQuality = enemyDataByQuality;
        }

        
        public EnemyDataByQuality EnemyDataByQuality { get; }
    }
}