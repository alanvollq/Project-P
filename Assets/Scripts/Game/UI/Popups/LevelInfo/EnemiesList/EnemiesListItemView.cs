﻿using Core.UIElements.ListView;
using Game.UI.Views;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Popups.LevelInfo.EnemiesList
{
    public class EnemiesListItemView : ListItemView<EnemiesListItem>
    {
        [SerializeField] private Image _icon;
        [SerializeField] private UnitTypeView _typeView;
        [SerializeField] private Image _qualityBorder;
        [SerializeField] private GameObject _newLabel;
        
        protected override void OnSetItem(EnemiesListItem item)
        {
            var enemyData = item.EnemyDataByQuality;
            _typeView.SetType(enemyData.UnitData.UnitType);
            _icon.sprite = enemyData.Icon;
            _qualityBorder.color = enemyData.QualityType.PrimaryColor;
            
            _newLabel.SetActive(Random.Range(0,3) == 0);
        }
    }
}