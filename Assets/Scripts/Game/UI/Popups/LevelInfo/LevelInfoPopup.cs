﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Systems.UI;
using Core.Systems.UI.PopupSystem;
using Game.Data;
using Game.Data.Units.Creatures;
using Game.Levels;
using Game.Managers.GameStateManager;
using Game.Systems.LevelSystem;
using Game.UI.Popups.LevelInfo.EnemiesList;
using Game.UI.Popups.LevelInfo.CreaturesList;
using Game.UI.Popups.SelectAbility;
using Game.UI.Popups.SelectCreature;
using Game.Units.Creatures;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Game.UI.Popups.LevelInfo
{
    public class LevelInfoPopup : BasePopup
    {
        [SerializeField] private CreatureDataContainer _creatureDataContainer;
        [SerializeField] private TMP_Text _levelNameText;
        [SerializeField] private CreaturesListView _creatureListView;
        [SerializeField] private EnemiesListView _enemyListView;
        [SerializeField] private Button _readyButton;
        [SerializeField] private TMP_Text _selectedValueText;

        private readonly List<EnemiesListItem> _enemyItems = new();
        private readonly List<CreatureSlot> _creatureSlots = new();
        private LevelData _levelData;
        private IGameStateManager _gameStateManager;
        private ILevelSystem _levelSystem;

        protected override void OnInitialization()
        {
            _gameStateManager = GameContext.GetManager<IGameStateManager>();
            _levelSystem = GameContext.GetSystem<ILevelSystem>();

            _creatureListView.Initialization();
            _enemyListView.Initialization();

            _readyButton.onClick.AddListener(OnReadyButtonDown);
        }

        private void Update()
        {
            _readyButton.interactable = _creatureSlots.All(item => item.Creature is not null);
        }

        private void OnReadyButtonDown()
        {
            var creaturesOnLevel = _creatureSlots.Select(item => item.Creature).ToList();
            _levelSystem.SetCreatures(creaturesOnLevel);
            _gameStateManager.StartNewLevel();
        }

        protected override void OnBeforeOpen(IUIElementOpenParam openParam)
        {
            if (openParam is not LevelInfoPopupOpenParam levelInfoPopupOpenParam)
                throw new Exception("open param is empty");
            
            _levelData = levelInfoPopupOpenParam.LevelData;
            _levelNameText.text = _levelData.LevelInfo.Name;
            
            var countAvailable = _levelData.BaseLevelStats.CreatureCountAvailable;
            for (var i = 0; i < countAvailable; i++)
            {
                var creatureSlot = new CreatureSlot();
                creatureSlot.Selected += OnCreatureSlotSelected;
                creatureSlot.Changed += CreatureSlotOnChanged;
                _creatureSlots.Add(creatureSlot);
            }


            _creatureListView.AddItems(_creatureSlots);


            var enemiesOnLevel = _levelData.LevelWaves.GetEnemies().ToList();
            foreach (var enemyData in enemiesOnLevel)
                _enemyItems.Add(new EnemiesListItem(enemyData));

            _enemyListView.AddItems(_enemyItems);

            TestLoad();
        }

        private void CreatureSlotOnChanged(Creature creature)
        {
            _selectedValueText.text = $"Selected {_creatureSlots.Sum(item => item.Creature is not null? 1 : 0)}/{_creatureSlots.Count}";
        }

        private void TestLoad()
        {
            var creatureDataByQualities = new List<CreatureDataByQuality>(_creatureDataContainer.Data);
            foreach (var creatureSlot in _creatureSlots)
            {
                var randomIndex = Random.Range(0, creatureDataByQualities.Count);
                var creatureDataByQuality = creatureDataByQualities[randomIndex];
                var creature = new Creature(creatureDataByQuality);
                creatureSlot.SetCreature(creature);
                creatureDataByQualities.RemoveAt(randomIndex);
            }

            creatureDataByQualities.Clear();
        }

        private void OnCreatureSlotSelected(CreatureSlot creatureSlot)
        {
            if (creatureSlot.Creature == null)
            {
                var openParam = new SelectCreaturePopupOpenParam(creatureSlot, _creatureSlots);
                PopupSystem.Open<SelectCreaturePopup>(openParam);
            }
            else
            {
                var openParam = new SelectAbilityPopupOpenParam(creatureSlot);
                PopupSystem.Open<SelectAbilityPopup>(openParam);
            }
        }

        protected override void OnAfterClose()
        {
            _enemyItems.Clear();
            _enemyListView.RemoveAll();

            _creatureSlots.Clear();
            _creatureListView.RemoveAll();
        }
    }
    
    public class LevelInfoPopupOpenParam : IUIElementOpenParam
    {
        public LevelInfoPopupOpenParam(LevelData levelData)
        {
            LevelData = levelData;
        }

        public LevelData LevelData { get; }
    }
}