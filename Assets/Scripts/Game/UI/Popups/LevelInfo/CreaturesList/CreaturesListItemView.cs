﻿using Core.UIElements.ListView;
using Game.UI.Views;
using Game.UI.Views.Creatures;
using Game.Units.Creatures;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Popups.LevelInfo.CreaturesList
{
    public class CreaturesListItemView : ListItemView<CreatureSlot>
    {
        [SerializeField] private Button _button;
        [SerializeField] private GameObject _emptyView;
        [SerializeField] private GameObject _filledView;
        [SerializeField] private Image _qualityBorder;
        [SerializeField] private Image _background;
        [SerializeField] private UnitTypeView _typeView;
        [SerializeField] private Image _creatureIcon;

        [SerializeField] private AbilityView _weaponAbilityView;
        [SerializeField] private AbilityView _passiveAbilityView;
        [SerializeField] private AbilityView _activeAbilityView;

        private CreatureSlot _item;
        private Creature _creature;


        private void Awake()
        {
            _button.onClick.AddListener(OnButtonDown);
        }

        private void OnButtonDown()
        {
            _item.Select();
        }

        protected override void OnSetItem(CreatureSlot item)
        {
            _item = item;
        }

        protected override void OnRecycle()
        {
            ItemOnChanged(_item.Creature);
            _item.Changed += ItemOnChanged;
        }

        private void ItemOnChanged(Creature creature)
        {
            _emptyView.SetActive(creature is null);
            _filledView.SetActive(creature is not null);

            if (creature is null)
                return;

            if(_creature is not null)
                _creature.AbilitySlots.ActiveAbilityChanged -= SetAbilities;
            
            _creature = creature;
            _qualityBorder.color = creature.QualityType.PrimaryColor;
            _background.color = creature.QualityType.SecondaryColor;
            _typeView.SetType(creature.UnitType);
            _creatureIcon.sprite = creature.Icon;

            SetAbilities();
            _creature.AbilitySlots.ActiveAbilityChanged += SetAbilities;
        }

        private void SetAbilities()
        {
            _weaponAbilityView.SetAbility(_creature.AbilitySlots.WeaponAbilitySlot.ActiveAbility);
            _passiveAbilityView.SetAbility(_creature.AbilitySlots.FirstAbilitySlot.ActiveAbility);
            _activeAbilityView.SetAbility(_creature.AbilitySlots.SecondAbilitySlot.ActiveAbility);
        }

        protected override void OnRelease()
        {
            if(_creature is not null)
                _creature.AbilitySlots.ActiveAbilityChanged -= SetAbilities;
            
            _item.Changed -= ItemOnChanged;
            _item = null;
        }
    }
}