﻿using Game.Data.Units.Creatures.Abilities;
using Game.UI.Views;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Popups.CreatureInfo
{
    public class AbilityPanelView : MonoBehaviour
    {
        [SerializeField] private Image _iconImage;
        [SerializeField] private TMP_Text _nameText;
        [SerializeField] private TMP_Text _descriptionText;
        [SerializeField] private UnitTypeView _unitTypeView;


        public void SetAbilityInfo(AbilityData abilityData)
        {
            _unitTypeView.SetType(abilityData.UnitType);
            _iconImage.sprite = abilityData.Icon;
            _nameText.text = abilityData.Name;
            _descriptionText.text = abilityData.GetDescription();
        }
    }
}