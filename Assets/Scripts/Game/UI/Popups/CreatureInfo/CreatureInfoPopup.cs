﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Core.Systems.UI;
using Core.Systems.UI.PopupSystem;
using Core.Systems.UI.TooltipSystem;
using Game.Data.Units.Creatures;
using Game.Data.Units.Creatures.Abilities;
using Game.UI.Popups.CreatureInfo;
using Game.UI.Tooltips;
using Game.UI.UIElements.ListViews;
using Game.UI.Views;
using Game.UI.Views.Creatures;
using Game.Units;
using Game.Units.Creatures;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Popups.CreatureInfo
{
    public class CreatureInfoPopup : BasePopup
    {
        [SerializeField] private CreaturePreviewCamera _creaturePreviewCameraPrefab;
        [SerializeField] private Image _icon;
        [SerializeField] private UnitTypeView _typeView;
        [SerializeField] private TMP_Text _nameText;
        [SerializeField] private TMP_Text _typeText;
        [SerializeField] private TMP_Text _qualityText;

        [SerializeField] private WeaponAbilityPanelView _firstWeaponAbilityView;
        [SerializeField] private WeaponAbilityPanelView _secondWeaponAbilityView;
        [SerializeField] private AbilitiesBlock _firstAbilitiesBlock;
        [SerializeField] private AbilitiesBlock _secondAbilitiesBlock;

        [SerializeField] private Image _topDecoration;
        [SerializeField] private Image _bottomDecoration;
        [SerializeField] private Image _topDecorationGlow;
        [SerializeField] private Image _bottomDecorationGlow;

        [SerializeField] private UnitTypeListView _strongVsListView = new();
        [SerializeField] private UnitTypeListView _weakVsListView = new();

        private CreatureInfoPopupOpenParam _openParam;
        private ITooltipSystem _tooltipSystem;
        private CreaturePreviewCamera _creaturePreviewCamera;


        protected override void OnInitialization()
        {
            _tooltipSystem = GameContext.GetSystem<ITooltipSystem>();

            _strongVsListView.Initialization();
            _weakVsListView.Initialization();

            // _abilitySlotsView.AbilitySlotDown += OnAbilityViewDown;
            
            var creaturePreviewParent = new GameObject("[ Creature Preview ]").transform;
            creaturePreviewParent.SetParent(GameContext.RootOwner.MiscRoot);
            _creaturePreviewCamera = _creaturePreviewCameraPrefab.Instantiate(creaturePreviewParent);
        }

        protected override void OnBeforeOpen(IUIElementOpenParam openParam)
        {
            _openParam = openParam as CreatureInfoPopupOpenParam;
            if (_openParam is null)
                throw new Exception("Open param is not correct!");

            var creature = new Creature(_openParam.CreatureDataByQuality);

            _icon.sprite = creature.Icon;
            _nameText.text = creature.Name;

            var unitType = creature.UnitType;
            _typeView.SetType(unitType);
            _typeText.text = unitType.Name;
            _typeText.color = unitType.Color;

            var quality = creature.QualityType;
            _qualityText.text = quality.Name;
            _qualityText.color = quality.PrimaryColor;

            _topDecoration.color = quality.SecondaryColor;
            _bottomDecoration.color = quality.SecondaryColor;
            _topDecorationGlow.color = quality.PrimaryColor;
            _bottomDecorationGlow.color = quality.PrimaryColor;

            var summaryTypes = new List<UnitType>();
            summaryTypes.Add(unitType);
            summaryTypes.Add(creature.AbilitySlots.WeaponAbilitySlot.FirstAbility.UnitType);
            summaryTypes.Add(creature.AbilitySlots.WeaponAbilitySlot.SecondAbility.UnitType);
            summaryTypes.Add(creature.AbilitySlots.FirstAbilitySlot.FirstAbility.UnitType);
            summaryTypes.Add(creature.AbilitySlots.FirstAbilitySlot.SecondAbility.UnitType);
            summaryTypes.Add(creature.AbilitySlots.SecondAbilitySlot.FirstAbility.UnitType);
            summaryTypes.Add(creature.AbilitySlots.SecondAbilitySlot.SecondAbility.UnitType);

            var distinctTypes = summaryTypes.Distinct();
            foreach (var distinctType in distinctTypes)
            {
                _strongVsListView.AddItem(distinctType.Advantage);
                _weakVsListView.AddItem(distinctType.Disadvantage);
            }

            
            var abilities = creature.AbilitySlots;
            
            _firstWeaponAbilityView.SetAbilityInfo(abilities.WeaponAbilitySlot.FirstAbility.WeaponAbilityData);
            _secondWeaponAbilityView.SetAbilityInfo(abilities.WeaponAbilitySlot.SecondAbility.WeaponAbilityData);
            
            _firstAbilitiesBlock.SetAbilities(
                abilities.FirstAbilitySlot.FirstAbility.AbilityData,
                abilities.FirstAbilitySlot.SecondAbility.AbilityData);
            _secondAbilitiesBlock.SetAbilities(
                abilities.SecondAbilitySlot.FirstAbility.AbilityData,
                abilities.SecondAbilitySlot.SecondAbility.AbilityData);
            
            _creaturePreviewCamera.SetPreview(_openParam.CreatureDataByQuality.Preview);
        }

        protected override void OnBeforeClose()
        {
            _creaturePreviewCamera.DeletePreview();
            _strongVsListView.RemoveAll();
            _weakVsListView.RemoveAll();
        }

        private void OnAbilityViewDown(AbilityView abilityView)
        {
            var rectTransform = abilityView.GetComponent<RectTransform>();
            var openParam = new AbilityTooltipOpenParam(abilityView.Ability, rectTransform);
            _tooltipSystem.Open<AbilityTooltip>(openParam);
        }
    }


    public record CreatureInfoPopupOpenParam(CreatureDataByQuality CreatureDataByQuality) : IUIElementOpenParam
    {
        public CreatureDataByQuality CreatureDataByQuality { get; private set; } = CreatureDataByQuality;
    }
}

[Serializable]
public class AbilitiesBlock
{
    [SerializeField] private AbilityPanelView _firstWeaponAbilityView;
    [SerializeField] private AbilityPanelView _secondWeaponAbilityView;


    public void SetAbilities(AbilityData firstAbilityData, AbilityData secondAbilityData)
    {
        _firstWeaponAbilityView.SetAbilityInfo(firstAbilityData);
        _secondWeaponAbilityView.SetAbilityInfo(secondAbilityData);
    }
}