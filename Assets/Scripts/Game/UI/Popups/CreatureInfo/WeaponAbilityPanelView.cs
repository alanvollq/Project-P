﻿using Game.Data.Units.Creatures.Abilities;
using Game.UI.Views;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Popups.CreatureInfo
{
    public class WeaponAbilityPanelView : MonoBehaviour
    {
        [SerializeField] private Image _iconImage;
        [SerializeField] private TMP_Text _nameText;
        [SerializeField] private UnitTypeView _unitTypeView;
        [SerializeField] private TMP_Text _damageValueText;
        [SerializeField] private TMP_Text _rangeValueText;
        [SerializeField] private TMP_Text _attackSpeedValeText;


        public void SetAbilityInfo(WeaponAbilityData abilityData)
        {
            _unitTypeView.SetType(abilityData.UnitType);
            _iconImage.sprite = abilityData.Icon;
            _nameText.text = abilityData.Name;

            var stats = abilityData.Stats;

            _damageValueText.text = stats.Damage.ToString("F");
            _rangeValueText.text = stats.Range.ToString("F");
            _attackSpeedValeText.text = stats.AttackSpeed.ToString("F");
        }
    }
}