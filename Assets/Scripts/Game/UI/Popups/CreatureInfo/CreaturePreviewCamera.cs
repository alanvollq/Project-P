﻿using UnityEngine;

namespace Game.UI.Popups.CreatureInfo
{
    public class CreaturePreviewCamera : MonoBehaviour
    {
        [SerializeField] private Transform _previewParent;
        
        private GameObject _preview;


        public void SetPreview(GameObject preview)
        {
            if(_preview is not null)
                Destroy(_preview);

            _preview = Instantiate(preview, _previewParent);
        }

        public void DeletePreview()
        {
           Destroy(_preview);
        }
    }
}