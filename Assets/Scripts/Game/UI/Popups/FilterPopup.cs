﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Systems.UI;
using Core.Systems.UI.PopupSystem;
using Game.QualityTypes;
using Game.UI.UIElements;
using Game.UI.Views;
using Game.Units;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Popups
{
    public class FilterPopup : BasePopup
    {
        [SerializeField] private List<CreatureTypeToggle> _typeToggles;
        [SerializeField] private List<CreatureTypeToggle> _strongVsToggles;
        [SerializeField] private List<QualityTypeToggle> _qualityToggles;
        [SerializeField] private Button _clearButton;
        [SerializeField] private GameObject _strongVsPanel;

        private FilterHandler<UnitType> _unitTypeFilter;
        private FilterHandler<UnitType> _strongVsFilter;
        private FilterHandler<QualityType> _qualityFilter;


        protected override void OnInitialization()
        {
            foreach (var creatureTypeToggle in _typeToggles)
                creatureTypeToggle.Toggled += OnUnitTypeToggled;

            foreach (var strongVsToggle in _strongVsToggles)
                strongVsToggle.Toggled += OnStrongVsToggled;

            foreach (var qualityTypeToggle in _qualityToggles)
                qualityTypeToggle.Toggled += OnQualityTypeToggled;

            _clearButton.onClick.AddListener(OnClearButtonDown);
        }

        private void OnClearButtonDown()
        {
            _unitTypeFilter.ClearFilter();
            _qualityFilter.ClearFilter();
            _strongVsFilter?.ClearFilter();

            foreach (var toggle in _typeToggles.Where(toggle => toggle.IsOn))
                toggle.Toggle();

            foreach (var toggle in _strongVsToggles.Where(toggle => toggle.IsOn))
                toggle.Toggle();

            foreach (var toggle in _qualityToggles.Where(toggle => toggle.IsOn))
                toggle.Toggle();
        }

        protected override void OnBeforeOpen(IUIElementOpenParam openParam)
        {
            if (openParam is not FilterPopupOpenParam filterPopupOpenParam)
                throw new Exception("Open param is empty.");

            _qualityFilter = filterPopupOpenParam.QualityFilter;
            _unitTypeFilter = filterPopupOpenParam.UnitTypeFilter;
            _strongVsFilter = filterPopupOpenParam.StrongVsFilter;
            _strongVsPanel.SetActive(_strongVsFilter is not null);

            foreach (var toggle in _typeToggles.Where(toggle => toggle.IsOn))
                toggle.Toggle();

            foreach (var toggle in _strongVsToggles.Where(toggle => toggle.IsOn))
                toggle.Toggle();

            foreach (var toggle in _qualityToggles.Where(toggle => toggle.IsOn))
                toggle.Toggle();
        }

        private void OnUnitTypeToggled(UnitType unitType, bool value)
        {
            _unitTypeFilter?.Toggle(unitType, value);
        }

        private void OnStrongVsToggled(UnitType unitType, bool value)
        {
            _strongVsFilter?.Toggle(unitType, value);
        }

        private void OnQualityTypeToggled(QualityType qualityType, bool value)
        {
            _qualityFilter?.Toggle(qualityType, value);
        }
    }


    public record FilterPopupOpenParam(
        FilterHandler<QualityType> QualityFilter,
        FilterHandler<UnitType> UnitTypeFilter,
        FilterHandler<UnitType> StrongVsFilter = null
    ) : IUIElementOpenParam
    {
        public FilterHandler<UnitType> UnitTypeFilter { get; } = UnitTypeFilter;
        public FilterHandler<UnitType> StrongVsFilter { get; } = StrongVsFilter;
        public FilterHandler<QualityType> QualityFilter { get; } = QualityFilter;
    }
}