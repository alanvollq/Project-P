﻿using System;
using Game.Data.Units.Creatures;

namespace Game.UI.Popups.SelectCreature.CreaturesList
{
    public class CreaturesListItem
    {
        public event Action<bool> VisibleChanged;
        public event Action<CreaturesListItem> Selected;
        public event Action<bool> ChosenChanged;


        public CreaturesListItem(CreatureDataByQuality creatureDataByQuality)
        {
            CreatureDataByQuality = creatureDataByQuality;
        }

        
        public CreatureDataByQuality CreatureDataByQuality { get; }

        public bool Chosen { get; private set; }

        
        public void Select()
        {
            Selected?.Invoke(this);
        }
        
        public void SetVisible(bool value)
        {
            VisibleChanged?.Invoke(value);
        }

        public void SetChosen(bool value)
        {
            Chosen = value;
            ChosenChanged?.Invoke(Chosen);
        }
    }
}