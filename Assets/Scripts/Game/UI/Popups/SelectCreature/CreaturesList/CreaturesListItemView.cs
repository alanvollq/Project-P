﻿using Core.UIElements.ListView;
using Game.UI.Views;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Popups.SelectCreature.CreaturesList
{
    public class CreaturesListItemView : ListItemView<CreaturesListItem>
    {
        [SerializeField] private Button _button;
        [SerializeField] private GameObject _selectMark;
        [SerializeField] private Image _qualityField;
        [SerializeField] private UnitTypeView _typeView;
        [SerializeField] private Image _creatureIcon;

        private CreaturesListItem _item;


        private void Awake()
        {
            _button.onClick.AddListener(OnButtonDown);
        }

        private void OnButtonDown()
        {
            if(_item.Chosen)
                return;
            
            _item.Select();
        }

        protected override void OnSetItem(CreaturesListItem item)
        {
            _item = item;

            var creatureDataByQuality = _item.CreatureDataByQuality;
            _qualityField.color = creatureDataByQuality.QualityType.PrimaryColor;
            _typeView.SetType(creatureDataByQuality.UnitData.UnitType);
            _creatureIcon.sprite = creatureDataByQuality.Icon;

            SetEnableSelectMark(_item.Chosen);
            _item.ChosenChanged += SetEnableSelectMark;
        }

        private void SetEnableSelectMark(bool value)
        {
            _selectMark.SetActive(value);
        }

        protected override void OnRecycle()
        {
            _item.VisibleChanged += OnVisibleChanged;
        }

        private void OnVisibleChanged(bool value)
        {
            gameObject.SetActive(value);
        }

        protected override void OnRelease()
        {
            _item.VisibleChanged -= OnVisibleChanged;
            _item.ChosenChanged -= SetEnableSelectMark;
            _item = null;
        }
    }
}