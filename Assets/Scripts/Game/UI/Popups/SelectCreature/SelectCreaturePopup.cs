﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Core.Systems.UI;
using Core.Systems.UI.PopupSystem;
using Game.Data;
using Game.QualityTypes;
using Game.UI.Popups.SelectAbility;
using Game.UI.Popups.SelectCreature.CreaturesList;
using Game.UI.Views;
using Game.Units;
using Game.Units.Creatures;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Popups.SelectCreature
{
    public class SelectCreaturePopup : BasePopup
    {
        [SerializeField] private CreatureDataContainer _creatureDataContainer;
        [SerializeField] private CreaturesListView _creaturesListView;
        [SerializeField] private Button _qualityTypeSortButton;
        [SerializeField] private Button _unitTypeSortButton;
        [SerializeField] private Button _filterButton;
        [SerializeField] private Button _typeInfoButton;
        [SerializeField] private ItemCountView _filterCountView;

        private readonly List<CreaturesListItem> _creaturesListItems = new();
        private CreatureSlot _creatureSlot;
        private List<CreatureSlot> _team;

        private readonly FilterHandler<UnitType> _unitTypeFilter = new();
        private readonly FilterHandler<UnitType> _strongVsFilter = new();
        private readonly FilterHandler<QualityType> _qualityFilter = new();


        protected override void OnInitialization()
        {
            _creaturesListView.Initialization();

            _filterCountView.SetEnable(false);
            _filterButton.onClick.AddListener(OnFilterButtonDown);
            _typeInfoButton.onClick.AddListener(OnTypeInfoButtonDown);

            _qualityTypeSortButton.onClick.AddListener(QualityTypeSort);
            _unitTypeSortButton.onClick.AddListener(UnitTypeSort);

            _unitTypeFilter.Changed += OnFiltersChanged;
            _qualityFilter.Changed += OnFiltersChanged;
            _strongVsFilter.Changed += OnFiltersChanged;
        }

        protected override void OnBeforeOpen(IUIElementOpenParam openParam)
        {
            if (openParam is not SelectCreaturePopupOpenParam currentOpenParam)
                throw new Exception("open param is empty");


            _creatureSlot = currentOpenParam.CreatureSlot;
            _team = currentOpenParam.Team;
            var creatures = _team.Select(item => item.Creature).Where(item => item is not null);

            foreach (var creatureDataByQuality in _creatureDataContainer.Data)
            {
                var item = new CreaturesListItem(creatureDataByQuality);
                item.Selected += ItemOnSelected;
                _creaturesListItems.Add(item);
            }

            _creaturesListView.AddItems(_creaturesListItems);
            QualityTypeSort();
        }

        private void OnTypeInfoButtonDown()
        {
            PopupSystem.Open<TypeInfoPopup>();
        }

        private void OnFiltersChanged()
        {
            var filterCount = _qualityFilter.FilterCount + _unitTypeFilter.FilterCount + _strongVsFilter.FilterCount;
            _filterCountView.SetCount(filterCount);
            _filterCountView.SetEnable(filterCount > 0);

            SetVisible();
        }

        protected override void OnBeforeShow()
        {
            Debug.Log("Before show");
            foreach (var creaturesListItem in _creaturesListItems)
            {
                var hasEqualsCreature =
                    _team
                        .Where(item => item.Creature is not null)
                        .Any(item => item.Creature.EqualsData(creaturesListItem.CreatureDataByQuality));

                creaturesListItem.SetChosen(hasEqualsCreature);
            }
        }


        protected override void OnBeforeClose()
        {
            Debug.Log("OnBeforeClose");
            _creaturesListItems.Clear();
            _creaturesListView.RemoveAll();
        }

        private void OnFilterButtonDown()
        {
            var openParam = new FilterPopupOpenParam(_qualityFilter, _unitTypeFilter, _strongVsFilter);
            PopupSystem.Open<FilterPopup>(openParam);
        }

        private void ItemOnSelected(CreaturesListItem creaturesListItem)
        {
            PopupSystem.CloseLast();
            var openParam = new SelectAbilityPopupOpenParam(_creatureSlot, creaturesListItem.CreatureDataByQuality);
            PopupSystem.Open<SelectAbilityPopup>(openParam);
        }

        private void SetVisible()
        {
            var hasQualityTypeFilters = _qualityFilter.FilterCount > 0;
            var hasUnitTypeFilters = _unitTypeFilter.FilterCount > 0;
            var hasStrongVSFilters = _strongVsFilter.FilterCount > 0;

            if (hasUnitTypeFilters == false &&
                hasQualityTypeFilters == false &&
                hasStrongVSFilters == false)
            {
                foreach (var item in _creaturesListItems)
                    item.SetVisible(true);

                return;
            }

            var qualityTypeFilters = _qualityFilter.Types.ToList();
            var unitTypeFilters = _unitTypeFilter.Types.ToList();
            var strongVsFilters = _strongVsFilter.Types.ToList();

            foreach (var creatureItem in _creaturesListItems)
            {
                var qualityTypeVisible = true;
                var unitTypeVisible = true;
                var unitStrongVsVisible = true;
                var abilityStrongVsVisible = true;

                var creatureDataByQuality = creatureItem.CreatureDataByQuality;
                var qualityType = creatureDataByQuality.QualityType;
                var unitType = creatureDataByQuality.UnitData.UnitType;
                var unitTypeAdvantage = unitType.Advantage;
                var abilityAdvantages =
                    creatureDataByQuality.UnitData.AbilitySlotsData.AllAbilities.Select(ability =>
                        ability.UnitType.Advantage).ToList();

                if (hasQualityTypeFilters)
                    qualityTypeVisible = qualityTypeFilters.Any(filterType => filterType == qualityType);

                if (hasUnitTypeFilters)
                    unitTypeVisible = unitTypeFilters.Any(filterType => filterType == unitType);

                if (hasStrongVSFilters)
                    unitStrongVsVisible = strongVsFilters.Any(filterType => filterType == unitTypeAdvantage);


                if (hasStrongVSFilters)
                    abilityStrongVsVisible = strongVsFilters
                        .Any(strongVsFilter => abilityAdvantages
                            .Any(abilityAdvantage => strongVsFilter == abilityAdvantage));

                creatureItem.SetVisible(unitTypeVisible &&
                                        qualityTypeVisible &&
                                        (unitStrongVsVisible || abilityStrongVsVisible));
            }
        }

        #region Sorting

        private void QualityTypeSort()
        {
            _creaturesListView.RemoveAll();

            _creaturesListItems.Sort(QualityCompare);

            _creaturesListView.AddItems(_creaturesListItems);
            SetVisible();
        }

        private void UnitTypeSort()
        {
            _creaturesListView.RemoveAll();

            _creaturesListItems.Sort(TypeCompare);

            _creaturesListView.AddItems(_creaturesListItems);
            SetVisible();
        }

        private int QualityCompare(CreaturesListItem itemA, CreaturesListItem itemB)
        {
            var usedCompare = CreatureItemUsedCompare(itemA, itemB);
            var qualityValue = CreatureItemQualityCompare(itemA, itemB);
            qualityValue *= 1000;
            var typeValue = CreatureItemTypeCompare(itemA, itemB);
            return usedCompare + qualityValue - typeValue;
        }

        private int TypeCompare(CreaturesListItem itemA, CreaturesListItem itemB)
        {
            var usedCompare = CreatureItemUsedCompare(itemA, itemB);
            var typeValue = CreatureItemTypeCompare(itemA, itemB);
            typeValue *= 1000;
            var qualityValue = CreatureItemQualityCompare(itemA, itemB);
            return usedCompare + qualityValue - typeValue;
        }

        private int CreatureItemQualityCompare(CreaturesListItem itemA, CreaturesListItem itemB)
        {
            var itemAQualityLevel = itemA.CreatureDataByQuality.QualityType.Level;
            var itemBQualityLevel = itemB.CreatureDataByQuality.QualityType.Level;
            return itemBQualityLevel - itemAQualityLevel;
        }

        private int CreatureItemTypeCompare(CreaturesListItem itemA, CreaturesListItem itemB)
        {
            var itemACreatureType = itemA.CreatureDataByQuality.UnitData.UnitType;
            var itemBCreatureType = itemB.CreatureDataByQuality.UnitData.UnitType;
            return string.Compare(itemBCreatureType.Name, itemACreatureType.Name, StringComparison.Ordinal);
        }

        private int CreatureItemUsedCompare(CreaturesListItem itemA, CreaturesListItem itemB)
        {
            var itemAUsed = _team.Where(item => item.Creature is not null)
                .Any(item => item.Creature.EqualsData(itemA.CreatureDataByQuality))
                ? 0
                : 10000;
            var itemBUsed = _team.Where(item => item.Creature is not null)
                .Any(item => item.Creature.EqualsData(itemB.CreatureDataByQuality))
                ? 0
                : 10000;
            return itemAUsed - itemBUsed;
        }

        #endregion
    }

    public record SelectCreaturePopupOpenParam(CreatureSlot CreatureSlot, List<CreatureSlot> Team) : IUIElementOpenParam
    {
        public CreatureSlot CreatureSlot { get; } = CreatureSlot;
        public List<CreatureSlot> Team { get; } = Team;
    }
}