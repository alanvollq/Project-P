﻿using System;
using Core.Systems.UI;
using Core.Systems.UI.PopupSystem;
using Game.Data.Units.Enemies;
using Game.Enemies;
using Game.UI.Views;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Popups
{
    public class EnemyInfoPopup : BasePopup
    {
        [SerializeField] private Image _icon;
        [SerializeField] private UnitTypeView _typeView;
        [SerializeField] private TMP_Text _typeNameText;
        [SerializeField] private TMP_Text _qualityText;
        [SerializeField] private TMP_Text _name;

        [SerializeField] private TMP_Text _healthValueText;
        [SerializeField] private TMP_Text _speedValueText;

        [SerializeField] private Image _topPlane;
        [SerializeField] private Image _bottomPlane;
        [SerializeField] private Image _topGlow;
        [SerializeField] private Image _bottomGlow;


        protected override void OnBeforeOpen(IUIElementOpenParam openParam)
        {
            if (openParam is not EnemyInfoPopupOpenParam enemyInfoPopupOpenParam)
                throw new Exception("Open param is not correct!");

            var enemyData = enemyInfoPopupOpenParam.EnemyDataByQuality;
            var quality = enemyData.QualityType;
            
            _icon.sprite = enemyData.Icon;
            
            _name.text = enemyData.UnitData.Name;
            
            _typeView.SetType(enemyData.UnitData.UnitType);
            _typeNameText.text = enemyData.UnitData.UnitType.Name;
            _typeNameText.color = enemyData.UnitData.UnitType.Color;

            _qualityText.text = quality.Name;
            _qualityText.color = quality.PrimaryColor;
            
            _topPlane.color = quality.SecondaryColor;
            _bottomPlane.color = quality.SecondaryColor;

            _topGlow.color = quality.PrimaryColor;
            _bottomGlow.color = quality.PrimaryColor;

            _healthValueText.text = $"{enemyData.Stats.MaxHealth}";
            _speedValueText.text = $"{enemyData.Stats.Speed}";
        }
    }

    public record EnemyInfoPopupOpenParam(EnemyDataByQuality EnemyDataByQuality) : IUIElementOpenParam
    {
        public EnemyDataByQuality EnemyDataByQuality { get; private set; } = EnemyDataByQuality;
    }
}