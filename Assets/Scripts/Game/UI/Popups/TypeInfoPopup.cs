﻿using System.Collections.Generic;
using System.Linq;
using Core.Systems.UI.PopupSystem;
using Game.Data;
using Game.UI.Views;
using UnityEngine;

namespace Game.UI.Popups
{
    public class TypeInfoPopup : BasePopup
    {
        [SerializeField] private UnitTypeDataContainer _unitTypeDataContainer;
        [SerializeField] private List<UnitTypeView> _views;


        protected override void OnInitialization()
        {
        }

        [ContextMenu("SetViews")]
        private void SetViews()
        {
            var types = _unitTypeDataContainer.Data.ToList();
            for (var i = 0; i < types.Count; i++)
                _views[i].SetType(types[i]);
        }
    }
}