using System;
using Core.Systems.TimeSystem;
using Core.Systems.UI;
using Core.Systems.UI.PageSystem;
using Core.Systems.UI.PopupSystem;
using Game.Managers.GameStateManager;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Popups
{
    public class LevelResultPopup : BasePopup
    {
        [SerializeField] private Button _exitToMenuButton;
        [SerializeField] private GameObject _winBackground;
        [SerializeField] private GameObject _defeatBackground;

        private IGameStateManager _gameStateManager;
        private ITimeSystem _timeSystem;


        protected override void OnInitialization()
        {
            _gameStateManager = GameContext.GetManager<IGameStateManager>();
            _timeSystem = GameContext.GetSystem<ITimeSystem>();
            _exitToMenuButton.onClick.AddListener(OnExitToMenuButtonDown);
        }

        protected override void OnBeforeOpen(IUIElementOpenParam openParam)
        {
            if (openParam is not UIResultLevelPopupOpenParam resultLevelPopupOpen)
                throw new Exception("Open param is not correct!");
            
            _winBackground.SetActive(resultLevelPopupOpen.IsWin);
            _defeatBackground.SetActive(resultLevelPopupOpen.IsWin == false);
            
            _timeSystem.Pause();
        }

        protected override void OnAfterClose()
        {
            _timeSystem.Play();
        }

        private void OnExitToMenuButtonDown()
        {
            _gameStateManager.CompleteLevel();
        }
    }

    public record UIResultLevelPopupOpenParam(bool IsWin) : IUIElementOpenParam
    {
        public bool IsWin { get; } = IsWin;
    }
}