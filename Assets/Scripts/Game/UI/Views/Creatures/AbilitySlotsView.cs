﻿using System;
using System.Collections;
using System.Collections.Generic;
using Game.Units.Creatures;
using UnityEngine;

namespace Game.UI.Views.Creatures
{
    [Serializable]
    public class AbilitySlotsView : IEnumerable<AbilityView>
    {
        public event Action<AbilityView> AbilitySlotDown;

        [SerializeField] private AbilityView _weaponFirstAbilityView;
        [SerializeField] private AbilityView _weaponSecondAbilityView;
        [SerializeField] private AbilityView _passiveFirstAbilityView;
        [SerializeField] private AbilityView _passiveSecondAbilityView;
        [SerializeField] private AbilityView _activeFirstAbilityView;
        [SerializeField] private AbilityView _activeSecondAbilityView;


        public void SetAbilities(Creature creature)
        {
            var abilitySlots = creature.AbilitySlots;
            _weaponFirstAbilityView.SetAbility(abilitySlots.WeaponAbilitySlot.FirstAbility);
            _weaponSecondAbilityView.SetAbility(abilitySlots.WeaponAbilitySlot.SecondAbility);

            _passiveFirstAbilityView.SetAbility(abilitySlots.FirstAbilitySlot.FirstAbility);
            _passiveSecondAbilityView.SetAbility(abilitySlots.FirstAbilitySlot.SecondAbility);

            _activeFirstAbilityView.SetAbility(abilitySlots.SecondAbilitySlot.FirstAbility);
            _activeSecondAbilityView.SetAbility(abilitySlots.SecondAbilitySlot.SecondAbility);


            foreach (var abilitySlotView in this)
            {
                abilitySlotView.Down += AbilityViewOnDown;
            }
        }

        private void AbilityViewOnDown(AbilityView abilityView)
        {
            OnAbilityViewDown(abilityView);
            AbilitySlotDown?.Invoke(abilityView);
        }

        protected virtual void OnAbilityViewDown(AbilityView abilityView)
        {
        }

        public IEnumerator<AbilityView> GetEnumerator()
        {
            yield return _weaponFirstAbilityView;
            yield return _weaponSecondAbilityView;
            yield return _passiveFirstAbilityView;
            yield return _passiveSecondAbilityView;
            yield return _activeFirstAbilityView;
            yield return _activeSecondAbilityView;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}