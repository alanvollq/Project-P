﻿using System;
using UnityEngine;

namespace Game.UI.Views.Creatures
{
    public class ActiveAbilityView : MonoBehaviour
    {
        [SerializeField] private AbilityView _abilityView;
        [SerializeField] private GameObject _selectObject;


        private void Update()
        {
            var ability = _abilityView.Ability;
            if(ability is null)
                return;
            
            _selectObject.SetActive(ability.IsActive);
        }
    }
}