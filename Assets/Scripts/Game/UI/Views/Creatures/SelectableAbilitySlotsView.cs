﻿using System;

namespace Game.UI.Views.Creatures
{
    [Serializable]
    public class SelectableAbilitySlotsView : AbilitySlotsView
    {
        protected override void OnAbilityViewDown(AbilityView abilityView)
        {
            abilityView.Ability.SetActive(true);
        }
    }
}