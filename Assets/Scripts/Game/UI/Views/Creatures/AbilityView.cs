﻿using System;
using Game.Units.Creatures.Abilities;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Views.Creatures
{
    public class AbilityView : MonoBehaviour
    {
        public event Action<AbilityView> Down;

        [SerializeField] private Image _iconImage;
        [SerializeField] private UnitTypeView _unitTypeView;
        [SerializeField] private Image _qualityFrame;
        [SerializeField] private Image _lockImage;
        [SerializeField] private Button _button;


        public IAbility Ability { get; private set; }


        private void Awake()
        {
            _button.onClick.AddListener(OnButtonDown);
        }

        private void OnEnable()
        {
            if (Ability is null)
                return;

            Ability.AvailableChanged += SetAvailable;
        }

        private void OnDisable()
        {
            if (Ability is null)
                return;

            Ability.AvailableChanged -= SetAvailable;
        }

        private void OnButtonDown()
        {
            Down?.Invoke(this);
        }

        private void SetAbilityInfo(IAbility ability)
        {
            _iconImage.sprite = ability.Icon;
            _unitTypeView.SetType(ability.UnitType);
            
            if (_qualityFrame is not null)
                _qualityFrame.color = ability.QualityType.PrimaryColor;

            SetAvailable(ability.IsAvailable);
        }

        private void SetAvailable(bool value)
        {
            _lockImage.gameObject.SetActive(value);
            _unitTypeView.SetLock(value);
        }

        public void SetAbility(IAbility ability)
        {
            Ability = ability;
            SetAbilityInfo(ability);
        }
    }
}