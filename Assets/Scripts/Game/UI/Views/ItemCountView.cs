﻿using TMPro;
using UnityEngine;

namespace Game.UI.Views
{
    public class ItemCountView : MonoBehaviour
    {
        [SerializeField] private TMP_Text _cuntText;


        public void SetCount(int value)
        {
            _cuntText.text = $"{value}";
        }

        public void SetEnable(bool value)
        {
            gameObject.SetActive(value);
        }
    }
}