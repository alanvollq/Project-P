﻿using Game.Data.Units.Creatures;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Views
{
    public class UITowerInventoryItemView : MonoBehaviour
    {
        [SerializeField] private Image _towerIcon;
        [SerializeField] private Image _qualityBackground;
        [SerializeField] private UnitTypeView _typeView;
        

        public void SetItem(CreatureDataByQuality creatureDataByQuality)
        {
            _towerIcon.sprite = creatureDataByQuality.Icon;
            _qualityBackground.color = creatureDataByQuality.QualityType.PrimaryColor;
            _typeView.SetType(creatureDataByQuality.UnitData.UnitType);
        }
    }
}