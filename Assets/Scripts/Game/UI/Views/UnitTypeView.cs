﻿using Game.Units;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Views
{
    public class UnitTypeView : MonoBehaviour
    {
        [SerializeField] private Image _icon;
        [SerializeField] private Image _border;
        [SerializeField] private Image _lockShadow;


        public void SetType(UnitType unitType)
        {
            _icon.sprite = unitType.Icon;
            _icon.color = unitType.Color;
            _border.color = unitType.Color;
        }

        public void SetLock(bool value)
        {
            _lockShadow.gameObject.SetActive(value);
        }
    }
}