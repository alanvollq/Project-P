﻿using Game.QualityTypes;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Views
{
    public class QualityView : MonoBehaviour
    {
        [SerializeField] private Image _background;
        [SerializeField] private TMP_Text _nameText;


        public void SetQuality(QualityType qualityType)
        {
            _background.color = qualityType.PrimaryColor;
            _nameText.text = qualityType.Name;
        }
    }
}