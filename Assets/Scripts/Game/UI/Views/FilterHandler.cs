﻿using System;
using System.Collections.Generic;

namespace Game.UI.Views
{
    public class FilterHandler<TType>
    {
        public event Action Changed;
        private readonly List<TType> _filterValues = new();


        public int FilterCount => _filterValues.Count;
        public IEnumerable<TType> Types => _filterValues;


        public void Add(TType type)
        {
            _filterValues.Add(type);
            Changed?.Invoke();
        }

        public void Remove(TType type)
        {
            _filterValues.Remove(type);
            Changed?.Invoke();
        }

        public void Toggle(TType type, bool value)
        {
            if (value)
                Add(type);
            else
                Remove(type);
        }

        public void ClearFilter()
        {
            _filterValues.Clear();
            Changed?.Invoke();
        }
    }
}