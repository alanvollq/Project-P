﻿using System;
using Core.Systems.UI;
using Core.Systems.UI.TooltipSystem;
using Game.UI.Views;
using Game.UI.Views.Creatures;
using Game.Units.Creatures.Abilities;
using TMPro;
using UnityEngine;

namespace Game.UI.Tooltips
{
    public class AbilityTooltip : BaseTooltip
    {
        [SerializeField] private TMP_Text _abilityName;
        [SerializeField] private TMP_Text _typeName;
        [SerializeField] private TMP_Text _abilityDesctiption;
        [SerializeField] private AbilityView _abilityView;
        [SerializeField] private UnitTypeView _advantageType;
        [SerializeField] private UnitTypeView _disadvantageType;

        private AbilityTooltipOpenParam _openParam;


        protected override void OnBeforeOpen(IUIElementOpenParam openParam)
        {
            _openParam = openParam as AbilityTooltipOpenParam;
            if (_openParam is null)
                throw new Exception("Open param is empty!");

            SetInfo(_openParam.Ability);
            SetPosition(_openParam.TargetTransform);
        }

        private void SetInfo(IAbility ability)
        {
            var unitType = ability.UnitType;
            var qualityType = ability.QualityType;

            // _abilitySlotView.SetInfo(abilityData);
            _abilityView.SetAbility(ability);
            _abilityName.text = ability.Name;
            _abilityName.color = qualityType.PrimaryColor;

            _typeName.text = unitType.Name;
            _typeName.color = unitType.Color;

            _advantageType.SetType(unitType.Advantage);
            _disadvantageType.SetType(unitType.Disadvantage);
        }
    }

    public sealed record AbilityTooltipOpenParam(
        IAbility Ability,
        RectTransform TargetTransform) : TooltipOpenParam(TargetTransform)
    {
        public IAbility Ability { get; } = Ability;
    }
}