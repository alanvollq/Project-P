﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Core.Systems.UI;
using Core.Systems.UI.PageSystem;
using Core.Systems.UI.PopupSystem;
using DG.Tweening;
using Game.Data;
using Game.Data.Units.Creatures;
using Game.Managers.GameStateManager;
using Game.QualityTypes;
using Game.UI.Pages.UIElements.CreaturesLibrary;
using Game.UI.Popups;
using Game.UI.Popups.CreatureInfo;
using Game.UI.Views;
using Game.Units;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Pages
{
    public class CreaturesLibraryPage : BasePage
    {
        [SerializeField] private Button _backButton;
        [SerializeField] private CreatureDataContainer _creatureDataContainer;
        [SerializeField] private CreaturesLibraryListView _creaturesLibraryListView;
        [SerializeField] private Button _qualityTypeSortButton;
        [SerializeField] private Button _unitTypeSortButton;
        [SerializeField] private Button _filterButton;
        [SerializeField] private ItemCountView _filterCountView;
        [SerializeField] private Button _typeInfoButton;
        [SerializeField] private Button _clearButton;
        [SerializeField] private TMP_Text _qualitySortLabel;
        [SerializeField] private TMP_Text _typeSortLabel;
        [SerializeField] private Image _typeSortGlow;
        [SerializeField] private Image _qualitySortGlow;
        [SerializeField] private Color _selectedColor;
        [SerializeField] private Color _unselectedColor;

        private readonly List<CreaturesLibraryListItem> _creatureItems = new();
        private ScrollRect _scrollRect;
        private IPopupSystem _popupSystem;
        private IGameStateManager _gameStateManager;

        private readonly FilterHandler<UnitType> _unitTypeFilter = new();
        private readonly FilterHandler<UnitType> _strongVsFilter = new();
        private readonly FilterHandler<QualityType> _qualityFilter = new();

        private const float SelectDuration = 0.25f;
        

        protected override void OnInitialization()
        {
            _popupSystem = GameContext.GetSystem<IPopupSystem>();
            _gameStateManager = GameContext.GetManager<IGameStateManager>();

            _filterCountView.SetEnable(false);

            _backButton.onClick.AddListener(OnBackButtonDown);
            _filterButton.onClick.AddListener(OnFilterButtonDown);
            _clearButton.onClick.AddListener(OnClearButtonDown);
            _typeInfoButton.onClick.AddListener(OnTypeInfoButtonDown);

            _creaturesLibraryListView.Initialization();
            _scrollRect = _creaturesLibraryListView.GetComponent<ScrollRect>();

            foreach (var creatureData in _creatureDataContainer.Data)
                _creatureItems.Add(new CreaturesLibraryListItem(creatureData));

            QualityTypeSort();

            _unitTypeFilter.Changed += OnFiltersChanged;
            _qualityFilter.Changed += OnFiltersChanged;
            _strongVsFilter.Changed += OnFiltersChanged;

            _qualityTypeSortButton.onClick.AddListener(QualityTypeSort);
            _unitTypeSortButton.onClick.AddListener(UnitTypeSort);

            CreaturesLibraryListItem.Selected += OnItemSelected;
        }

        private void OnClearButtonDown()
        {
            _unitTypeFilter.ClearFilter();
            _strongVsFilter.ClearFilter();
            _qualityFilter.ClearFilter();
        }

        private void OnBackButtonDown()
        {
            _gameStateManager.OpenMenu();
        }

        private void OnTypeInfoButtonDown()
        {
            _popupSystem.Open<TypeInfoPopup>();
        }

        private void OnFilterButtonDown()
        {
            var openParam = new FilterPopupOpenParam(_qualityFilter, _unitTypeFilter, _strongVsFilter);
            _popupSystem.Open<FilterPopup>(openParam);
        }

        private void OnItemSelected(CreatureDataByQuality creatureDataByQuality)
        {
            var openParam = new CreatureInfoPopupOpenParam(creatureDataByQuality);
            _popupSystem.Open<CreatureInfoPopup>(openParam);
        }

        protected override void OnAfterOpen(IUIElementOpenParam openParam)
        {
            _scrollRect.SetContentOnStart();
        }

        private void OnFiltersChanged()
        {
            var filterCount = _qualityFilter.FilterCount + _unitTypeFilter.FilterCount + _strongVsFilter.FilterCount;
            _filterCountView.SetCount(filterCount);
            _filterCountView.SetEnable(filterCount > 0);

            SetVisible();
        }

        private void SetVisible()
        {
            var hasQualityTypeFilters = _qualityFilter.FilterCount > 0;
            var hasUnitTypeFilters = _unitTypeFilter.FilterCount > 0;
            var hasStrongVSFilters = _strongVsFilter.FilterCount > 0;

            if (hasUnitTypeFilters == false &&
                hasQualityTypeFilters == false &&
                hasStrongVSFilters == false)
            {
                foreach (var item in _creatureItems)
                    item.SetVisible(true);

                return;
            }

            var qualityTypeFilters = _qualityFilter.Types.ToList();
            var unitTypeFilters = _unitTypeFilter.Types.ToList();
            var strongVsFilters = _strongVsFilter.Types.ToList();

            foreach (var creatureItem in _creatureItems)
            {
                var qualityTypeVisible = true;
                var unitTypeVisible = true;
                var unitStrongVsVisible = true;
                var abilityStrongVsVisible = true;

                var creatureDataByQuality = creatureItem.CreatureDataByQuality;
                var qualityType = creatureDataByQuality.QualityType;
                var unitType = creatureDataByQuality.UnitData.UnitType;
                var unitTypeAdvantage = unitType.Advantage;
                var abilityAdvantages =
                    creatureDataByQuality.UnitData.AbilitySlotsData.AllAbilities.Select(ability =>
                        ability.UnitType.Advantage).ToList();

                if (hasQualityTypeFilters)
                    qualityTypeVisible = qualityTypeFilters.Any(filterType => filterType == qualityType);

                if (hasUnitTypeFilters)
                    unitTypeVisible = unitTypeFilters.Any(filterType => filterType == unitType);

                if (hasStrongVSFilters)
                    unitStrongVsVisible = strongVsFilters.Any(filterType => filterType == unitTypeAdvantage);


                if (hasStrongVSFilters)
                    abilityStrongVsVisible = strongVsFilters
                        .Any(strongVsFilter => abilityAdvantages
                            .Any(abilityAdvantage => strongVsFilter == abilityAdvantage));

                creatureItem.SetVisible(unitTypeVisible &&
                                        qualityTypeVisible &&
                                        (unitStrongVsVisible || abilityStrongVsVisible));
            }

            _scrollRect.SetContentOnStart();
        }

        #region Sorting

        private void QualityTypeSort()
        {
            _creaturesLibraryListView.RemoveAll();

            _creatureItems.Sort(QualityCompare);

            _creaturesLibraryListView.AddItems(_creatureItems);
            SetVisible();

            _qualitySortLabel.DOColor(_selectedColor, SelectDuration);
            _typeSortLabel.DOColor(_unselectedColor, SelectDuration);
            _qualitySortGlow.DOFade(1, SelectDuration);
            _typeSortGlow.DOFade(0, SelectDuration);
        }

        private void UnitTypeSort()
        {
            _creaturesLibraryListView.RemoveAll();

            _creatureItems.Sort(TypeCompare);

            _creaturesLibraryListView.AddItems(_creatureItems);
            SetVisible();
            
            _qualitySortLabel.DOColor(_unselectedColor, SelectDuration);
            _typeSortLabel.DOColor(_selectedColor, SelectDuration);
            _qualitySortGlow.DOFade(0, SelectDuration);
            _typeSortGlow.DOFade(1, SelectDuration);
        }

        private static int QualityCompare(CreaturesLibraryListItem itemA, CreaturesLibraryListItem itemB)
        {
            var qualityValue = CreatureItemQualityCompare(itemA, itemB);
            qualityValue *= 1000;
            var typeValue = CreatureItemTypeCompare(itemA, itemB);
            return qualityValue - typeValue;
        }

        private static int TypeCompare(CreaturesLibraryListItem itemA, CreaturesLibraryListItem itemB)
        {
            var typeValue = CreatureItemTypeCompare(itemA, itemB);
            typeValue *= 1000;
            var qualityValue = CreatureItemQualityCompare(itemA, itemB);
            return qualityValue - typeValue;
        }

        private static int CreatureItemQualityCompare(CreaturesLibraryListItem itemA, CreaturesLibraryListItem itemB)
        {
            var itemAQualityLevel = itemA.CreatureDataByQuality.QualityType.Level;
            var itemBQualityLevel = itemB.CreatureDataByQuality.QualityType.Level;
            return itemBQualityLevel - itemAQualityLevel;
        }

        private static int CreatureItemTypeCompare(CreaturesLibraryListItem itemA, CreaturesLibraryListItem itemB)
        {
            var itemACreatureType = itemA.CreatureDataByQuality.UnitData.UnitType;
            var itemBCreatureType = itemB.CreatureDataByQuality.UnitData.UnitType;
            return string.Compare(itemBCreatureType.Name, itemACreatureType.Name, StringComparison.Ordinal);
        }

        #endregion
    }
}