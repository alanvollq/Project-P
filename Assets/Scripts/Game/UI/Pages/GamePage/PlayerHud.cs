﻿using System;
using Core.Loggers;
using Game.Managers.LevelCurrencyManager;
using Game.Managers.LevelHealthManager;
using Game.Systems.WaveSystem;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static Game.Data.Loggers.GameLogData;

namespace Game.UI.Pages.GamePage
{
    [Serializable]
    public class PlayerHud
    {
        [SerializeField] private TMP_Text _waveText;
        [SerializeField] private TMP_Text _healthText;
        [SerializeField] private TMP_Text _currencyText;
        [SerializeField] private TMP_Text _enemyCountText;
        [SerializeField] private TMP_Text _enemyHealthText;
        [SerializeField] private Image _enemyCountProgressBar;
        [SerializeField] private Image _enemyHealthProgressBar;

        private IWaveSystem _waveSystem;
        private ILevelCurrencyManager _levelCurrencyManager;
        private ILevelHealthManager _levelHealthManager;


        public void Init(IWaveSystem waveSystem, ILevelCurrencyManager levelCurrencyManager,
            ILevelHealthManager levelHealthManager)
        {
            _waveSystem = waveSystem;
            _levelCurrencyManager = levelCurrencyManager;
            _levelHealthManager = levelHealthManager;

            _waveSystem.WavesChanged += UpdateWaveText;
            _waveSystem.WaveStarted += UpdateWaveText;
            _waveSystem.EnemyCounter.ValueChanged += EnemyCounterOnValueChanged;
            _levelCurrencyManager.CurrencyValueChanged += SetCurrency;
            _levelHealthManager.HealthValueChanged += SetHealth;
        }

        private void EnemyCounterOnValueChanged()
        {
            var counter = _waveSystem.EnemyCounter;
            _enemyCountText.text = $"{counter.CurrentValue} / {counter.MaxValue}";
            _enemyHealthText.text = $"{counter.CurrentValue * 8} / {counter.MaxValue * 8}";
            _enemyCountProgressBar.fillAmount = (float)counter.CurrentValue / counter.MaxValue;
            _enemyHealthProgressBar.fillAmount = (float)counter.CurrentValue / counter.MaxValue;
        }

        private void OnLevelHealthValueChanged(int value)
        {
            _healthText.text = $"{value}";
        }

        private void OnLevelCurrencyValueChanged(int value)
        {
            _currencyText.text = $"{value}";
        }

        private void UpdateWaveText()
        {
            SetWave(_waveSystem.CurrentWaveNumber, _waveSystem.WaveCount);
        }

        public void SetWave(int currentWaveNumber, int waveCount)
        {
            WaveSystemLogData.Log($"Set WAVE: {currentWaveNumber} / {waveCount}");
            _waveText.text = $"Wave: {currentWaveNumber} / {waveCount}";
        }

        public void SetCurrency(int value)
        {
            _currencyText.text = $"{value}";
        }

        public void SetHealth(int value)
        {
            _healthText.text = $"{value}";
        }
    }
}