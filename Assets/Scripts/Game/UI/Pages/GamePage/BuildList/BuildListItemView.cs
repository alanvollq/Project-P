﻿using Core.UIElements.ListView;
using Game.UI.Views;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Pages.GamePage.BuildList
{
    public class BuildListItemView : ListItemView<BuildListItem>
    {
        [SerializeField] private Button _button;
        [SerializeField] private Image _icon;
        [SerializeField] private Image _qualityFrame;
        [SerializeField] private Image _glow;
        [SerializeField] private UnitTypeView _unitTypeView;
        [SerializeField] private GameObject _defaultBorder;
        [SerializeField] private GameObject _selectBorder;

        private BuildListItem _item;


        private void Awake()
        {
            _button.onClick.AddListener(OnButtonDown);
        }

        private void OnButtonDown()
        {
            _item.Select();
        }

        protected override void OnSetItem(BuildListItem item)
        {
            _item = item;

            var creature = item.Creature;
            _qualityFrame.color = creature.QualityType.SecondaryColor;
            _glow.color = creature.QualityType.PrimaryColor;
            _icon.sprite = creature.Icon;
            _unitTypeView.SetType(creature.UnitType);
        }

        protected override void OnRecycle()
        {
            _item.Selected += ItemOnSelected;
            _item.Deselected += ItemOnDeselected;
        }

        protected override void OnRelease()
        {
            _item.Selected -= ItemOnSelected;
            _item.Deselected -= ItemOnDeselected;
        }

        private void ItemOnDeselected()
        {
            _selectBorder.SetActive(false);
            _defaultBorder.SetActive(true);
        }

        private void ItemOnSelected(BuildListItem _)
        {
            _selectBorder.SetActive(true);
            _defaultBorder.SetActive(false);
        }
    }
}