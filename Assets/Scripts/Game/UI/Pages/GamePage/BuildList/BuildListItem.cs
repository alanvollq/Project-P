﻿using System;
using Game.Units.Creatures;

namespace Game.UI.Pages.GamePage.BuildList
{
    public record BuildListItem(Creature Creature)
    {
        public event Action<BuildListItem> Selected;
        public event Action<BuildListItem> Accept;

        public event Action Deselected;

        private static BuildListItem _selectedItem;


        public Creature Creature { get; private set; } = Creature;


        public void Select()
        {
            if (_selectedItem is not null)
            {
                if (_selectedItem == this)
                    Accept?.Invoke(this);
                else
                    _selectedItem.Deselect();
            
                _selectedItem = null;
            }
            
            _selectedItem = this;
            Selected?.Invoke(this);
        }

        public void Deselect()
        {
            Deselected?.Invoke();
        }
    }
}