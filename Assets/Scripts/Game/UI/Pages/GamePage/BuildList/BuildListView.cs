﻿using System;
using System.Collections.Generic;
using Core.UIElements.ListView;
using Game.Units.Creatures;

namespace Game.UI.Pages.GamePage.BuildList
{
    public class BuildListView : ListView<BuildListItem>
    {
        public event Action<Creature> Selected;
        
        
        public void SetAvailableCreatures(IEnumerable<Creature> creaturesOnLevel)
        {
            RemoveAll();
            foreach (var creatureSlot in creaturesOnLevel)
            {
                var buildItem = new BuildListItem(creatureSlot);
                buildItem.Selected += BuildItemOnSelected;
                AddItem(buildItem);
            }
            
            foreach (var buildListItem in Items)
            {
                buildListItem.Deselect();
            }
        }
        
        public void SetEnable(bool value)
        {
            gameObject.SetActive(value);
        }
        
        private void BuildItemOnSelected(BuildListItem buildListItem)
        {
            Selected?.Invoke(buildListItem.Creature);
        }
    }
}