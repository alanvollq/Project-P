using Core.Systems.EventSystem;
using Core.Systems.UI.PageSystem;
using Core.Systems.UI.PopupSystem;
using Game.Events.Waves;
using Game.Managers.LevelCurrencyManager;
using Game.Managers.LevelHealthManager;
using Game.Systems.BuildSystem;
using Game.Systems.WaveSystem;
using Game.UI.Popups;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Pages.GamePage
{
    public sealed class GamePage : BasePage
    {
        [SerializeField] private Button _menuButton;
        [SerializeField] private Button _startWaveButton;
        [SerializeField] private PlayerHud _playerHud;

        private IPopupSystem _popupSystem;
        private IWaveSystem _waveSystem;
        private IBuildSystem _buildSystem;
        private IEventSystem _eventSystem;


        protected override void OnInitialization()
        {
            _popupSystem = GameContext.GetSystem<IPopupSystem>();
            _waveSystem = GameContext.GetSystem<IWaveSystem>();
            _buildSystem = GameContext.GetSystem<IBuildSystem>();
            _eventSystem = GameContext.GetSystem<IEventSystem>();

            _buildSystem.BuildModeActivated += BuildSystemOnBuildModeActivated;
            _buildSystem.BuildModeDeactivated += BuildSystemOnBuildModeDeactivated;

            var levelCurrencyManager = GameContext.GetManager<ILevelCurrencyManager>();
            var levelHealthManager = GameContext.GetManager<ILevelHealthManager>();
            _playerHud.Init(_waveSystem, levelCurrencyManager, levelHealthManager);

            _menuButton.onClick.AddListener(OnMenuButtonClick);
            _startWaveButton.onClick.AddListener(OnStartWaveButtonClick);
        }

        private void BuildSystemOnBuildModeActivated()
        {
          _popupSystem.Open<CreatureBuildPopup>();
        }
        
        private void BuildSystemOnBuildModeDeactivated()
        {
            _popupSystem.CloseLast();
        }

        private void OnStartWaveButtonClick()
        {
            _eventSystem.Raise(new TryStartWaveEvent());
        }

        private void OnMenuButtonClick()
        {
            _popupSystem.Open<GameMenuPopup>();
        }
    }
}