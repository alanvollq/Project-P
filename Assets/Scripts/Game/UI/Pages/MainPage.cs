using System.Collections.Generic;
using Core.Systems.UI;
using Core.Systems.UI.PageSystem;
using Core.Systems.UI.PopupSystem;
using Game.Levels.MainCampaign;
using Game.Managers.GameStateManager;
using Game.UI.Pages.UIElements.MainCampaign;
using Game.UI.Popups;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Pages
{
    public sealed class MainPage : BasePage
    {
        [SerializeField] private Button _settingsButton;
        [SerializeField] private Button _enemiesLibraryButton;
        [SerializeField] private Button _creaturesLibraryButton;
        [SerializeField] private Button _continueButton;
        [SerializeField] private Button _storeButton;
        [SerializeField] private Button _researchingButton;

        [SerializeField] private CampaignData _campaignData;
        [SerializeField] private StageListView _stageListView;

        private IPopupSystem _popupSystem;
        private List<StageListItem> _stageItems;
        private IGameStateManager _gameStateManager;


        protected override void OnInitialization()
        {
            _gameStateManager = GameContext.GetManager<IGameStateManager>();
            _popupSystem = GameContext.GetSystem<IPopupSystem>();

            _settingsButton.onClick.AddListener(OnSettingsButtonDown);
            _creaturesLibraryButton.onClick.AddListener(OnTowerLibraryButtonDown);
            _enemiesLibraryButton.onClick.AddListener(OnEnemiesLibraryButtonDown);
            _continueButton.onClick.AddListener(OnPlayButtonDown);
            _storeButton.onClick.AddListener(OnStoreButtonDown);
            _researchingButton.onClick.AddListener(OnResearchingButtonDown);

            _stageListView.Initialization();
            _stageItems = new List<StageListItem>();
            foreach (var stageData in _campaignData.Stages)
            {
                var stageListItem = new StageListItem(stageData);
                stageListItem.Selected += OnStageSelected;
                _stageItems.Add(stageListItem);
            }
        }

        private void OnPlayButtonDown()
        {
        }

        private void OnSettingsButtonDown() => _popupSystem.Open<GameSettingsPopup>();

        private void OnStageSelected(StageListItem stageListItem) => _gameStateManager.OpenStage(stageListItem.StageData);

        private void OnTowerLibraryButtonDown() => _gameStateManager.OpenCreatureLibrary();

        private void OnEnemiesLibraryButtonDown() => _gameStateManager.OpenEnemyLibrary();
        
        private void OnStoreButtonDown() => _gameStateManager.OpenStore();

        private void OnResearchingButtonDown() => _gameStateManager.OpenResearching();

        protected override void OnBeforeOpen(IUIElementOpenParam openParam)
        {
            // TODO: Need scroll to max available stage.
            _stageListView.AddItems(_stageItems);
        }

        protected override void OnAfterClose()
        {
            _stageListView.RemoveAll();
        }
    }
}