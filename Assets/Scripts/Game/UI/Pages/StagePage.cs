﻿using System;
using Core.Systems.UI;
using Core.Systems.UI.PageSystem;
using Game.Levels;
using Game.Levels.MainCampaign;
using Game.Systems.LevelSystem;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Pages
{
    public class StagePage : BasePage
    {
        [SerializeField] private TMP_Text _stageNameText;
        [SerializeField] private Button _backButton;

        private Transform _stageMapParent;
        private StageData _stageData;
        private StageMap _stageMap;
        private ILevelSystem _levelSystem;


        protected override void OnInitialization()
        {
            _levelSystem = GameContext.GetSystem<ILevelSystem>();
            
            _stageMapParent = new GameObject("[ Stage Map ]").transform;
            _stageMapParent.SetParent(GameContext.RootOwner.GameRoot);
            _backButton.onClick.AddListener(OnBackButtonClick);
        }

        private void OnBackButtonClick()
        {
            PageSystem.Open<MainPage>();
        }

        protected override void OnBeforeOpen(IUIElementOpenParam openParam)
        {
            if (openParam is not CampaignStagePageOpenParam campaignStagePageOpenParam)
                throw new Exception($"Open param for {this} is not correct!");

            _stageData = campaignStagePageOpenParam.StageData;
            _stageNameText.text = _stageData.Name;

            var stageMap = _stageData.Map;
            if (_stageMap != null)
                Destroy(_stageMap.gameObject);

            _stageMap = Instantiate(stageMap, _stageMapParent);
            _stageMap.Init(GameContext);

            for (var i = 0; i < _stageData.Levels.Count; i++)
            {
                var levelBanner = _stageMap.LevelBannerViews[i];
                levelBanner.SetInfo(_stageData.Levels[i], i + 1);
                levelBanner.LevelSelected += OnLevelSelected;
            }
        }

        private void OnLevelSelected(LevelData levelData)
        {
            _levelSystem.SelectLevel(levelData);
        }

        protected override void OnBeforeClose()
        {
            if (_stageMap == null)
                return;

            Destroy(_stageMap.gameObject);
        }
    }

    public record CampaignStagePageOpenParam(StageData StageData) : IUIElementOpenParam
    {
        public StageData StageData { get; } = StageData;
    }
}