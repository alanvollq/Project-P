﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Core.Systems.UI;
using Core.Systems.UI.PageSystem;
using Core.Systems.UI.PopupSystem;
using DG.Tweening;
using Game.Data;
using Game.Data.Units.Enemies;
using Game.QualityTypes;
using Game.UI.Pages.UIElements.EnemiesLibrary;
using Game.UI.Popups;
using Game.UI.UIElements;
using Game.UI.Views;
using Game.Units;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Pages
{
    public class EnemiesLibraryPage : BasePage
    {
        [SerializeField] private Button _backButton;
        [SerializeField] private EnemiesDataContainer _enemiesDataContainer;
        [SerializeField] private EnemiesLibraryListView _enemiesLibraryListView;
        [SerializeField] private Button _qualityTypeSortButton;
        [SerializeField] private Button _creatureTypeSortButton;
        [SerializeField] private Button _filterButton;
        [SerializeField] private ItemCountView _filterCountView;
        [SerializeField] private Button _typeInfoButton;
        [SerializeField] private Button _clearButton;
        [SerializeField] private TMP_Text _qualitySortLabel;
        [SerializeField] private TMP_Text _typeSortLabel;
        [SerializeField] private Image _typeSortGlow;
        [SerializeField] private Image _qualitySortGlow;
        [SerializeField] private Color _selectedColor;
        [SerializeField] private Color _unselectedColor;

        private readonly List<EnemiesLibraryListItem> _enemyItems = new();
        private ScrollRect _scrollRect;
        private IPopupSystem _popupSystem;
        
        private readonly FilterHandler<UnitType> _unitTypeFilter = new();
        private readonly FilterHandler<QualityType> _qualityFilter = new();
        
        private const float SelectDuration = 0.25f;


        protected override void OnInitialization()
        {
            _popupSystem = GameContext.GetSystem<IPopupSystem>();
            
            _filterCountView.SetEnable(false);
            
            _backButton.onClick.AddListener(OnBackButtonClick);
            _filterButton.onClick.AddListener(OnFilterButtonDown);
            _clearButton.onClick.AddListener(OnClearButtonDown);
            _typeInfoButton.onClick.AddListener(OnTypeInfoButtonDown);

            _enemiesLibraryListView.Initialization();
            _scrollRect = _enemiesLibraryListView.GetComponent<ScrollRect>();

            foreach (var enemyData in _enemiesDataContainer.Data)
                _enemyItems.Add(new EnemiesLibraryListItem(enemyData));

            QualityTypeSort();

            _unitTypeFilter.Changed += OnFiltersChanged;
            _qualityFilter.Changed += OnFiltersChanged;
            
            _qualityTypeSortButton.onClick.AddListener(QualityTypeSort);
            _creatureTypeSortButton.onClick.AddListener(CreatureTypeSort);

            EnemiesLibraryListItem.Selected += OnItemSelected;
        }
        
        private void OnClearButtonDown()
        {
            _unitTypeFilter.ClearFilter();
            _qualityFilter.ClearFilter();
        }
        
        private void OnFiltersChanged()
        {
            var filterCount = _qualityFilter.FilterCount + _unitTypeFilter.FilterCount;
            _filterCountView.SetCount(filterCount);
            _filterCountView.SetEnable(filterCount > 0);

            SetVisible();
        }

        private void OnItemSelected(EnemyDataByQuality enemyDataByQuality)
        {
            var openParam = new EnemyInfoPopupOpenParam(enemyDataByQuality);
            _popupSystem.Open<EnemyInfoPopup>(openParam);
        }

        protected override void OnAfterOpen(IUIElementOpenParam openParam)
        {
            _scrollRect.SetContentOnStart();
        }

        private int CreatureItemQualityCompare(EnemiesLibraryListItem itemA, EnemiesLibraryListItem itemB)
        {
            var itemAQualityLevel = itemA.EnemyDataByQuality.QualityType.Level;
            var itemBQualityLevel = itemB.EnemyDataByQuality.QualityType.Level;
            return itemBQualityLevel - itemAQualityLevel;
        }

        private int CreatureItemTypeCompare(EnemiesLibraryListItem itemA, EnemiesLibraryListItem itemB)
        {
            var itemACreatureType = itemA.EnemyDataByQuality.UnitData.UnitType;
            var itemBCreatureType = itemB.EnemyDataByQuality.UnitData.UnitType;
            return string.Compare(itemBCreatureType.Name, itemACreatureType.Name, StringComparison.Ordinal);
        }

        private int QualityCompare(EnemiesLibraryListItem itemA, EnemiesLibraryListItem itemB)
        {
            var qualityValue = CreatureItemQualityCompare(itemA, itemB);
            qualityValue *= 1000;
            var typeValue = CreatureItemTypeCompare(itemA, itemB);
            return qualityValue - typeValue;
        }

        private int TypeCompare(EnemiesLibraryListItem itemA, EnemiesLibraryListItem itemB)
        {
            var typeValue = CreatureItemTypeCompare(itemA, itemB);
            typeValue *= 1000;
            var qualityValue = CreatureItemQualityCompare(itemA, itemB);
            return qualityValue - typeValue;
        }

        private void SetVisible()
        {
            var hasQualityTypeFilters = _qualityFilter.FilterCount > 0;
            var hasUnitTypeFilters = _unitTypeFilter.FilterCount > 0;
            
            if (hasUnitTypeFilters == false && hasQualityTypeFilters == false)
            {
                foreach (var item in _enemyItems)
                    item.SetVisible(true);

                return;
            }

            var qualityTypeFilters = _qualityFilter.Types.ToList();
            var unitTypeFilters = _unitTypeFilter.Types.ToList();
            
            foreach (var enemyItem in _enemyItems)
            {
                var qualityTypeVisible = true;
                var unitTypeVisible = true;

                var enemyDataByQuality = enemyItem.EnemyDataByQuality;
                var qualityType = enemyDataByQuality.QualityType;
                var unitType = enemyDataByQuality.UnitData.UnitType;
            

                if (hasQualityTypeFilters)
                    qualityTypeVisible = qualityTypeFilters.Any(filterType => filterType == qualityType);

                if (hasUnitTypeFilters)
                    unitTypeVisible = unitTypeFilters.Any(filterType => filterType == unitType);

                enemyItem.SetVisible(unitTypeVisible && qualityTypeVisible);
            }

            _scrollRect.SetContentOnStart();
        }

        private void QualityTypeSort()
        {
            _enemiesLibraryListView.RemoveAll();

            _enemyItems.Sort(QualityCompare);

            _enemiesLibraryListView.AddItems(_enemyItems);
            SetVisible();
            
            _qualitySortLabel.DOColor(_selectedColor, SelectDuration);
            _typeSortLabel.DOColor(_unselectedColor, SelectDuration);
            _qualitySortGlow.DOFade(1, SelectDuration);
            _typeSortGlow.DOFade(0, SelectDuration);
        }

        private void CreatureTypeSort()
        {
            _enemiesLibraryListView.RemoveAll();

            _enemyItems.Sort(TypeCompare);

            _enemiesLibraryListView.AddItems(_enemyItems);
            SetVisible();

            _qualitySortLabel.DOColor(_unselectedColor, SelectDuration);
            _typeSortLabel.DOColor(_selectedColor, SelectDuration);
            _qualitySortGlow.DOFade(0, SelectDuration);
            _typeSortGlow.DOFade(1, SelectDuration);
        }

        private void OnBackButtonClick()
        {
            PageSystem.Open<MainPage>();
        }
        
        private void OnTypeInfoButtonDown()
        {
            _popupSystem.Open<TypeInfoPopup>();
        }

        private void OnFilterButtonDown()
        {
             var openParam = new FilterPopupOpenParam(_qualityFilter, _unitTypeFilter);
            _popupSystem.Open<FilterPopup>(openParam);
        }
    }
}