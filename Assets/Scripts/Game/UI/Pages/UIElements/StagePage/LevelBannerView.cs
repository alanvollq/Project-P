﻿using System;
using Game.Levels;
using Game.Misc;
using TMPro;
using UnityEngine;

namespace Game.UI.Pages.UIElements.StagePage
{
    public class LevelBannerView : MonoBehaviour
    {
        public event Action<LevelData> LevelSelected;

        [SerializeField] private ClickHandler _clickHandler;
        [SerializeField] private GameObject _locker;
        [SerializeField] private SpriteRenderer _coloredLine;
        [SerializeField] private Color _completeColor;
        [SerializeField] private Color _availableColor;
        [SerializeField] private Color _lockColor;
        [SerializeField] private TMP_Text _levelText;
        
        private LevelData _levelData;


        private void Awake()
        {
            _clickHandler.Up += OnUp;
        }

        private void OnUp()
        {
            if(_levelData.Available == false)
                return;
            
            LevelSelected?.Invoke(_levelData);
        }

        public void SetInfo(LevelData levelData, int number)
        {
            _levelData = levelData;

            if (_levelData.Available)
                _coloredLine.color = _levelData.IsComplete ? _completeColor : _availableColor;
            else
                _coloredLine.color = _lockColor;
            
            _locker.SetActive(_levelData.Available == false);
            _levelText.gameObject.SetActive(_levelData.Available);
            _levelText.text = $"{number}";
        }
    }
}