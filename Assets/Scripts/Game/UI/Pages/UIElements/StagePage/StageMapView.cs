﻿using System.Collections.Generic;
using Core.Context;
using Game.Levels;
using UnityEngine;

namespace Game.UI.Pages.UIElements.StagePage
{
    public class StageMapView : MonoBehaviour
    {
        [SerializeField] private LevelMap _levelMap;
        [SerializeField] private List<LevelBannerView> _levelBannerViews;


        public List<LevelBannerView> LevelBannerViews => _levelBannerViews;

        public void Init(IGameContext gameContext)
        {
            _levelMap.Init(gameContext);
        }
    }
}