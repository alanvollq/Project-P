﻿using System.Linq;
using Core.UIElements.ListView;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Pages.UIElements.MainCampaign
{
    public class StageListItemView : ListItemView<StageListItem>
    {
        [SerializeField] private TMP_Text _nameText;
        [SerializeField] private TMP_Text _starsText;
        [SerializeField] private Image _previewImage;
        [SerializeField] private Button _selectButton;
        [SerializeField] private Image _coloredFrame;
        [SerializeField] private GameObject _lighting;
        [SerializeField] private GameObject _lockImage;
        [SerializeField] private GameObject _starsPanel;
        [SerializeField] private Color _notActiveColor;
        [SerializeField] private Color _completedColor;

        private StageListItem _item;

        protected override void OnSetItem(StageListItem stageListItem)
        {
            _item = stageListItem;

            var stageData = _item.StageData;
            _nameText.text = stageData.Name;
            _starsText.text = $"{stageData.Levels.Sum(level => level.IsComplete ? 3 : 0)} / {stageData.Levels.Count * 3}";
            _previewImage.sprite = stageData.PreviewSprite;
            
            var scale = 0.9f;
            var unActiveScale = new Vector3(scale, scale, 1);
            switch (stageData.IsComplete, stageData.IsAvailable)
            {
                case (true, true):
                    _coloredFrame.color = _completedColor;
                    transform.DOScale(unActiveScale, 0);
                    _lockImage.SetActive(false);
                    _starsPanel.SetActive(true);
                    _lighting.SetActive(false);
                    break;
                
                case (false, false):
                    _coloredFrame.color = _notActiveColor;
                    transform.DOScale(unActiveScale, 0);
                    _lockImage.SetActive(true);
                    _starsPanel.SetActive(false);
                    _lighting.SetActive(false);
                    break;
                
                case (false, true):
                    transform.DOScale(Vector3.one, 0);
                    _lockImage.SetActive(false);
                    _starsPanel.SetActive(true);
                    _lighting.SetActive(true);
                    break;
            }
        }

        protected override void OnRecycle()
        {
            if (_item == null)
                return;
        }

        protected override void OnRelease()
        {
            if (_item == null)
                return;
        }

        private void Awake()
        {
            _selectButton.onClick.AddListener(OnSelectButtonDown);
        }

        private void OnSelectButtonDown()
        {
            if(_item.StageData.IsAvailable == false)
                return;
            
            _item.Select();
        }
    }
}