﻿using System;
using Game.Levels.MainCampaign;

namespace Game.UI.Pages.UIElements.MainCampaign
{
    public record StageListItem(StageData StageData)
    {
        public event Action<StageListItem> Selected;

        
        public StageData StageData { get; } = StageData;


        public void Select() => Selected?.Invoke(this);
    }
}