﻿using System;
using Game.Creatures;
using Game.Data.Units.Creatures;

namespace Game.UI.Pages.UIElements.CreaturesLibrary
{
    public class CreaturesLibraryListItem
    {
        public static event Action<CreatureDataByQuality> Selected;
        public event Action<bool> VisibleChanged;

        
        public CreaturesLibraryListItem(CreatureDataByQuality creatureDataByQuality)
        {
            CreatureDataByQuality = creatureDataByQuality;
        }

        public CreatureDataByQuality CreatureDataByQuality { get; }


        public void SetVisible(bool value)
        {
            VisibleChanged?.Invoke(value);
        }

        public void Select()
        {
            Selected?.Invoke(CreatureDataByQuality);
        }
    }
}