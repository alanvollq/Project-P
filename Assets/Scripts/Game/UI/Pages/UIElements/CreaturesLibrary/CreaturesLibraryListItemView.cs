﻿using Core.UIElements.ListView;
using Game.UI.Views;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Pages.UIElements.CreaturesLibrary
{
    public class CreaturesLibraryListItemView : ListItemView<CreaturesLibraryListItem>
    {
        [SerializeField] private Image _icon;
        [SerializeField] private TMP_Text _nameText;
        [SerializeField] private Image _backgroundImage;
        [SerializeField] private Image _glowImage;
        [SerializeField] private UnitTypeView _typeView;
        [SerializeField] private Button _button;

        private CreaturesLibraryListItem _creaturesLibraryListItem;


        private void Awake()
        {
            _button.onClick.AddListener(OnButtonDown);
        }

        protected override void OnSetItem(CreaturesLibraryListItem creaturesLibraryListItem)
        {
            _creaturesLibraryListItem = creaturesLibraryListItem;
            var creatureDataByQuality = _creaturesLibraryListItem.CreatureDataByQuality;

            _icon.sprite = creatureDataByQuality.Icon;
            _nameText.text = creatureDataByQuality.UnitData.Name;
            _typeView.SetType(creatureDataByQuality.UnitData.UnitType);
            _backgroundImage.color = creatureDataByQuality.QualityType.SecondaryColor;
            _glowImage.color = creatureDataByQuality.QualityType.PrimaryColor;
        }

        private void OnButtonDown()
        {
            _creaturesLibraryListItem.Select();
        }

        protected override void OnRecycle()
        {
            if (_creaturesLibraryListItem != null)
                _creaturesLibraryListItem.VisibleChanged += OnVisibleChanged;
        }

        protected override void OnRelease()
        {
            if (_creaturesLibraryListItem != null)
                _creaturesLibraryListItem.VisibleChanged -= OnVisibleChanged;
        }

        private void OnVisibleChanged(bool value)
        {
            gameObject.SetActive(value);
        }
    }
}