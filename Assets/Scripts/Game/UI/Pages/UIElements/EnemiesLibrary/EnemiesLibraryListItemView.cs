﻿using Core.UIElements.ListView;
using Game.UI.Views;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Pages.UIElements.EnemiesLibrary
{
    public class EnemiesLibraryListItemView : ListItemView<EnemiesLibraryListItem>
    {
        [SerializeField] private Image _icon;
        [SerializeField] private TMP_Text _nameText;
        [SerializeField] private TMP_Text _qualityText;
        [SerializeField] private Image _border;
        [SerializeField] private UnitTypeView _typeView;
        [SerializeField] private Button _button;

        private EnemiesLibraryListItem _enemiesLibraryListItem;
        
        
        private void Awake()
        {
            _button.onClick.AddListener(OnButtonDown);
        }
        
        protected override void OnSetItem(EnemiesLibraryListItem enemiesLibraryListItem)
        {
            _enemiesLibraryListItem = enemiesLibraryListItem;

            _icon.sprite = _enemiesLibraryListItem.EnemyDataByQuality.Icon;
            _nameText.text = _enemiesLibraryListItem.EnemyDataByQuality.UnitData.Name;
            _qualityText.text = _enemiesLibraryListItem.EnemyDataByQuality.QualityType.Name;
            _qualityText.color = _enemiesLibraryListItem.EnemyDataByQuality.QualityType.PrimaryColor;
            _border.color = _enemiesLibraryListItem.EnemyDataByQuality.QualityType.PrimaryColor;
            _typeView.SetType(_enemiesLibraryListItem.EnemyDataByQuality.UnitData.UnitType);
        }
        
        private void OnButtonDown()
        {
            _enemiesLibraryListItem.Select();
        }

        protected override void OnRecycle()
        {
            if (_enemiesLibraryListItem != null)
                _enemiesLibraryListItem.VisibleChanged += OnVisibleChanged;
        }

        protected override void OnRelease()
        {
            if (_enemiesLibraryListItem != null)
                _enemiesLibraryListItem.VisibleChanged -= OnVisibleChanged;
        }

        private void OnVisibleChanged(bool value)
        {
            gameObject.SetActive(value);
        }
    }
}