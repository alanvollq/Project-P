﻿using System;
using Game.Data.Units.Enemies;
using Game.Enemies;

namespace Game.UI.Pages.UIElements.EnemiesLibrary
{
    public class EnemiesLibraryListItem
    {
        public static event Action<EnemyDataByQuality> Selected;
        public event Action<bool> VisibleChanged;


        public EnemiesLibraryListItem(EnemyDataByQuality enemyDataByQuality)
        {
            EnemyDataByQuality = enemyDataByQuality;
        }

        
        public EnemyDataByQuality EnemyDataByQuality { get; }


        public void SetVisible(bool value)
        {
            VisibleChanged?.Invoke(value);
        }
        
        public void Select()
        {
            Selected?.Invoke(EnemyDataByQuality);
        }
    }
}