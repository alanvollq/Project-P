﻿using Core.Systems.UI.PageSystem;
using Game.Managers.GameStateManager;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Pages
{
    public class StorePage : BasePage
    {
        [SerializeField] private Button _backButton;

        private IGameStateManager _gameStateManager;
        

        protected override void OnInitialization()
        {
            _gameStateManager = GameContext.GetManager<IGameStateManager>();
            
            _backButton.onClick.AddListener(OnBackButtonDown);
        }

        private void OnBackButtonDown()
        {
            _gameStateManager.OpenMenu();
        }
    }
}