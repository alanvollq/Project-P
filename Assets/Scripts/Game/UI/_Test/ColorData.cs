﻿using UnityEngine;

namespace Game.UI._Test
{
    [CreateAssetMenu(fileName = "Color Data", menuName = "Data/Misc/Color Data")]
    public class ColorData : ScriptableObject
    {
        [SerializeField] private Color _color;

        
        public Color Color => _color;
    }
}