﻿using TMPro;
using UnityEngine;

namespace Game.UI._Test
{
    [RequireComponent(typeof(TMP_Text))]
    public class TextColorSetter : MonoBehaviour
    {
        [SerializeField] private ColorData _colorData;

        private TMP_Text _text;


        private void Awake()
        {
            SetText();
        }

        [ContextMenu("Set Text")]
        private void SetText()
        {
            if (_text is null)
                _text = GetComponent<TMP_Text>();

            _text.color = _colorData.Color;
        }
    }
}