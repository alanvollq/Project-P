﻿using UnityEngine;
using UnityEngine.Serialization;

namespace Game.UI._Test
{
    [CreateAssetMenu(fileName = "Size Data", menuName = "Data/Misc/Size Data")]
    public class SizeData : ScriptableObject
    {
        [SerializeField] private int _size;


        public int Size => _size;
    }
}