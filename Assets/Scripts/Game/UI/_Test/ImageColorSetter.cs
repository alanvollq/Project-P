﻿using UnityEngine;
using UnityEngine.UI;

namespace Game.UI._Test
{
    [RequireComponent(typeof(Image))]
    public class ImageColorSetter : MonoBehaviour
    {
        [SerializeField] private ColorData _colorData;

        private Image _image;


        private void Awake()
        {
            SetColor();
        }

        [ContextMenu("Set Color")]
        private void SetColor()
        {
            if (_image is null)
                _image = GetComponent<Image>();

            _image.color = _colorData.Color;
        }
    }
}