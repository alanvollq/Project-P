﻿using TMPro;
using UnityEngine;

namespace Game.UI._Test
{
    [RequireComponent(typeof(TMP_Text))]
    public class TextSizeSetter : MonoBehaviour
    {
        [SerializeField] private SizeData _sizeData;

        private TMP_Text _text;


        private void Awake()
        {
            SetText();
        }

        [ContextMenu("Set Text")]
        private void SetText()
        {
            if (_text is null)
                _text = GetComponent<TMP_Text>();

            _text.fontSize = _sizeData.Size;
        }
    }
}