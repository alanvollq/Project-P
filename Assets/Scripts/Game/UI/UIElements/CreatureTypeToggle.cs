﻿using System;
using Game.UI.Views;
using Game.Units;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.UIElements
{
    [RequireComponent(typeof(Toggle))]
    public class CreatureTypeToggle : MonoBehaviour
    {
        public event Action<UnitType, bool> Toggled;

        [SerializeField] private Toggle _toggle;
        [SerializeField] private UnitType _unitType;
        [SerializeField] private UnitTypeView _unitTypeView;
        [SerializeField] private TMP_Text _nameText;
        [SerializeField] private Color _enableColor;


        public UnitType CreatureType => _unitType;
        public bool IsOn => _toggle.isOn;


        private void Awake()
        {
            SetInfo();
            _toggle.onValueChanged.AddListener(OnToggle);
        }

        private void OnToggle(bool value)
        {
            Toggled?.Invoke(_unitType, value);
            _nameText.color = value ? _enableColor : Color.white;
        }

        public void Toggle()
        {
            _toggle.isOn = !_toggle.isOn;
        }
        
        [ContextMenu("SetInfo")]
        private void SetInfo()
        {
            _unitTypeView.SetType(_unitType);
            _nameText.text = _unitType.Name;
        }
    }
}