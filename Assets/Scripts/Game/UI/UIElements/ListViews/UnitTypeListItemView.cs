﻿using Core.UIElements.ListView;
using Game.UI.Views;
using Game.Units;
using UnityEngine;

namespace Game.UI.UIElements.ListViews
{
    public class UnitTypeListItemView : ListItemView<UnitType>
    {
        [SerializeField] private UnitTypeView _unitTypeView;


        protected override void OnSetItem(UnitType item)
        {
            _unitTypeView.SetType(item);
        }
    }
}