﻿using System;
using Game.Creatures;
using Game.Data.Units.Creatures;
using Game.Inventory;

namespace Game.UI.UIElements.ScrollViews
{
    public class TowerSlot
    {
        public event Action<CreatureDataByQuality> ItemChanged;
        public event Action<TowerSlot> Selected;
        public event Action Deselected;

        private CreatureDataByQuality _creatureDataByQuality;


        public CreatureDataByQuality CreatureDataByQuality => _creatureDataByQuality;
        public bool IsEmpty => _creatureDataByQuality == null;

        
        public TowerSlot(CreatureDataByQuality creatureDataByQuality)
        {
            SetTowerInventoryItem(creatureDataByQuality);
        }

        public void SetTowerInventoryItem(CreatureDataByQuality creatureDataByQuality)
        {
            // if (_creatureDataByQuality != null)
            //     _creatureDataByQuality.SetOnLevel(false);
            //
            // _creatureDataByQuality = creatureDataByQuality;
            // if (_creatureDataByQuality != null)
            //     _creatureDataByQuality.SetOnLevel(true);
            
            ItemChanged?.Invoke(_creatureDataByQuality);
        }

        public void Select()
        {
            Selected?.Invoke(this);
        }

        public void Deselect()
        {
            Deselected?.Invoke();
        }
    }
}