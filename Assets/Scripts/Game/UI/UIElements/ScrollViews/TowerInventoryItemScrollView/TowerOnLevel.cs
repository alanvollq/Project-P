﻿using System;
using Game.Creatures;
using Game.Data.Units.Creatures;
using Game.Inventory;

namespace Game.UI.UIElements.ScrollViews.TowerInventoryItemScrollView
{
    public class TowerOnLevel
    {
        public event Action<CreatureDataByQuality> Selected;

        private readonly CreatureDataByQuality _creatureDataByQuality;


        public CreatureDataByQuality CreatureDataByQuality => _creatureDataByQuality;


        public TowerOnLevel(CreatureDataByQuality creatureDataByQuality)
        {
            _creatureDataByQuality = creatureDataByQuality;
        }

        public void Select()
        {
            Selected?.Invoke(_creatureDataByQuality);
        }
    }
}