﻿using Core.Context;
using Game.Systems.CameraSystem;
using UnityEngine;

namespace Game.UI.UIElements
{
    public class WorldMarkView : MonoBehaviour
    {
        [SerializeField] private RectTransform _rectTransform;
        [SerializeField] private RectTransform _targetImage;
        [SerializeField] private RectTransform _arrowRoot;
        [SerializeField] private RectTransform _imageRoot;
        [SerializeField] private Vector2 _offset;

        private Transform _targetTransform;
        private Camera _camera;
        private bool _canShow = true;

        
        private void LateUpdate()
        {
            if (_targetTransform == null)
                return;

            var screenPosition = _camera.WorldToScreenPoint(_targetTransform.position);
            var rect = new Rect(0, 0, Screen.width, Screen.height);

            var needShow = _canShow && rect.Contains(screenPosition) == false;
            _arrowRoot.gameObject.SetActive(needShow);
            _targetImage.gameObject.SetActive(needShow);

            if (needShow == false)
                return;

            var sizeDelta = _rectTransform.sizeDelta;
            var sizeX = sizeDelta.x / 2 + _offset.x;
            var sizeY = sizeDelta.y / 2 + _offset.y;
            var positionInScreen = screenPosition;
            positionInScreen.x = Mathf.Clamp(screenPosition.x, sizeX, Screen.width - sizeX);
            positionInScreen.y = Mathf.Clamp(screenPosition.y, sizeY, Screen.height - sizeY);

            _rectTransform.position = positionInScreen;

            var position = screenPosition - positionInScreen;
            var angle = Mathf.Atan2(position.y, position.x) * Mathf.Rad2Deg;
            _arrowRoot.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            _targetImage.position = _imageRoot.position;
        }


        public void Init(IGameContext gameContext)
        {
            var cameraSystem = gameContext.GetSystem<ICameraSystem>();
            _camera = cameraSystem.MainCamera;
        }

        public void SetItem(Transform targetTransform)
        {
            _targetTransform = targetTransform;
        }

        public void SetShowAvailable(bool value)
        {
            _canShow = value;
        }
    }
}