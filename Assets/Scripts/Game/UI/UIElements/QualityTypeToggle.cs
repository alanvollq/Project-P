﻿using System;
using Game.QualityTypes;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.UIElements
{
    [RequireComponent(typeof(Toggle))]
    public class QualityTypeToggle : MonoBehaviour
    {
        public event Action<QualityType, bool> Toggled;

        [SerializeField] private Toggle _toggle;
        [SerializeField] private QualityType _qualityType;
        [SerializeField] private Image _icon;
        [SerializeField] private TMP_Text _qualityName;
        [SerializeField] private Color _enableTextColor;


        public QualityType QualityType => _qualityType;
        public bool IsOn => _toggle.isOn;


        private void Awake()
        {
            SetInfo();
            _toggle.onValueChanged.AddListener(OnToggle);
        }

        private void OnToggle(bool value)
        {
            Toggled?.Invoke(_qualityType, value);
            _qualityName.color = value ? _enableTextColor : Color.white;
        }
        
        public void Toggle()
        {
            _toggle.isOn = !_toggle.isOn;
        }

        [ContextMenu("SetInfo")]
        private void SetInfo()
        {
            _icon.color = _qualityType.PrimaryColor;
            _qualityName.text = _qualityType.Name;
        }
    }
}