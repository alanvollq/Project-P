using System;
using Core.Context;
using Core.Systems.TimeSystem;
using Game.Abilities;
using Game.Creatures;
using Game.Data.Units.Creatures;
using Game.Data.Units.Creatures.Abilities;
using Game.Systems.DamageSystem;
using UnityEngine;
using UnityEngine.Serialization;

namespace Game.Towers
{
    public class TowerController : MonoBehaviour, IDamageDealer
    {
        [SerializeField] private TowerView _towerView;
        [FormerlySerializedAs("_radiusHandler")] [SerializeField] private TargetHandler _targetHandler;
        
        private AbilitySlotsData _abilities;

      
        private float _attackLeftTime;
        private float _allDamage;
        private Weapon _weapon;
        private CreatureData _creatureData;
        private ITimeSystem _timeSystem;

        private static readonly int Attack = Animator.StringToHash("Attack");
        private CreatureDataByQuality _creatureDataByQuality;

        //
        //
        // public WeaponStats WeaponStats => _abilities.WeaponAbilitySlot.ActiveAbility.Stats;
        // public float AllDamage => _allDamage;
        // public CreatureType Type => _type;
        //
        // public AbilitySlots Abilities => _abilities;

        public CreatureData CreatureData => _creatureData;

        public CreatureDataByQuality CreatureDataByQuality => _creatureDataByQuality;
        //
        //
        //

        public void Init(IGameContext gameContext)
        {
            _timeSystem = gameContext.GetSystem<ITimeSystem>();
            _timeSystem.Paused += _towerView.PauseAnimation;
            _timeSystem.Played += _towerView.PlayAnimation;
            
            _towerView.Init(this, gameContext);
            // _weapon = new Weapon(_abilities.WeaponAbilitySlotData.FirstAbility, _targetHandler, this, transform, _timeSystem);
            _weapon.Attack += OnAttack;
            _towerView.SetRadiusEnable(false);
        }

        private void OnDestroy()
        {
            _timeSystem.Paused -= _towerView.PauseAnimation;
            _timeSystem.Played -= _towerView.PlayAnimation;
        }

        private void OnAttack()
        {
            _towerView.PlayAttackAnimation();
        }

        private void Update()
        {
            _weapon?.Update();
        }
        
        public void Selected()
        {
            _towerView.SetRadiusEnable(true);
            // TowerLogData.Log($"{name} - select");
        }
        
        public void Deselected()
        {
            _towerView.SetRadiusEnable(false);
            // TowerLogData.Log($"{name} - deselect");
        }
        
        public void SetTowerData(CreatureDataByQuality creatureDataByQuality)
        {
            _creatureDataByQuality = creatureDataByQuality;
            _creatureData = creatureDataByQuality.UnitData;
            _abilities = _creatureData.AbilitySlotsData;
            // _towerView.SetRadiusScale(_creatureData.Abilities.WeaponAbilitySlot.ActiveAbility.Stats.Range);
        }
        
        public event Action<DamageResult> OnDealDamage;
        
        public void AddDamageResult(DamageResult damageResult)
        {
            //     //TODO прокинуть урон через систему урона, а не через диллера.
            //     _allDamage += damageResult.Damage;
            //     OnDealDamage?.Invoke(damageResult);
        }
    }
}