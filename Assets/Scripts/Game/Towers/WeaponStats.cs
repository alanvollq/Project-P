using System;
using UnityEngine;

namespace Game.Towers
{
    [Serializable]
    public class WeaponStats
    {
        [SerializeField] private float _range;
        [SerializeField] private float _damage;
        [SerializeField] private float _attackInterval;


        public float Range => _range;
        public float Damage => _damage;
        public float AttackInterval => _attackInterval;
        public float AttackSpeed => 1 / _attackInterval;
    }
}