using Core.Context;
using Game.Misc;
using UnityEngine;

namespace Game.Towers
{
    public class TowerView : MonoBehaviour
    {
        [SerializeField] private Transform _radiusView;
        [SerializeField] private SpriteRenderer _spriteRenderer;
        [SerializeField] private TowerController _towerController;
        [SerializeField] private ClickHandler _clickHandler;

        private ITowerManager _towerManager;
        private Color _defaultColor;
        private Animator _animator;
        
        private static readonly int Attack = Animator.StringToHash("Attack");


        private void Awake()
        {
            _animator = GetComponentInChildren<Animator>(true);
            
            _clickHandler.Down += OnDown;
            
            _defaultColor = _spriteRenderer.color;
        }

        public void Init(TowerController towerController, IGameContext gameContext)
        {
            _towerController = towerController;
            
            _towerManager = gameContext.GetManager<ITowerManager>();
        }

        private void OnDown()
        {
            // TODO: Перенести на ивент и обработать в контроллере.
            _towerManager.Select(_towerController);
        }

        public void SetRadiusEnable(bool value)
        {
            _spriteRenderer.color = value ? _defaultColor : Color.clear;
            _spriteRenderer.enabled = value;
        }

        public void SetRadiusScale(float value)
        {
            value = value * 2 + 1;
            _radiusView.localScale = new Vector3(value, value, 1);
        }

        public void PlayAttackAnimation()
        {
            _animator.SetTrigger(Attack);
        }

        public void PauseAnimation()
        {
            _animator.enabled = false;
        }

        public void PlayAnimation(float pauseTime)
        {
            _animator.enabled = true;
        }
    }
}