using Core.Context;
using Core.Systems.UI.PopupSystem;
using Game.UI.Popups;

namespace Game.Towers
{
    public class TowerManager : ITowerManager, IGameContextInitializable
    {
        private IGameContext _gameContext;
        private IPopupSystem _popupSystem;

        
        public void Initialization(IGameContext gameContext)
        {
            _gameContext = gameContext;
            _popupSystem = _gameContext.GetSystem<IPopupSystem>();
        }
        
        public void Select(TowerController towerController)
        {
            var openParam = new CreaturePopupOpenParam(towerController);
            _popupSystem.Open<CreaturePopup>(openParam);
        }
    }
}