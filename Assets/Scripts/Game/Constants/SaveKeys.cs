﻿namespace Game.Constants
{
    public static class SaveKeys
    {
        public static string LevelCompletedSaveKey(string name) => $"Level - {name} completed value";
    }
}