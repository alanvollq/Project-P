namespace Game.Misc.Interfaces
{
    public interface IClickable
    {
        public event System.Action Down;
        public event System.Action Up;
    }
}