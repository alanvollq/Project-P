﻿using System;

namespace Game.Misc
{
    public class Counter
    {
        public event Action ValueChanged;
        public event Action ValueDepleted;
        public event Action ValueReachedMax;
        private int _maxValue;
        private int _currentValue;


        public int MaxValue => _maxValue;
        public int CurrentValue => _currentValue;
        public bool IsMax => _currentValue == _maxValue;
        public bool IsDepleted => _currentValue == 0;


        public void SetMax(int maxValue)
        {
            _maxValue = maxValue;
            _currentValue = maxValue;
        }

        public void SetCurrentToMax()
        {
            SetCurrentValue(_maxValue);
        }

        public void SetCurrentToZero()
        {
            SetCurrentValue(0);
        }

        public void SetCurrentValue(int value)
        {
            if(value < 0 || value > _maxValue)
                return;
            
            _currentValue = value;
            ValueChanged?.Invoke();
            
            if (_currentValue == 0)
                ValueDepleted?.Invoke();
            
            if (_currentValue == _maxValue)
                ValueReachedMax?.Invoke();
        }

        public void DecreaseCount()
        {
            SetCurrentValue(_currentValue - 1);
        }

        public void IncreaseValue()
        {
            SetCurrentValue(_currentValue + 1);
        }
    }
}