﻿using System;
using Core.Pool;
using UnityEngine;

namespace Game
{
    public class ClickEffect : MonoBehaviour, IPoolable
    {
        public event Action<ClickEffect> Completed;
        
        [SerializeField] private ParticleSystem _particleSystem;

        private float _timeLeft;

        private void Awake()
        {
            _particleSystem.Stop();
        }

        private void Update()
        {
            _timeLeft -= Time.deltaTime;
            if(_timeLeft > 0)
                return;
            
            Completed?.Invoke(this);
        }

        public void Recycle()
        {
            _timeLeft = _particleSystem.duration + _particleSystem.startLifetime;
        }

        public void Release()
        {
        }
    }
}