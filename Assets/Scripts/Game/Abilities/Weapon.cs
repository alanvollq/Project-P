using System;
using Game.Charges;
using Game.Creatures;
using Game.Data.Units.Creatures.Abilities;
using Game.Systems.DamageSystem;
using Game.Towers;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Game.Abilities
{
    public class Weapon
    {
        public event Action Attack;

        private readonly TargetHandler _targetHandler;
        private WeaponStats _stats;
        private Charge _chargePrefab;
        private float _attackLeftTime;
        private readonly IDamageDealer _damageDealer;
        private readonly Transform _parent;

        public Weapon(TargetHandler targetHandler, IDamageDealer damageDealer,
            Transform parent)
        {
            _targetHandler = targetHandler;
            _damageDealer = damageDealer;
            _parent = parent;
        }

        public void Update()
        {
            _attackLeftTime -= Time.deltaTime;
            if (_attackLeftTime > 0)
                return;

            _attackLeftTime = _stats.AttackInterval;

            if (_targetHandler.HasTarget == false)
                return;

            var enemy = _targetHandler.GetTarget();
            
            var charge = Object.Instantiate(_chargePrefab, _parent);
            charge.transform.position = _parent.position;
            var damage = new Damage
            {
                Owner = _damageDealer,
                Value = _stats.Damage
            };
            charge.Init();
            charge.SetDamage(damage);
            charge.MoveToTarget(enemy.transform);

            Attack?.Invoke();
        }

        public void SetAbilityData(WeaponAbilityData weaponAbilityData)
        {
            _stats = weaponAbilityData.Stats;
            _chargePrefab = weaponAbilityData.ChargePrefab;
        }
    }
}