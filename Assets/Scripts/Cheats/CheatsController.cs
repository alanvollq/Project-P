﻿using Core.Context;
using UnityEngine;

namespace Cheats
{
    public class CheatsController : MonoBehaviour, IGameContextInitializable
    {
        private IGameContext _gameContext;

        public void Initialization(IGameContext gameContext)
        {
            _gameContext = gameContext;
        }
    }
}