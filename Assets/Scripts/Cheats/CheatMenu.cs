﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Cheats
{
    public class CheatMenu : MonoBehaviour
    {
        [SerializeField] private Button _enableButton;
        [SerializeField] private GameObject _panel;
        [SerializeField] private CheatsController _cheatsController;


        private void Awake()
        {
            _enableButton.onClick.AddListener(OnEnableButtonDown);
        }

        private void OnEnableButtonDown()
        {
            _panel.SetActive(!_panel.activeSelf);
        }
    }
}