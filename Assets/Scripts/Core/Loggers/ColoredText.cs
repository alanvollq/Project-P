using UnityEngine;

namespace Core.Loggers
{
    [System.Serializable]
    public class ColoredText
    {
        [SerializeField] private string text;
        [SerializeField] private Color _color = Color.white;

        public string Text => text;
        public Color Color => _color;
    }
}