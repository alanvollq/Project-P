using System.Collections.Generic;
using UnityEngine;

namespace Core.Loggers
{
    public interface ILogData
    {
        public Color TextColor { get; }
        public bool IsEnable => true;
        
        public List<ColoredText> GetPrefixes();
    }
}