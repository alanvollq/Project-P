﻿using Core.Systems.UI.TooltipSystem;
using UnityEngine;

namespace Core.Data.UIContainers
{
    [CreateAssetMenu(fileName = "Tooltip Container", menuName = "Data/Core/Managers/UI/Tooltip Container")]
    public class TooltipContainer : DataContainer<BaseTooltip> { }
}