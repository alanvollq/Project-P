﻿using System;
using System.Collections.Generic;

namespace Core.Misc.Enumeration
{
    public class BaseEnumeration<TType> :  IComparable<BaseEnumeration<TType>>
        where TType : BaseEnumeration<TType>
    {
        private static readonly Dictionary<string, TType> _values = new();
        private readonly string _name;


        protected BaseEnumeration(string name)
        {
            _name = name;

            _values.Add(_name, (TType)this);
        }

        public static IEnumerable<TType> Values => _values.Values;


        public static bool TryParse(string name, out TType? enumeration)
        {
            return _values.TryGetValue(name, out enumeration);
        }
        
        public static TType Parse(string name)
        {
            return _values[name];
        }

        public static implicit operator BaseEnumeration<TType>(string name)
        {
            return _values[name];
        }

        public override string ToString()
        {
            return _name;
        }
        
        public int CompareTo(BaseEnumeration<TType> other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            return string.Compare(_name, other._name, StringComparison.Ordinal);
        }
    }
}