﻿using System.Collections.Generic;

namespace Core.Misc.Enumeration
{
    public class Enumeration<TType, TValue> : BaseEnumeration<TType> 
        where TType : Enumeration<TType, TValue> where TValue : notnull
    {
        private static readonly Dictionary<TValue, TType> _valueDictionary = new();
    
    
        protected Enumeration(string name, TValue value) : base(name)
        {
            Value = value;
            _valueDictionary.Add(Value, (TType)this);
        }
    
        public TValue Value { get; }
    
        public static bool TryParse(TValue value, out TType enumeration)
        {
            return _valueDictionary.TryGetValue(value, out enumeration);
        }
    
        public static explicit operator TValue(Enumeration<TType, TValue> enumeration)
        {
            return enumeration.Value;
        }

        public static implicit operator Enumeration<TType, TValue>(TValue value)
        {
            return _valueDictionary[value];
        }
    }
}