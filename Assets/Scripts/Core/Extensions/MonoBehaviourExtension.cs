using UnityEngine;

namespace Core.Extensions
{
    public static class MonoBehaviourExtension
    {
        public static T Instantiate<T>(this T component, Transform parent = null) where T : Object
        {
            return parent is null ? Object.Instantiate(component) : Object.Instantiate(component, parent);
        }

        public static GameObject Instantiate(this GameObject gameObject, Transform parent = null)
        {
            return parent is null ? Object.Instantiate(gameObject) : Object.Instantiate(gameObject, parent);
        }

        public static void DestroyGO(this GameObject gameObject)
        {
            Object.Destroy(gameObject);
        }

        public static void DestroyGO(this Component component)
        {
            Object.Destroy(component.gameObject);
        }
    }
}