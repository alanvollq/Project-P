﻿namespace Core.Extensions
{
    public static class BaseTypeExtensions
    {
        /// <summary>
        /// Converts an integer value to a boolean.
        /// </summary>
        /// <param name="value">The integer value to convert.</param>
        /// <returns>Returns true if the value is non-zero, otherwise false.</returns>
        public static bool ToBool(this int value)
        {
            return value != 0;
        }
        
        /// <summary>
        /// Converts a boolean value to an integer.
        /// </summary>
        /// <param name="value">The boolean value to convert.</param>
        /// <returns>Returns 1 if the value is true, otherwise 0.</returns>
        public static int ToInt(this bool value)
        {
            return value ? 1 : 0;
        }
    }
}