﻿using System;
using UnityEngine;

namespace Core.StateMachine
{
    public abstract class StateMachine<TStateId, TTriggerId>
    {
        protected readonly IFiniteStateMachine<TStateId, TTriggerId> FiniteStateMachine =
            new FiniteStateMachine<TStateId, TTriggerId>();


        protected abstract void CreateStates();

        protected abstract void CreateTransitions();

        public void Initialization()
        {
            CreateStates();
            CreateTransitions();
        }

        public void InvokeTrigger(TTriggerId triggerId)
        {
            FiniteStateMachine.InvokeTrigger(triggerId);
        }

        public void Enable()
        {
            FiniteStateMachine.Enable();
            OnEnable();
        }

        public void Update()
        {
            FiniteStateMachine.Update();
            OnUpdate();
        }

        public void Disable()
        {
            FiniteStateMachine.Disable();
            OnDisable();
        }

        protected virtual void OnEnable()
        {
        }

        protected virtual void OnUpdate()
        {
        }

        protected virtual void OnDisable()
        {
        }

        protected virtual void OnStateChanged()
        {
        }
    }
}