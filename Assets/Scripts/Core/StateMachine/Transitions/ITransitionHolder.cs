﻿using System;

namespace Core.StateMachine.Transitions
{
    public interface ITransitionHolder<TStateId, in TTriggerId>
    {
        public ITransitionHolder<TStateId, TTriggerId> AddTransition(TStateId toState, TStateId fromState,
            Func<bool> condition = null);

        public ITransitionHolder<TStateId, TTriggerId> AddTransitionFromAny(TStateId toState,
            Func<bool> condition = null);

        public ITransitionHolder<TStateId, TTriggerId> AddTriggerTransition(TStateId toState, TStateId fromState,
            TTriggerId triggerId, Func<bool> condition = null);

        public ITransitionHolder<TStateId, TTriggerId> AddTriggerTransitionFromAny(TStateId toState,
            TTriggerId triggerId, Func<bool> condition = null);


        public bool TryGetAvailableTransition(TStateId currentState, out ITransitionFromAny<TStateId> outTransition);
        public bool TryGetAvailableTriggerTransition(TTriggerId trigger,TStateId currentState, out ITransitionFromAny<TStateId> outTransition);
    }
}