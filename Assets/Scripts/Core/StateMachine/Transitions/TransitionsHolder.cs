﻿using System;
using System.Collections.Generic;

namespace Core.StateMachine.Transitions
{
    public class TransitionsHolder<TStateId, TTriggerId> : ITransitionHolder<TStateId, TTriggerId>
    {
        private readonly Dictionary<(TStateId fromState, TStateId toState), ITransition<TStateId>> _transitions = new();
        private readonly Dictionary<TStateId, ITransitionFromAny<TStateId>> _transitionsFromAny = new();

        private readonly Dictionary<(TTriggerId trigger, TStateId toState), ITransitionFromAny<TStateId>>
            _triggerTransitionsFromAny = new();

        private readonly
            Dictionary<(TTriggerId trigger, TStateId fromState, TStateId toState), ITransitionFromAny<TStateId>>
            _triggerTransitions = new();


        public ITransitionHolder<TStateId, TTriggerId> AddTransition(
            TStateId fromState, TStateId toState, Func<bool> condition = null)
        {
            var transition = new Transition<TStateId>(fromState, toState, condition);
            _transitions.Add((fromState, toState), transition);
            return this;
        }

        public ITransitionHolder<TStateId, TTriggerId> AddTransitionFromAny(
            TStateId toState, Func<bool> condition = null)
        {
            var transition = new TransitionFromAny<TStateId>(toState, condition);
            _transitionsFromAny.Add(toState, transition);
            return this;
        }

        public ITransitionHolder<TStateId, TTriggerId> AddTriggerTransition(
            TStateId fromState, TStateId toState, TTriggerId triggerId, Func<bool> condition = null)
        {
            var transition = new Transition<TStateId>(fromState, toState, condition);
            _triggerTransitions.Add((triggerId, fromState, toState), transition);
            return this;
        }

        public ITransitionHolder<TStateId, TTriggerId> AddTriggerTransitionFromAny(
            TStateId toState, TTriggerId triggerId, Func<bool> condition = null)
        {
            var transition = new TransitionFromAny<TStateId>(toState, condition);
            _triggerTransitionsFromAny.Add((triggerId, toState), transition);
            return this;
        }

        public bool TryGetAvailableTransition(TStateId currentState, out ITransitionFromAny<TStateId> outTransition)
        {
            foreach (var (_, transition) in _transitionsFromAny)
            {
                if (transition.IsAvailable == false)
                    continue;

                outTransition = transition;
                return true;
            }

            foreach (var (key, transition) in _transitions)
            {
                if (EqualityComparer<TStateId>.Default.Equals(key.fromState, currentState) == false)
                    continue;

                if (transition.IsAvailable == false)
                    continue;

                outTransition = transition;
                return true;
            }


            outTransition = null;
            return false;
        }

        public bool TryGetAvailableTriggerTransition(TTriggerId trigger, TStateId currentState,
            out ITransitionFromAny<TStateId> outTransition)
        {
            foreach (var ((triggerId, _), transition) in _triggerTransitionsFromAny)
            {
                if (EqualityComparer<TTriggerId>.Default.Equals(triggerId, trigger) == false)
                    continue;

                if (transition.IsAvailable == false)
                    continue;

                outTransition = transition;
                return true;
            }

            foreach (var (key, transition) in _triggerTransitions)
            {
                if (EqualityComparer<TTriggerId>.Default.Equals(key.trigger, trigger) == false)
                    continue;

                if (EqualityComparer<TStateId>.Default.Equals(key.fromState, currentState) == false)
                    continue;

                if (transition.IsAvailable == false)
                    continue;

                outTransition = transition;
                return true;
            }

            outTransition = null;
            return false;
        }
    }
}