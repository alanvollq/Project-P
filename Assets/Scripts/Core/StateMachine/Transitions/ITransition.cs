﻿namespace Core.StateMachine.Transitions
{
    public interface ITransition<out TStateId> : ITransitionFromAny<TStateId>
    {
        public TStateId FromState { get; }
    }
}