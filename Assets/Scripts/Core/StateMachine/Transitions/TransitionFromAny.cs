﻿using System;

namespace Core.StateMachine.Transitions
{
    public class TransitionFromAny<TStateId> : ITransitionFromAny<TStateId>
    {
        private readonly Func<bool> _condition;


        public TransitionFromAny(TStateId toState, Func<bool> condition = null)
        {
            ToState = toState;
            _condition = condition;
        }


        public TStateId ToState { get; }
        
        public bool IsAvailable => CheckAvailable();
        

        private bool CheckAvailable() => _condition is null || _condition.Invoke();
    }
}