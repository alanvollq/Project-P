﻿using System;
using Core.StateMachine.States;
using Core.StateMachine.Transitions;
using UnityEngine;

namespace Core.StateMachine
{
    public class FiniteStateMachine<TStateId, TTriggerId> : IFiniteStateMachine<TStateId, TTriggerId>
    {
        public event Action StateChanged;

        private readonly StateHolder<TStateId> _stateHolder = new();
        private readonly TransitionsHolder<TStateId, TTriggerId> _transitionsHolder = new();
        private TStateId _startState;
        private IState<TStateId> _previousState;
        private IState<TStateId> _currentState;
        private bool _isEnable;


        public IState<TStateId> PreviousState => _previousState;
        public IState<TStateId> CurrentState => _currentState;
        

        public IFiniteStateMachine<TStateId, TTriggerId> SetStartState(TStateId stateId)
        {
            _startState = stateId;
            return this;
        }

        public void InvokeTrigger(TTriggerId trigger)
        {
            if (_transitionsHolder.TryGetAvailableTriggerTransition(trigger, _currentState.Id, out var transition)
                && transition.IsAvailable)
            {
                SetState(transition.ToState);
            }
        }

        private void SetState(TStateId stateKey)
        {
            _previousState = _currentState;
            _previousState?.Exit();

            _currentState = _stateHolder[stateKey];
            _currentState.Enter();

            StateChanged?.Invoke();
        }

        public void Enable()
        {
            _isEnable = true;
            SetState(_startState);
        }

        public void Update()
        {
            if (_isEnable == false)
                return;

            if (_currentState is null)
                return;
            
            if(_currentState.IsExitAvailable == false)
                return;

            if (_transitionsHolder.TryGetAvailableTransition(_currentState.Id, out var transition))
            {
                SetState(transition.ToState);
                return;
            }

            _currentState.Execute();
        }

        public void Disable()
        {
            _isEnable = false;
            _currentState?.Exit();
        }
        
        
        #region AddStates

        public IFiniteStateMachine<TStateId, TTriggerId> AddState(IState<TStateId> state)
        {
            _stateHolder.AddState(state);
            return this;
        }

        public IFiniteStateMachine<TStateId, TTriggerId> AddState(
            TStateId stateId, Action onEnter = null, Action onExit = null, Action onExecute = null)
        {
            _stateHolder.AddState(stateId, onEnter, onExit, onExecute);
            return this;
        }

        #endregion
        
        
        #region AddTransitions

        public IFiniteStateMachine<TStateId, TTriggerId> AddTransition(
            TStateId fromState, TStateId toState, Func<bool> condition = null)
        {
            _transitionsHolder.AddTransition(fromState, toState, condition);
            return this;
        }

        public IFiniteStateMachine<TStateId, TTriggerId> AddTransition(TStateId toState, Func<bool> condition = null)
        {
            _transitionsHolder.AddTransitionFromAny(toState, condition);
            return this;
        }

        public IFiniteStateMachine<TStateId, TTriggerId> AddTransition(
            TStateId fromState, TStateId toState, TTriggerId triggerId, Func<bool> condition = null)
        {
            _transitionsHolder.AddTriggerTransition(fromState, toState, triggerId, condition);
            return this;
        }

        public IFiniteStateMachine<TStateId, TTriggerId> AddTransition(
            TStateId toState, TTriggerId triggerId, Func<bool> condition = null)
        {
            _transitionsHolder.AddTriggerTransitionFromAny(toState, triggerId, condition);
            return this;
        }

        #endregion
    }
}