using System.Collections.Generic;
using UnityEngine;

namespace Core.UIElements
{
    public class UIToggleGroup : MonoBehaviour
    {
        private readonly List<UIToggle> _toggles = new();


        public void AddToggle(UIToggle toggle)
        {
            _toggles.Add(toggle);
            toggle.SetValue(false);
        }

        public void SetActiveToggle(UIToggle uiToggle)
        {
            foreach (var toggle in _toggles)
            {
                if (toggle != uiToggle)
                    toggle.SetValue(false);
            }
        }
    }
}