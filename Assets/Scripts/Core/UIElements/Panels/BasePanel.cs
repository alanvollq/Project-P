using UnityEngine;
using UnityEngine.UI;

namespace Core.UIElements.Panels
{
    public abstract class BasePanel : MonoBehaviour
    {
        [SerializeField] private Button _closeButton;


        public bool IsActive => gameObject.activeSelf;


        private void Awake()
        {
            if (_closeButton is not null)
                _closeButton.onClick.AddListener(ClosePanel);

            OnAwake();
        }

        private void SetEnable(bool value)
        {
            gameObject.SetActive(value);
        }

        public void OpenPanel()
        {
            OnBeforeOpenPanel();
            SetEnable(true);
            OnAfterOpenPanel();
        }

        public void ClosePanel()
        {
            OnBeforeClosePanel();
            SetEnable(false);
            OnAfterClosePanel();
        }

        #region Virtual Methods

        protected virtual void OnAwake()
        {
        }

        protected virtual void OnBeforeOpenPanel()
        {
        }

        protected virtual void OnAfterOpenPanel()
        {
        }

        protected virtual void OnBeforeClosePanel()
        {
        }

        protected virtual void OnAfterClosePanel()
        {
        }

        #endregion
    }
}