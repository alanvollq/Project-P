﻿using System.Collections.Generic;
using Core.Pool;
using UnityEngine;

namespace Core.UIElements.ListView
{
    public abstract class ListView<TItem> : MonoBehaviour
    {
        [SerializeField] private Transform _poolParent;
        [SerializeField] private Transform _content;
        [SerializeField] private ListItemView<TItem> _listItemViewPrefab;

        private readonly List<ListItemView<TItem>> _itemViews = new();
        private readonly List<TItem> _items = new();
        private ObjectsPool<ListItemView<TItem>> _pool;

        
        protected List<TItem> Items => _items;


        public void Initialization()
        {
            _pool = new ObjectsPool<ListItemView<TItem>>(_content, _poolParent, _listItemViewPrefab);
            OnInitialization();
        }

        public void AddItem(TItem item)
        {
            if (item == null)
                return;

            var itemView = _pool.Get(view => view.SetItem(item));

            Items.Add(item);
            _itemViews.Add(itemView);
        }

        public void AddItems(IEnumerable<TItem> items)
        {
            if (items == null)
                return;

            foreach (var item in items)
                AddItem(item);
        }

        public void RemoveItem(TItem item)
        {
            var itemIndex = Items.IndexOf(item);
            Items.RemoveAt(itemIndex);
            _pool.Return(_itemViews[itemIndex]);
            _itemViews.RemoveAt(itemIndex);
        }

        public void RemoveAll()
        {
            Items.Clear();
            _itemViews.Reverse();
            _itemViews.ForEach(item => _pool.Return(item));
            _itemViews.Clear();
        }
        
        protected virtual void OnInitialization()
        {
        }
    }
}