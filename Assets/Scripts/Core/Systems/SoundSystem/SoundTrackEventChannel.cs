using System;
using Core.Systems.SoundSystem.SoundTracks;
using UnityEngine;

namespace Core.Systems.SoundSystem
{
    [CreateAssetMenu(fileName = "Sound Track Event Channel", menuName = "Data/Event Channels/Sound Track Event Channel")]
    public class SoundTrackEventChannel : ScriptableObject
    {
        private event Action<ISoundTrack> PlayRequested;

        
        public void RaisePlayEvent(ISoundTrack soundTrack) => PlayRequested?.Invoke(soundTrack);

        public void AddListener(Action<ISoundTrack> listener)
        {
            if(listener is null)
                return;
            
            PlayRequested += listener;
        }
        
        public void RemoveListener(Action<ISoundTrack> listener)
        {
            if(listener is null)
                return;
            
            PlayRequested -= listener;
        }

        public void RemoveAllListeners()
        {
            PlayRequested = null;
        }
    }
}