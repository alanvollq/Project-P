using System.Collections.Generic;
using UnityEngine;

namespace Core.Systems.SoundSystem.Sound
{
    [CreateAssetMenu(fileName = "Sound Settings Data", menuName = "Data/Sound Settings Data")]
    public class SoundSettingsData : ScriptableObject
    {
        [SerializeField] private List<SoundSetting> _soundSettings;


        public void Init()
        {
            foreach (var soundSetting in _soundSettings)
                soundSetting.Init();
        }

        public void SubscribeSetting()
        {
            foreach (var soundSetting in _soundSettings)
                soundSetting.SubscribeToVolumeChannel();
        }

        public void UnsubscribeSetting()
        {
            foreach (var soundSetting in _soundSettings)
                soundSetting.UnsubscribeToVolumeChannel();
        }

        // For editor
        [ContextMenu("Reset Settings")]
        private void Reset()
        {
            foreach (var soundSetting in _soundSettings)
                soundSetting.Reset();
        }
    }
}