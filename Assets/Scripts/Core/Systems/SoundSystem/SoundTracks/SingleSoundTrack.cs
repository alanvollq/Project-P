using UnityEngine;

namespace Core.Systems.SoundSystem.SoundTracks
{
    [CreateAssetMenu(fileName = "Single Sound Track", menuName = "Data/Sound/Single Sound Track")]
    public class SingleSoundTrack : SoundTrack
    {
        [SerializeField] private AudioClip _audioClip;


        public override AudioClip AudioClip => _audioClip;
    }
}