﻿using UnityEngine;

namespace Core.Systems.SoundSystem.SoundTracks
{
    public abstract class SoundTrack: ScriptableObject, ISoundTrack
    {
        [SerializeField] private SoundTrackEventChannel _soundTrackEventChannel;


        public abstract AudioClip AudioClip { get; }
        
        
        public void Play() => _soundTrackEventChannel.RaisePlayEvent(this);
    }
}