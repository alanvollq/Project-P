using System;
using UnityEngine;

namespace Core.Systems.SoundSystem
{
    [CreateAssetMenu(fileName = "Float Event Channel", menuName = "Data/Event Channels/Float Event Channel")]
    public class FloatEventChannel : ScriptableObject
    {
        public event Action<float> EventRaised;
        
        
        public float LastValue { get; private set; }


        public void RaisePlayEvent(float value)
        {
            LastValue = value;
            EventRaised?.Invoke(value);
        }
    }
}