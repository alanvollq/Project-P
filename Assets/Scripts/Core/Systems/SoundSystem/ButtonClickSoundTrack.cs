﻿using Core.Systems.SoundSystem.SoundTracks;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Systems.SoundSystem
{
    public class ButtonClickSoundTrack : MonoBehaviour
    {
        [SerializeField] private SoundTrack _soundTrack;
        [SerializeField] private Button _button;


        private void Awake()
        {
            _button.onClick.AddListener(OnButtonDown);
        }

        private void OnButtonDown()
        {
            _soundTrack.Play();
        }
    }
}