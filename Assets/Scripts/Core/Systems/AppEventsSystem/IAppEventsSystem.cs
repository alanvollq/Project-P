﻿using System;

namespace Core.Systems.AppEventsSystem
{
    public interface IAppEventsSystem : ISystem
    {
        public event Action<bool> AppFocusChanged; 
        public event Action<bool> AppPauseChanged; 
        public event Action AppQuit; 
    }
}