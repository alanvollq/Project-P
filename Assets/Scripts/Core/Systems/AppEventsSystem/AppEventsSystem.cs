﻿using System;
using UnityEngine;

namespace Core.Systems.AppEventsSystem
{
    public class AppEventsSystem : MonoBehaviour, IAppEventsSystem
    {
        public event Action<bool> AppFocusChanged; 
        public event Action<bool> AppPauseChanged; 
        public event Action AppQuit; 
        
        
        private void OnApplicationFocus(bool hasFocus)
        {
            AppFocusChanged?.Invoke(hasFocus);
        }

        private void OnApplicationPause(bool pauseStatus)
        {
            AppPauseChanged?.Invoke(pauseStatus);
        }

        private void OnApplicationQuit()
        {
            AppQuit?.Invoke();
        }
    }
}