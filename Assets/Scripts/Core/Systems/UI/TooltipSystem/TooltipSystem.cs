﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Context;
using Core.Extensions;
using Core.Loggers;
using Game.Systems.InputSystem;
using UnityEngine;
using static Core.Loggers.CoreLogData;
using static UnityEngine.Object;

namespace Core.Systems.UI.TooltipSystem
{
    public class TooltipSystem : ITooltipSystem, IGameContextInitializable
    {
        private readonly IDictionary<Type, BaseTooltip> _tooltips;
        private BaseTooltip _openedTooltip;
        private IInputSystem _inputSystem;

        public TooltipSystem(IEnumerable<BaseTooltip> basePages)
        {
            TooltipSystemLogData.Log("Create");
            _tooltips = basePages
                .Select(Instantiate)
                .ToDictionary(baseTooltip => baseTooltip.GetType());
        }


        public void Initialization(IGameContext gameContext)
        {
            _inputSystem = gameContext.GetSystem<IInputSystem>();
            _inputSystem.PrimaryTouch += OnPrimaryTouch;
            
            TooltipSystemLogData.Log("Initialize");
            var parentObject = new GameObject("[ Tooltips ]");
            var parentTransform = parentObject.AddComponent<RectTransform>();
            
            parentTransform.SetParent(gameContext.RootOwner.UIRoot);
            parentTransform.SetDefaultMaxStretch();

            foreach (var tooltip in _tooltips.Values)
            {
                tooltip.transform.SetParent(parentTransform);
                tooltip.Initialization(this, gameContext)
                    .RectTransform.SetDefaultMaxStretch();
            }
        }

        private void OnPrimaryTouch()
        {
            CloseLast();
        }

        public void Open<TTooltip>(IUIElementOpenParam openParam = null) where TTooltip : BaseTooltip
        {
            var type = typeof(TTooltip);
            if (!_tooltips.TryGetValue(type, out var tooltip))
                throw new Exception($"Tooltip {type} is not found!");

            CloseLast();
            tooltip.Open(openParam);
            _openedTooltip = tooltip;
        }

        private void CloseLast()
        {
            if (_openedTooltip is null)
                return;

            _openedTooltip.Close();
        }
    }
}