﻿using Game.UI.Tooltips;

namespace Core.Systems.UI.TooltipSystem
{
    public interface ITooltipSystem : ISystem
    {
        public void Open<TTooltip>(IUIElementOpenParam openParam) where TTooltip : BaseTooltip;
    }
}