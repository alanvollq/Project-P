﻿using Core.Context;
using UnityEngine;

namespace Core.Systems.UI.TooltipSystem
{
    public class BaseTooltip : MonoBehaviour
    {
        [SerializeField] private RectTransform _contentTransform;
        [SerializeField] private float _arrowOffset;
        
        
        private Vector2 _offset => new(_contentTransform.sizeDelta.x / 2, _contentTransform.sizeDelta.y / 2);
        protected ITooltipSystem TooltipSystem;
        protected IGameContext GameContext;

        
        public RectTransform RectTransform { get; private set; }

        
        public BaseTooltip Initialization(TooltipSystem tooltipSystem, IGameContext gameContext)
        {
            TooltipSystem = tooltipSystem;
            GameContext = gameContext;

            RectTransform = GetComponent<RectTransform>();

            OnInitialization();

            gameObject.SetActive(false);

            return this;
        }

        public void Open(IUIElementOpenParam openParam)
        {
            OnBeforeOpen(openParam);
            gameObject.SetActive(true);
            OnAfterOpen(openParam);
        }

        public void Close()
        {
            OnBeforeClose();
            gameObject.SetActive(false);
            OnAfterClose();
        }
        
        protected void SetPosition(RectTransform targetTransform)
        {
            var targetPointSizeDelta = targetTransform.sizeDelta;
            var targetPointOffset = new Vector2(targetPointSizeDelta.x / 2, targetPointSizeDelta.y / 2);

            _contentTransform.position = targetTransform.position;
            var showPosition = _contentTransform.anchoredPosition;
            
            showPosition.y += targetPointOffset.y + _offset.y + _arrowOffset;
            _contentTransform.anchoredPosition = showPosition;
        }

        protected virtual void OnInitialization()
        {
        }

        protected virtual void OnBeforeOpen(IUIElementOpenParam openParam)
        {
        }

        protected virtual void OnAfterOpen(IUIElementOpenParam openParam)
        {
        }
        
        protected virtual void OnBeforeClose()
        {
        }

        protected virtual void OnAfterClose()
        {
        }
    }
    
    public abstract record TooltipOpenParam(RectTransform TargetTransform) : IUIElementOpenParam
    {
        public RectTransform TargetTransform { get; } = TargetTransform;
    }
}