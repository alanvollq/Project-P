using System;

namespace Core.Systems.UI.PopupSystem
{
    public interface IPopupSystem : ISystem
    {
        public event Action<BasePopup> PopupOpened;
        public event Action<BasePopup> PopupClosed;


        public bool HasOpenedPopup { get; }


        public void Open<TPopup>(IUIElementOpenParam openParam = null, bool hidePrev = false) where TPopup : BasePopup;
        public void CloseLast(bool isForce = false);
        public void CloseAll(bool isForce = false);
    }
}