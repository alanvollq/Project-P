﻿using DG.Tweening;
using UnityEngine;

namespace Core.Systems.UI.PopupSystem.PopupAnimators
{
    public class DefaultPopupContentShowAnimator : UIAnimator
    {
        [SerializeField] private Transform _content;
        [SerializeField] private Vector3 _startScale = new(0.8f, 0.8f, 0.8f);
        [SerializeField] private Vector3 _endScale = Vector3.one;
        [SerializeField] private float _duration = 0.3f;


        public override void SetDefaultState()
        {
            _content.localScale = _startScale;
        }

        public override void PlayAnimation(Sequence sequence)
        {
            sequence
                .Join(_content.DOScale(Vector3.one, _duration))
                ;
        }
    }
}