﻿using DG.Tweening;
using UnityEngine;

namespace Core.Systems.UI.PopupSystem.PopupAnimators
{
    public class DefaultPopupContentHideAnimator: UIAnimator
    {
        [SerializeField] private Transform _content;
        [SerializeField] private Vector3 _endScale = new(0.8f, 0.8f, 0.8f);
        [SerializeField] private float _duration = 0.3f;
        

        public override void PlayAnimation(Sequence sequence)
        {
            sequence
                .Join(_content.DOScale(_endScale, _duration))
                ;
        }
    }
}