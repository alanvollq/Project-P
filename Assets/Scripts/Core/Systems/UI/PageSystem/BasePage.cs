using Core.Context;
using UnityEngine;

namespace Core.Systems.UI.PageSystem
{
    public abstract class BasePage : MonoBehaviour
    {
        protected IPageSystem PageSystem;
        protected IGameContext GameContext;

        public RectTransform RectTransform { get; private set; }


        public BasePage Initialization(IPageSystem pageSystem, IGameContext gameContext)
        {
            PageSystem = pageSystem;
            GameContext = gameContext;

            RectTransform = GetComponent<RectTransform>();

            OnInitialization();

            gameObject.SetActive(false);

            return this;
        }

        protected virtual void OnInitialization()
        {
        }

        public void Open(IUIElementOpenParam openParam)
        {
            OnBeforeOpen(openParam);
            gameObject.SetActive(true);
            OnAfterOpen(openParam);
        }

        public void Close()
        {
            OnBeforeClose();
            gameObject.SetActive(false);
            OnAfterClose();
        }

        protected virtual void OnBeforeOpen(IUIElementOpenParam openParam)
        {
        }

        protected virtual void OnAfterOpen(IUIElementOpenParam openParam)
        {
        }

        protected virtual void OnBeforeClose()
        {
        }

        protected virtual void OnAfterClose()
        {
        }
    }
}