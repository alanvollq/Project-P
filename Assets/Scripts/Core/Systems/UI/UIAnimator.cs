﻿using DG.Tweening;
using UnityEngine;

namespace Core.Systems.UI
{
    public abstract class UIAnimator : MonoBehaviour
    {
        [SerializeField] private bool _isEnable = true;


        public bool IsEnable => _isEnable;


        public abstract void PlayAnimation(Sequence sequence);


        public void SetEnable(bool value)
        {
            _isEnable = value;
        }

        public virtual void SetDefaultState()
        {
        }
    }
}