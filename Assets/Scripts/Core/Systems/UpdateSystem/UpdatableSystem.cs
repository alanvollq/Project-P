﻿using System.Collections.Generic;
using Core.Context;
using UnityEngine;

namespace Core.Systems.UpdateSystem
{
    public class UpdatableSystem : MonoBehaviour, IUpdateSystem, IGameContextInitializable
    {
        private readonly List<IUpdatable> _updatable = new();
        private readonly List<ILateUpdatable> _lateUpdatable = new();
        private readonly List<IFixedUpdatable> _fixedUpdatable = new();


        private void Update()
        {
            foreach (var updatable in _updatable)
                updatable.Update();
        }

        private void FixedUpdate()
        {
            foreach (var fixedUpdatable in _fixedUpdatable)
                fixedUpdatable.FixedUpdate();
        }

        private void LateUpdate()
        {
            foreach (var lateUpdatable in _lateUpdatable)
                lateUpdatable.LateUpdate();
        }

        public void Initialization(IGameContext gameContext)
        {
            transform.SetParent(gameContext.RootOwner.CoreRoot);
        }

        public void AddUpdatable(IUpdatable updatable)
        {
            _updatable.Add(updatable);
        }

        public void RemoveUpdatable(IUpdatable updatable)
        {
            _updatable.Remove(updatable);
        }

        public void AddFixedUpdatable(IFixedUpdatable updatable)
        {
            _fixedUpdatable.Add(updatable);
        }

        public void RemoveFixedUpdatable(IFixedUpdatable updatable)
        {
            _fixedUpdatable.Remove(updatable);
        }

        public void AddLateUpdatable(ILateUpdatable updatable)
        {
            _lateUpdatable.Add(updatable);
        }

        public void RemoveLateUpdatable(ILateUpdatable updatable)
        {
            _lateUpdatable.Remove(updatable);
        }
    }
}