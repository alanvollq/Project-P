﻿namespace Core.Systems.UpdateSystem
{
    public interface IUpdateSystem : ISystem
    {
        public void AddUpdatable(IUpdatable updatable);
        public void RemoveUpdatable(IUpdatable updatable);
        
        public void AddFixedUpdatable(IFixedUpdatable updatable);
        public void RemoveFixedUpdatable(IFixedUpdatable updatable);
        
        public void AddLateUpdatable(ILateUpdatable updatable);
        public void RemoveLateUpdatable(ILateUpdatable updatable);
    }
}