﻿namespace Core.Systems.UpdateSystem
{
    public interface ILateUpdatable
    {
        public void LateUpdate();
    }
}