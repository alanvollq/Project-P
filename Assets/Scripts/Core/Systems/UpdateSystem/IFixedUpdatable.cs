﻿namespace Core.Systems.UpdateSystem
{
    public interface IFixedUpdatable
    {
        public void FixedUpdate();
    }
}