﻿using System;
using System.Collections.Generic;

namespace Core.Systems.EventSystem
{
    /// <summary>
    /// Provides an implementation of the IEventSystem interface for managing and dispatching events.
    /// </summary>
    public class EventSystem : IEventSystem
    {
        private readonly Dictionary<Type, List<Delegate>> _subscribers = new();

        /// <inheritdoc />
        public void Subscribe<TEvent>(Action<TEvent> callback) where TEvent : IEvent
        {
            if (_subscribers.TryGetValue(typeof(TEvent), out var subscribersList) == false)
            {
                subscribersList = new List<Delegate>();
                _subscribers[typeof(TEvent)] = subscribersList;
            }

            subscribersList.Add(callback);
        }

        /// <inheritdoc />
        public void Unsubscribe<TEvent>(Action<TEvent> callback) where TEvent : IEvent
        {
            if (_subscribers.TryGetValue(typeof(TEvent), out var subscribersList) == false)
                return;

            subscribersList.Remove(callback);
        }

        /// <inheritdoc />
        public void Raise<TEvent>(TEvent @event) where TEvent : IEvent
        {
            if (_subscribers.TryGetValue(typeof(TEvent), out var subscribersList) == false)
                return;

            foreach (var callback in subscribersList)
                (callback as Action<TEvent>)?.Invoke(@event);
        }
    }
}