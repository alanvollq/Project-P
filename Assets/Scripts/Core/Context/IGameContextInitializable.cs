namespace Core.Context
{
    public interface IGameContextInitializable
    {
        public void Initialization(IGameContext gameContext);
    }
}