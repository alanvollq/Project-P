using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Core.Pool
{
    public class ObjectsPool<TPoolableObject> where TPoolableObject : MonoBehaviour, IPoolable
    {
        private readonly Transform _enableParentTransform;
        private readonly Transform _disableParentTransform;
        private readonly TPoolableObject _prefab;
        private readonly Stack<TPoolableObject> _disableObjects = new();
        private readonly List<TPoolableObject> _enableObjects = new();

        public ObjectsPool(Transform enableParentTransform, Transform disableParentTransform, TPoolableObject prefab)
        {
            _enableParentTransform = enableParentTransform;
            _disableParentTransform = disableParentTransform;
            _prefab = prefab;
        }


        public IEnumerable<TPoolableObject> ActiveObjects => _enableObjects;
        

        public bool TryReturn(TPoolableObject poolableObject)
        {
            if (_enableObjects.Contains(poolableObject) == false) 
                return false;
            
            Return(poolableObject);
            return true;

        }


        public void Return(TPoolableObject poolableObject)
        {
            poolableObject.gameObject.SetActive(false);
            poolableObject.Release();
            poolableObject.transform.SetParent(_disableParentTransform);
            _enableObjects.Remove(poolableObject);
            _disableObjects.Push(poolableObject);
        }

        public void ReturnAll()
        {
            var returnedObject = new List<TPoolableObject>(_enableObjects);
            foreach (var poolableObject in returnedObject)
                Return(poolableObject);
        }

        public TPoolableObject Get(Action<TPoolableObject> getAction = null)
        {
            var poolableObject = _disableObjects.Count > 0 ? _disableObjects.Pop() : Create();

            poolableObject.transform.SetParent(_enableParentTransform);
            getAction?.Invoke(poolableObject);
            poolableObject.Recycle();
            poolableObject.gameObject.SetActive(true);
            _enableObjects.Add(poolableObject);

            return poolableObject;
        }

        private TPoolableObject Create()
        {
            var poolableObject = Object.Instantiate(_prefab, _disableParentTransform);
            poolableObject.gameObject.SetActive(false);
            return poolableObject;
        }
    }
}