using System.Collections.Generic;
using Core.Context;
using Core.Data;
using Core.Loggers;
using Core.Root;
using UnityEngine;

namespace Core.GameLaunch
{
    public class GameLauncher : MonoBehaviour
    {
        [SerializeField] private SOLogData _launchLogData;
        [SerializeField] private RootOwner _root;
        [SerializeField] private List<LaunchTask> _launchTasks;

        
        private void Awake()
        {
            var gameContext = new GameContext(_root);

            foreach (var task in _launchTasks)
            {
                _launchLogData.Log($"Started - {task.name}");
                task.Execute(gameContext);
                _launchLogData.Log($"Completed - {task.name}");
            }
            
            Destroy(gameObject);
        }
    }
}