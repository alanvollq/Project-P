﻿namespace Core.SaveSystem
{
    public static class SaveSystemProvider
    {
        private static ISaveSystem _saveSystem;

        public static void SetSaveSystem(ISaveSystem saveSystem) => _saveSystem = saveSystem;

        public static void SetString(string key, string value) => _saveSystem.SetString(key, value);
        public static void SetBool(string key, bool value) => _saveSystem.SetBool(key, value);
        public static void SetFloat(string key, float value) => _saveSystem.SetFloat(key, value);
        public static void SetInt(string key, int value) => _saveSystem.SetInt(key, value);

        public static string GetString(string key, string defaultValue = null) => _saveSystem.GetString(key, defaultValue);
        public static bool GetBool(string key, bool defaultValue = false) => _saveSystem.GetBool(key, defaultValue);
        public static int GetInt(string key, int defaultValue = 0) => _saveSystem.GetInt(key, defaultValue);
        public static float GetFloat(string key, float defaultValue = 0) => _saveSystem.GetFloat(key, defaultValue);

        public static void DeleteAll() => _saveSystem.DeleteAll();
        public static void DeleteKey(string key) => _saveSystem.DeleteKey(key);
        public static bool HasKey(string key) => _saveSystem.HasKey(key);
        public static void Save() => _saveSystem.Save();
    }
}