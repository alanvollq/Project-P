﻿using System;

namespace Core.SaveSystem
{
    public interface ISaveSystem
    {
        public void SetString(string key,string value);
        public void SetBool(string key,bool value);
        public void SetFloat(string key,float value);
        public void SetInt(string key,int value);

        public string GetString(string key, string defaultValue = null);
        public bool GetBool(string key, bool defaultValue = false);
        public int GetInt(string key, int defaultValue = 0);
        public float GetFloat(string key, float defaultValue = 0);

        public void DeleteAll();
        public void DeleteKey(string key);
        public bool HasKey(string key);

        public void Save();
    }
}