﻿using Core.Extensions;
using UnityEngine;

namespace Core.SaveSystem
{
    public class PlayerPrefsSaveSystem : ISaveSystem
    {
        public void SetString(string key, string value) => PlayerPrefs.SetString(key, value);
        public void SetBool(string key, bool value) => PlayerPrefs.SetInt(key, value.ToInt());
        public void SetFloat(string key, float value) => PlayerPrefs.SetFloat(key, value);
        public void SetInt(string key, int value) => PlayerPrefs.SetInt(key, value);
        
        public string GetString(string key, string defaultValue = null) => PlayerPrefs.GetString(key, defaultValue);
        public bool GetBool(string key, bool defaultValue = false) => PlayerPrefs.GetInt(key, defaultValue.ToInt()).ToBool();
        public int GetInt(string key, int defaultValue = 0) => PlayerPrefs.GetInt(key, defaultValue);
        public float GetFloat(string key, float defaultValue = 0) => PlayerPrefs.GetFloat(key, defaultValue);
        
        public void DeleteAll() => PlayerPrefs.DeleteAll();
        public void DeleteKey(string key) => PlayerPrefs.DeleteKey(key);
        public bool HasKey(string key) => PlayerPrefs.HasKey(key);
        
        public void Save() => PlayerPrefs.Save();
    }
}