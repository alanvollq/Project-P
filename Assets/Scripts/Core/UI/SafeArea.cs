﻿using NaughtyAttributes;
using UnityEngine;

namespace Core.UI
{
    public class SafeArea : MonoBehaviour
    {
        [SerializeField] private bool _applyHorizontal = true;
        [SerializeField] private bool _applyVertical = true;
        
        [Space]
        [Header("Horizontal apply to")]
        [SerializeField, ShowIf("_applyHorizontal")] private bool _applyLeft = true;
        [SerializeField, ShowIf("_applyHorizontal")] private bool _applyRight = true;
        
        [Header("Vertical apply to")]
        [SerializeField, ShowIf("_applyVertical")] private bool _applyBottom = true;
        [SerializeField, ShowIf("_applyVertical")] private bool _applyTop = true;
        
        private RectTransform _rectTransform;


        private void Awake()
        {
            _rectTransform = GetComponent<RectTransform>();

            ApplySafeArea();
        }

        private void ApplySafeArea()
        {
            var safeArea = Screen.safeArea;

            var positionMin = safeArea.position;
            var positionMax = safeArea.position + safeArea.size;

            var anchorMin = Vector2.zero;
            var anchorMax = Vector2.one;

            if (_applyHorizontal)
            {
                if (_applyLeft)
                    anchorMin.x = positionMin.x / Screen.width;
                if (_applyRight)
                    anchorMax.x = positionMax.x / Screen.width;
            }

            if (_applyVertical)
            {
                if (_applyBottom)
                    anchorMin.y = positionMin.y / Screen.height;
                if (_applyTop)
                    anchorMax.y = positionMax.y / Screen.height;
            }

            _rectTransform.anchorMin = anchorMin;
            _rectTransform.anchorMax = anchorMax;
        }
    }
}