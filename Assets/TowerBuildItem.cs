using UnityEngine;

public class TowerBuildItem : MonoBehaviour
{
    [SerializeField] private SpriteRenderer _icon;
    [SerializeField] private SpriteRenderer _border;
    
    
    public void SetInfo(Sprite icon, Color color)
    {
        _icon.sprite = icon;
        _border.color = color;
    }
}
